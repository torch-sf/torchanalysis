import pickle
import numpy as np

data_dir = "/projects/0/flashrad/sean/prod/L3-70M"
cluster_maxrad = pickle.load( open(data_dir + "/energy_clusters/clusters3373.pickle", "rb") )
cluster_lagr09 = pickle.load( open(data_dir +"/energy_clusters_lagr09/clusters3373.pickle", "rb") )
cluster_lagr07 = pickle.load( open(data_dir +"/energy_clusters_lagr07/clusters3373.pickle", "rb") )

for i, cluster in enumerate(cluster_maxrad.grp_gas_TM):
    ratio09_grp_gas_TM = float(cluster_maxrad.grp_gas_TE[i]) / float(cluster_lagr09.grp_gas_TE[i])
    ratio07_grp_gas_TM = float(cluster_maxrad.grp_gas_TE[i]) / float(cluster_lagr07.grp_gas_TE[i])
    print "ratio max_rad/lagr09: ", ratio09_grp_gas_TM
    print "ratio max_rad/lagr07: ", ratio07_grp_gas_TM

print "TE gas of max_rad ", cluster_maxrad.grp_gas_TE
print "TE gas of lagr09 ", cluster_lagr09.grp_gas_TE
print "TE gas of lagr07 ", cluster_lagr07.grp_gas_TE
