# README
## analysis_routines/

Josh's analysis scripts. Utilizing mpi4py.

You may need to make changed to your local $PYTHONPATH env variable to ensure these scripts are able to import each other properly.

## Main scripts:

* energy_analysis2.py
	* See run_cluster_analysis.sh for an example for how to call.

* paraplot_mpi.py

* run_ba.py

* star_formation_rate.py

* get_dens_center.py

* bound_mass_frac.py

## All script descriptions (main, support, out-of-date):

* analysis_utilities.py
	* Collection of MPI-based functions used for picking up and distributing files to processors.
	* This is utilized by most scripts here.

* binary_routines.py
	* Support fuctions for use in binary_analysis.py

* binary_analysis.py
	* Support functions for use in run_ba.py

* bound_mass_frac.py
	* Main run script for reporting fraction of bound gas/stars.
	* A localized version of this is used in energy_analysis2.py

* energy_routines.py
	* Supoprt fuctions for use in binary_analysis.py and energy_analysis2.py

* energy_analysis.py
	* OUT OF DATE main run script for gas/star energy fraction over time

* energy_analysis2.py
	* Main run script for gas/star energy fraction over time.
	* Reports all cluster properties as well as gas properties on which the cluster sit.

* get_dens_center.py
	* Main run script for gas density center.

* load_flash_particles.py
	* Support script for loading FLASH particle files.

* make_clustering_plots.py
	* Support script, generates cluster plots (can be stitched into movies).

* paraplot_mpi.py
	* Main run script for generating parameter plots.

* plotting_routines.py
	* Support functions for plotting histogram or scatter plots.

* run_ba.py
	* Main run script for binary analysis

* run_cluster_analysis.sh
	* Runs energy_analysis2.py

* star_formation_rate.py
	* Main run script for star formation rate data output

* task_based_processing.py
	* Support utilities for MPI communication structure.