from analysis_utilities import gather_files, load_pickles_as_dictionary
from task_based_processing import initialize_mpi, perform_task_in_parallel
from energy_routines import plot_clumps
from amuse.lab import units as u


import argparse

parser = argparse.ArgumentParser(description="Plot all the clusters \
                                 analyzed previously using Hop or DBSCAN \
                                 Written by: Joshua Wall, Drexel University")

parser.add_argument("-d", "--directory", default="./",
                   help="Input directory where the particle files are located. \
                         Default is './'")

parser.add_argument("-w", "--write_directory", default="./",
                   help="Output directory where the files are written. \
                         Default is './'")

parser.add_argument("-f", "--filename", default="clusters",
                   help="Filename prefix (before the numbers). \
                         Default is clusters.")

parser.add_argument("-s", "--start_file_number", default=None, type=int,
                    help="The starting number of the files you want to \
                          plot. By default its None, which picks the first file \
                          in the folder.")

parser.add_argument("-e", "--end_file_number", default=None, type=int,
                    help="The ending number of the files you want to \
                          plot. By default its None, which picks the \
                          last file in the folder.")

parser.add_argument("-c", "--chunk_size", default=1, type=int,
                    help="Number of chunks to send to each worker process.")

parser.add_argument("-db", "--debug", action="store_true",
                    help="Debug this script.")

# Plot all the clusters that were found using
# either DBSCAN or Hop

def load_cluster_and_plot_it(one_whole_filename, file_dir='./',debug=False):
    # Load the file.
    clusters = load_pickles_as_dictionary(one_whole_filename, 
                                          whole_filename_passed=True,
                                          debug=debug)
    
    plt_number = one_whole_filename[0][-11:-7]
    if (debug): 
        print clusters.groups
        print "{:2f} Myr".format(clusters.groups[0][0].ht.value_in(u.Myr))
        print file_dir
        print one_whole_filename
        print plt_number
    # Plot it.
    plot_clumps(clusters.groups, plt_dir=file_dir, plt_name='clumpy'+plt_number,
                time="{:2f} Myr".format(clusters.groups[0][0].ht.value_in(u.Myr)),
                save_plts=True)
    return

if (__name__ == "__main__"):

    args        = parser.parse_args()
    file_dir    = args.directory
    file_name   = args.filename
    start       = args.start_file_number
    end         = args.end_file_number
    out_dir     = args.write_directory
    chunk_size  = args.chunk_size
    debug       = args.debug

    if (debug): print "Starting up in debug mode.", debug
    root = 0
    # Setup for MPI run.
    rank, size, comm = initialize_mpi()

    pre = "Proc", rank

    if (rank==0):
        if (debug):
            print "Calling gather files"
            print "file_name = ", file_name
            print "file_dir  = ", file_dir
            print "start     = ", start
            print "end       = ", end
        files, start, end = gather_files(file_name, file_dir,
                            suffix='.pickle',
                            start=start, end=end, debug=debug)

        num_files = len(files)

        if (debug):
            print files
            print num_files
    else:
        files=None

    args   = []
    kwargs = {'file_dir':file_dir,
              'debug':debug}
    perform_task_in_parallel(
        load_cluster_and_plot_it, args, kwargs, 
        files, chunk_size, rank, size, comm, debug=debug
        )
