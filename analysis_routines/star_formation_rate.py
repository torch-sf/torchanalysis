import numpy as np
import sys, os

from analysis_routines.analysis_utilities import gather_files, initialize_mpi, \
    mpi_gather_dict_to_root, mpi_reduce_np_array_in_place
from analysis_routines.task_based_processing import perform_task_in_parallel
import argparse
import h5py
import sys
import pickle

parser = argparse.ArgumentParser(description=" Calculate the star \
                                 formation rate from the particle \
                                 files from a FLASH simulation. \
                                 Written by: Joshua Wall, Drexel University")
                                 
parser.add_argument("-d", "--directory", default="./",
                   help="Input directory where the particle files are located. \
                         Default is './' Note we assume the particle files look like *part*.")

parser.add_argument("-w", "--write_directory", default=".",
                   help="Output directory where the files are written. \
                         Default is '.'")

parser.add_argument("-f", "--filename", default="part",
                   help="Filename prefix (before the numbers). \
                         Default is part.")

parser.add_argument("-s", "--start_file_number", default=None, type=int,
                    help="The starting number of the files you want to \
                          plot. By default its None, which picks the first file \
                          in the folder.")

parser.add_argument("-e", "--end_file_number", default=None, type=int,
                    help="The ending number of the files you want to \
                          plot. By default its None, which picks the \
                          last file in the folder.")

parser.add_argument("-v", "--verbose", default=0, type=int,
                    help="Speak to me.")

parser.add_argument("-c", "--chunk_size", default=1, type=int,
                    help="Number of chunks to send to each worker process.")


class sfr_storage_object(object):

    def __init__(self, mass_all_stars = {},
                        vel_mag_all_stars = {},
                        creation_time_all_stars = {},
                        ejected_mass = {},
                        ejected_vel = {},
                        sink_mass = None,
                        part_time = None,
                        tot_part_mass = None):

        self.mass_all_stars    = mass_all_stars
        self.vel_mag_all_stars = vel_mag_all_stars
        self.creation_time_all_stars = creation_time_all_stars
        self.ejected_mass      = ejected_mass
        self.ejected_vel       = ejected_vel
        self.sink_mass         = sink_mass
        self.part_time         = part_time
        self.tot_part_mass     = tot_part_mass

        return
    

def get_sfr_and_things(this_file, mass_all_stars,
                                  vel_mag_all_stars,
                                  creation_time_all_stars,
                                  ejected_mass,
                                  ejected_vel):

    """
    Gather all the requisite data to perform
    the star formation rate analysis as well
    as the ejection analysis from a particle
    file. Note mass, creation time, ejected mass
    and ejected vel are dictionaries with the
    particle tags as keys.
 
    Keyword arguments:

    this_file          -- The string name of the file to load.
    mass_all_stars     -- Dictionary for appending stellar masses. 
    vel_mag_all_stars  -- Dictionary for appending velocity magnitude
    creation_time_all_stars -- Dictionary for appending creation time
    ejected_mass       -- Dictionary for appending ejected masses.
    ejected_vel        -- Dictionary for appending ejected velocities.

    Returns:

    mass_all_stars -- Masses of the stars.
    creation_time_all_stars  -- Creation times of the stars.
    ejected_mass   -- Mass of all the ejected stars.
    ejected_vel    -- Velocity of all the ejected stars.
    vel_mag_all_stars -- Velocity magnitude of all the stars.
    sink_mass      -- Summed masses of the sinks.
    tot_part_mass  -- Summed masses of all the stars and sinks.
    part_time      -- Current simulation time.
    """

    mass_all_stars = creation_time_all_stars = \
           ejected_mass = ejected_vel = vel_mag_all_stars = {}
        
    sink_mass = tot_part_mass = part_time = None

    # Load the data    
    ds = h5py.File(this_file, 'r')
    # Get all the data pieces by label.
    part_data    = ds.get('tracer particles')
    part_names   = ds.get('particle names')
    part_data    = np.array(part_data)
    part_names   = np.array(part_names)
    real_scalars = ds.get('real scalars')
    part_time    = real_scalars[0][1]
    
    # If there are particles, do all the data gathering.
    if (part_data.size > 1):
        
        mass_ind = np.where(np.char.strip(part_names)=='mass')[0]
        tag_ind  = np.where(np.char.strip(part_names)=='tag')[0]
        type_ind = np.where(np.char.strip(part_names)=='type')[0]
        ct_ind   = np.where(np.char.strip(part_names)=='creation_time')[0]
        velx_ind  = np.where(np.char.strip(part_names)=='velx')[0]
        vely_ind  = np.where(np.char.strip(part_names)=='vely')[0]
        velz_ind  = np.where(np.char.strip(part_names)=='velz')[0]
        
        # Get the particle types
        type_part = part_data[:,type_ind].flatten()
        # Find only star particles (particle_type=1)
        star_ind = np.where(type_part==1)[0]
        # Find only sink particles (particle_type=2)
        sink_ind = np.where(type_part==2)[0]
        # Masses of stars
        mass_part = part_data[star_ind,mass_ind]
        # Sink masses
        sink_mass = part_data[sink_ind,mass_ind].sum()
        # Tags of stars
        tags_part = part_data[star_ind,tag_ind]
        # Creation times of stars
        ctime_part = part_data[star_ind,ct_ind]
        # Velocities
        velx = np.zeros(len(tags_part))
        vely = np.zeros(len(tags_part))
        velz = np.zeros(len(tags_part))
        vel  = np.zeros(len(tags_part))
        
        velx = part_data[star_ind,velx_ind]
        vely = part_data[star_ind,vely_ind]
        velz = part_data[star_ind,velz_ind]
        vel  = np.sqrt(velx**2.0 + vely**2.0 + velz**2.0)
        
        # Now add any particle who we haven't added previously to the dictionaries.
        for j, tag in enumerate(tags_part):
            
            if tag not in mass_all_stars:
                
                mass_all_stars[tag]          = mass_part[j]
                creation_time_all_stars[tag] = ctime_part[j]
            else:
                # Update the vel mag for this star
                vel_mag_all_stars[tag]       = vel[j]
        
        for key, val in vel_mag_all_stars.iteritems():
            if key not in tags_part:
                ejected_mass[key]     = mass_all_stars[key]
                ejected_vel[key]      = vel_mag_all_stars[key]
                
        tot_part_mass = np.array(mass_all_stars.values()).sum() \
                        + sink_mass

    return mass_all_stars, creation_time_all_stars, \
           ejected_mass, ejected_vel, vel_mag_all_stars, \
           sink_mass, tot_part_mass, part_time

def perform_sfr_analysis_on_batch_files(local_files,sfr_obj,
                                        current_index=None,
                                        debug=False):
    """
    Perform the SFR and other analysis
    on a batch of files, storing the
    results in sfr_obj.
    """
    pre = '[perform_sfr_analysis]:'

    if (current_index is None):
        sys.exit('current_index has not been set!')

    for i,f in enumerate(local_files):

        if (debug): print pre, 'current_index+i=', current_index+i

        sfr_obj.mass_all_stars, \
        sfr_obj.creation_time_all_stars, \
        sfr_obj.ejected_mass, \
        sfr_obj.ejected_vel, \
        sfr_obj.vel_mag_all_stars, \
        sfr_obj.sink_mass[current_index+i], \
        sfr_obj.tot_part_mass[current_index+i], \
        sfr_obj.part_time[current_index+i] = \
            get_sfr_and_things(f, sfr_obj.mass_all_stars,
                                  sfr_obj.vel_mag_all_stars,
                                  sfr_obj.creation_time_all_stars,
                                  sfr_obj.ejected_mass,
                                  sfr_obj.ejected_vel)

    return

if (__name__ == '__main__'):

    # Parse those args.
    args = parser.parse_args()
    file_dir   = args.directory
    file_name  = args.filename
    start      = args.start_file_number
    end        = args.end_file_number
    out_dir    = args.write_directory
    chunk_size = args.chunk_size
    verbose    = args.verbose

    debug = verbose > 0
    root = 0

    if (debug):
        print "Starting up MPI."

    # Setup for MPI run.
    rank, size, comm = initialize_mpi(debug=debug)

    if (debug):
        print "Gathering files."

    # Get all the files.
    files, start, end = gather_files(file_name, file_dir,
                        start=start, end=end,
                        debug=debug)

    if (debug): print "files=", files

    if (debug):
        print "Setting up the sfr object."

    # Initialize all the data storage.
    # Dicitionaries.
    mass_all_stars = {}
    vel_mag_all_stars = {}
    creation_time_all_stars = {}
    ejected_mass = {}
    ejected_vel = {}
    # Numpy arrays.
    sink_mass     = np.zeros(len(files))
    part_time     = np.zeros(len(files))
    tot_part_mass = np.zeros(len(files))

    # Make an instance of the storage object.
    sfr_obj = sfr_storage_object(mass_all_stars,
                            vel_mag_all_stars,
                            creation_time_all_stars,
                            ejected_mass,
                            ejected_vel,
                            sink_mass,
                            part_time,
                            tot_part_mass)

    # Set up the arguments for the function call
    # to be done by all the worker processors.
    func_args = [sfr_obj]
    func_kwargs = {'debug':debug}

    if (debug):
        print "func_args =", func_args
        print "func_kwargs =", func_kwargs

    # Now run the analysis in parallel.
    perform_task_in_parallel(perform_sfr_analysis_on_batch_files,
                                func_args, func_kwargs, files,
                                chunk_size, rank, size, comm,
                                root=0, debug=debug)

    # Okay, now watch this fancy footwork for reducing the data across
    # all the processors....

    # First find all the attributes of the sfr object. 
    attributes = filter(lambda x: not x.startswith("__"), dir(sfr_obj))

    if (debug): print 'attributes=', attributes

    # Now for each attr, idenitfy if its a dictionary or numpy array.
    for attr in attributes:
        this_attr_value = getattr(sfr_obj, attr)
        
        if (isinstance(this_attr_value, dict)): # Dictionary?
            # Reduce with our dictionary method
            this_attr_value = mpi_gather_dict_to_root(this_attr_value, comm)
        elif (isinstance(this_attr_value,np.ndarray)): # Numpy array?
            if (debug and rank==root): print "this_attr before reduce =", this_attr_value
            # Reduce with our mpi array reduction in place.
            this_attr_value = mpi_reduce_np_array_in_place(this_attr_value, comm)
            if (debug and rank==root): print "this_attr after reduce =", this_attr_value
        else:
            sys.exit('Unrecognized as a dict or array, what is this?')
        
        # Now set the attribute to the reduced value.
        setattr(sfr_obj, attr, this_attr_value)

    if (rank == root):
        # Store the sfr object
        with open('sfr.pickle', 'wb') as f:
            pickle.dump(sfr_obj, f)

    print "All done!"
