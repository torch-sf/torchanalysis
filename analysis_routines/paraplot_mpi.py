# A script for plotting using YT
# in parallel.

# Written by Joshua Wall and Joseph Tomlinson
# Drexel University
# 2015-2017

import os

from functools import partial
import matplotlib
matplotlib.use('Agg')
from matplotlib import cm, colors, pyplot
import yt
import sys
import signal
import pickle

yt.mylog.level=0
import time # Check timing of this module.
import argparse
import numpy as np

from analysis_utilities import setup_for_mpi_run, gather_files

# Note we use the '@' symbol instead of the normal argparse '-'
# because we need the '-' to still be the minus sign in the
# center argument.

parser = argparse.ArgumentParser(prefix_chars='@', \
                                 description="Make plots of the given \
                                 variable for all the files in \
                                 a Flash run using yt. \
                                 A great starting point for \
                                 movies! \n \
                                 Written by: Joshua Wall, Drexel University")
                                 
parser.add_argument("@d", "@@directory", default="./",
                   help="Input directory where the plot files are located. \
                         Default is '.' Note we assume the plot files look like *plt_cnt* \
                         and the particle files look like *part*.")
                         
parser.add_argument("@s", "@@start_file_number", default=None, type=int,
                    help="The starting number of the files you want to \
                          plot. By default its None, which picks the first file \
                          in the folder.")

parser.add_argument("@e", "@@end_file_number", default=None, type=int,
                    help="The ending number of the files you want to \
                          plot. By default its None, which picks the \
                          last file in the folder.")

parser.add_argument("@n", "@@plot_name", default=None, 
                    help="The name of the output plot files. By \
                    default it will be the variable you choose to plot \
                    with the Flash file number appended.")

parser.add_argument("@v", "@@variable", default="dens",
                    help="The variable you wish to plot from the Flash \
                          dataset. By default it is density.")

parser.add_argument("@p", "@@include_particles", default="none", choices=["all", "none", "mass", "sink"],
                    help="Whether to include particles (and therefore \
                          particle files) in the plots. Default is none. \
                          Can plot a specific type if you are using massive \
                          and sink particles in the same simulation.")
                          
parser.add_argument("@ps", "@@p_size", type=int, default=None, 
                    help="Size of particles in the plot in terms of pixels. \
                          Default is None, where particle size is prop to \
                          the particle mass in solar mass units.")
                          
parser.add_argument("@ann_vel", "@@annotate_velocity",action="store_true",
                    help="Whether to annotate velocity vectors onto the \
                          plot made. Default is False.")
                          
parser.add_argument("@db", "@@debug",action="store_true",
                    help="Switches on debug mode.")

parser.add_argument("@Bl", "@@annotate_B_lines",action="store_true",
                    help="Whether to annotate B field streamlines onto the \
                          plot made. Default is False.")

parser.add_argument("@Bf", "@@annotate_B_field",action="store_true",
                    help="Whether to annotate B field integral lines onto the \
                          plot made. Default is False.")

parser.add_argument("@k", "@@use_chk",action="store_true",
                    help="Whether to switch to using checkpoint files \
                          instead of plot files. Default is False.")

parser.add_argument("@sph", "@@annotate_sphere",  nargs=4, default=None,
                    help="Overplot a sphere/circle onto the plot image. \
                          The first three numbers are the center of the \
                          sphere while the last number is the radius, \
                          all in cm.")

parser.add_argument("@t", "@@plot_type", default="slice", choices=["slice", "projection",
                                                                   "thin_proj", "max_proj"],
                    help="Type of plot to make. Default is slice.")

parser.add_argument("@w", "@@weight_variable", default=None,
                    help="When plotting a projection you can weight the \
                          projected variable by another variable (usually \
                          density). By default it is None. Throws an error \
                          if used with a slice plot.")
                    
parser.add_argument("@a", "@@plot_axis", default="z", choices=["x", "y", "z"],
                    help="Axis to plot along. Default is z.")
                          
parser.add_argument("@l", "@@no_log", action="store_false",
                    help="Turn off log scale for the variable color bar. \
                          Note log scale is on by default.")

parser.add_argument("@zlim", "@@set_zlim", default=None, nargs=2, type=float, required=False, 
                    help="Set the limits of the colorbar for the variable \
                          we're plotting. Required argument. Note that it \
                          matters if you turn off the log scale for the \
                          colorbar!")
                          
parser.add_argument("@np", "@@np", type=int, default=1, 
                    help="Number of processors for yt to use in plotting. \
                          Default is just one. Note that especially for \
                          projections this can speed up plotting a lot.")
                          
parser.add_argument("@z", "@@zoom", type=float, default=1.0, 
                    help="Zoom factor for the plot. Default is 1.0.")
                    
parser.add_argument("@c", "@@center", type=float, nargs=3, default=[0.0, 0.0, 0.0],
                    help="Location to center the plot in the simulation. \
                          Default is [0.0, 0.0, 0.0].")
                          
parser.add_argument("@com", "@@center_on_com",action="store_true",
                    help="Whether to center plot on the particles \
                          center of mass. Default is False.")
                          
parser.add_argument("@cmm", "@@center_on_most_massive",action="store_true",
                    help="Whether to center the plot on the most \
                          massive particle. Default is False.")

parser.add_argument("@tID", "@@track_ID", default=None, type=float, required=False,
                    help="Track a particle by tag, where this is the tag to track. \
                          Default is None.")

first_except = True

# The plots need to be wrapped in a function to be able to use multiprocessing's pool.map.

def plot_func(plt_file, file_num=None, prt_file=None,
              proj_dir="z", zoom_factor=1,
              center_loc=[0.0, 0.0, 0.0], plt_var='dens',
              plt_type='slice', zlim=None, save=False,
              min_mass=0.0, plt_log=True, ann_velocity=False,
              p_size=None, include_particles="all",
              plot_name=None, weight_variable=None, ann_sphere=None,
              ann_mag=False, ann_magSL=False, plt_cmap=None,
              center_on_com=False, center_on_most_massive=False,
              track_ID=None, debug=False):
    
    global first_except

    if (file_num==None):
        file_num=0
        plt_file=[plt_file]

    ds = yt.load(plt_file[file_num]) #, particle_filename=prt_file[file_num])
    dd = ds.all_data()
        
    if (debug): print dd["particle_mass"].in_units("Msun")

    if (include_particles != "none"):
        
        # Test for the presence of particles. If none, jump to exception.
        if (getattr(ds.fields, 'io', None) != None):
            
            all_mass = dd["particle_mass"].in_units("Msun")
            pos      = dd["particle_position"]
            
            if (not first_except):
                print "Plotting with particles."
                first_except = True
            
            if (debug): print "Got masses."
            
            mass_color = None
            
            # Check to see if there are more than one type of particle in
            # Flash. If so, implicitly assume that ACTIVE_PART_TYPE=1 and
            # its what we want. Otherwise plot all particles.
            mass_ind = None
            sink_ind = None
            if (include_particles == "mass"):
                mass_ind = np.where(dd["particle_type"]==1)[0]
                if (debug):
                    print "Plotting just massive."
                    print "Mass ind =", mass_ind
            elif (include_particles == "sink"):
                mass_ind = np.where(dd["particle_type"]==2)[0]
                if (debug):
                    print "Plotting just sink."
                    print "Mass ind =", mass_ind
            elif (include_particles == "all"):
                mass_ind = np.where(dd["particle_type"]==1)[0]
                sink_ind = np.where(dd["particle_type"]==2)[0]
                if (debug):
                    print "Plotting massive and sink."
                    print "Mass ind =", mass_ind
                    print "sink ind =", sink_ind
            
            mass = all_mass[mass_ind]
            px_mass = dd['particle_position_x'][mass_ind].in_units('cm').v
            py_mass = dd['particle_position_y'][mass_ind].in_units('cm').v
            pz_mass = dd['particle_position_z'][mass_ind].in_units('cm').v
            
            if (p_size == None):
                
                # For more 'dynamic', darker colors.
                #norm = colors.Normalize(vmin=1.0, vmax=14.0)
                #mass_color = cm.seismic_r(norm(mass.v))
                #p_size = np.multiply(20.0,np.log10(mass.v))
                
                if (debug): print "Getting pastels."
                # For uniform, bright colors.
                #cmap = pyplot.get_cmap('Pastel1')
                cmap = pyplot.get_cmap('Set1')
                c1 = cmap.colors[0]
                c2 = cmap.colors[1]

                if (debug): print "Got colors."
                mass_color = np.zeros((len(mass.v),3))
                mass_color[np.where(mass.v < 7.0)] = c1
                mass_color[np.where(mass.v >= 7.0)] = c2
                if (debug): print "Colors set."
                p_size = np.multiply(20.0,np.minimum(20.0,mass.v))

                if (debug): 
                    print "Got mass colors."
                    print "mass =   ", mass
                    print "p_size = ", p_size
                        
            
            if (debug):
                print "center_on_com=", center_on_com
                print "track_ID=", track_ID
                print "center_on_most_massive=", center_on_most_massive
                print "all center_loc = 0?", np.all(center_loc == 0.0)
                
            if (center_on_com and ((not np.all(center_loc == 0.0)) and center_on_most_massive)):
                
                print "Warning! Both center_on_com/center_on_most_massive and center is set. \n \
                       Falling back to the set center location!"
                       
            elif (center_on_com and np.all(center_loc == 0.0) and not center_on_most_massive):
                
                if (debug): print "Using the density center locations from density_center.pickle."

                with open('./dens_center.pickle', 'rb') as df:
                    center_loc = pickle.load(df)[file_num,:]


            elif (not center_on_com and center_on_most_massive and (len(mass.v) > 0)):
                
                if (debug): print "Getting location of most massive particle."
                center_loc[0] = px_mass[np.where(mass.v == mass.v.max())][0]
                center_loc[1] = py_mass[np.where(mass.v == mass.v.max())][0]
                center_loc[2] = pz_mass[np.where(mass.v == mass.v.max())][0]

            elif (not center_on_com and not center_on_most_massive 
                  and (track_ID is not None) and (len(mass.v) > 0)):
                
                if (debug): print "Getting location of tracked particle."
                track_ind = np.where(dd['particle_tag'].v == track_ID)[0]
                if (len(track_ind) > 0): # if this particle doesn't exist yet, default to center loc. 
                    center_loc[0] = px_mass[track_ind][0]
                    center_loc[1] = py_mass[track_ind][0]
                    center_loc[2] = pz_mass[track_ind][0]
                
            if (sink_ind is not None):
                
                sink   = all_mass[sink_ind]
                #s_size = np.multiply(20.0,np.log10(sink.v))
                s_size  = np.multiply(20.0,np.minimum(20.0,sink.v))
                px_sink = dd['particle_position_x'][sink_ind].in_units('cm').v
                py_sink = dd['particle_position_y'][sink_ind].in_units('cm').v
                pz_sink = dd['particle_position_z'][sink_ind].in_units('cm').v
            
                if (debug):
                    print "Got sink colors."
                    print "sink =   ", sink
                    print "s_size = ", s_size
                        
            
            #tags = dd["particle_tag"] # In case you want to track which particle is which.
              
            # Note this can be Projection, Slice or Particle plots. 
            if (debug):
                print "plotting gas."
            
            #print "p_size =", p_size

            if (plt_type=="slice"):
                slc = yt.SlicePlot(ds, proj_dir, plt_var,
                                   center=center_loc)
            elif (plt_type=="projection"):
                slc = yt.ProjectionPlot(ds, proj_dir, plt_var,
                                        center=center_loc, weight_field=weight_variable)
            elif (plt_type=="max_proj"):
                slc = yt.ProjectionPlot(ds, proj_dir, plt_var,
                                        center=center_loc, method="mip")
            elif (plt_type=="thin_proj"):
                # Make a thin projection, averaging the density weighted data over +- 3 cells.
                
                # Boundary should be periodic for this.
                ds.periodicity=[True, True, True]
                # First make the left and right corner of the region based
                # on the full domain.
                left_corner = ds.domain_left_edge
                right_corner = ds.domain_right_edge
                # Now adjust the size of the region along the line of sight`.
                depth = 6*dd['dx'].min().in_units('cm')
                if (proj_dir=='x'):
                    left_corner[0] = ds.quan(center_loc[0],'cm') - 0.5 * depth
                    right_corner[0] = ds.quan(center_loc[0],'cm') + 0.5 * depth
                if (proj_dir=='y'):
                    left_corner[1] = ds.quan(center_loc[1],'cm') - 0.5 * depth
                    right_corner[1] = ds.quan(center_loc[1],'cm') + 0.5 * depth
                if (proj_dir=='z'):
                    left_corner[2] = ds.quan(center_loc[2],'cm') - 0.5 * depth
                    right_corner[2] = ds.quan(center_loc[2],'cm') + 0.5 * depth
                weight_variable = 'density'
                # Create the region
                region = ds.box(left_corner, right_corner) 
                slc = yt.ProjectionPlot(ds, proj_dir, plt_var,
                                        center=center_loc, weight_field=weight_variable)
            else: print "Error, improper plt_type given!"
            
            if (debug):
                print "annotating B field lines"
                
            if (ann_mag):
    
                maglimL = 0.5
                maglimH = 0.7
                if (proj_dir == 'x'):
                    slc.annotate_line_integral_convolution('magy', 'magz', lim=(maglimL,maglimH), alpha=1.0)
    
                if (proj_dir == 'y'):
                    slc.annotate_line_integral_convolution('magx', 'magz', lim=(maglimL,maglimH), alpha=1.0)
    
                if (proj_dir == 'z'):
                    slc.annotate_line_integral_convolution('magx', 'magy', lim=(maglimL,maglimH), alpha=1.0)

            if (ann_magSL and not ann_mag):
                
                if (proj_dir == 'x'):
                    slc.annotate_streamlines('magy', 'magz')
                
                if (proj_dir == 'y'):
                    slc.annotate_streamlines('magx', 'magz')
        
                if (proj_dir == 'z'):
                    slc.annotate_streamlines('magx', 'magy')

            if (ann_sphere is not None): # Annotate a sphere onto the plot?
                slc.annotate_sphere(
                    ann_sphere.center[:3], # sphere center
                    radius=(ann_sphere[3],'cm'), # sphere radius
                    circle_args={'color':'black','linestyle':'--','linewidth':3})


            if (debug):
                print "Zooming"
            
            slc.zoom(zoom_factor)
            
            if (plt_cmap!=None):
                slc.set_cmap(plt_var, plt_cmap)

            if (plt_log==False):
                slc.set_log(plt_var, plt_log)
            if (zlim!=None):
                slc.set_zlim(plt_var, zlim[0], zlim[1])
            #if (ann_velocity):
            #    slc.annotate_velocity()
            #    plt_var=plt_var+"_vel"
            
            slc.annotate_timestamp(0.5,0.95, time_unit='yr')
            # particles must be added with a separate call to scatter.
            
            # tell yt to setup the plot so we can plot particles over it.
            #print "setting up"
            slc._setup_plots()
            this_plt = slc.plots[plt_var]

            # grab the plot figure
            if (debug):
                print "grabbing axes"
            slc_ax = this_plt.axes

            # first, save the current x and y axes and units from the plot object
            # note these are made using whatever units yt thinks correct plus
            # our zoom factor and already centered, so we'll just have to deal
            # with each in kind

            xLim = slc_ax.get_xlim()
            yLim = slc_ax.get_ylim()
            
            if (debug):
                print "xLim = ", xLim
                print "ds.domain_right_edge=", slc.ds.domain_right_edge
            #print "proj_dir =", proj_dir
            #print dd['particle_position_z'][mass_ind].in_units('cm').v
            
            # All the particle plotting stuff we need.
            if (proj_dir == 'z'):
                
                # get the conversion factor from units and zooming.
                xf = (xLim[1] - xLim[0]) / (slc.ds.domain_right_edge[0].v - slc.ds.domain_left_edge[0].v) * zoom_factor
                yf = (yLim[1] - yLim[0]) / (slc.ds.domain_right_edge[1].v - slc.ds.domain_left_edge[1].v) * zoom_factor

                # get the particle positions including offsets and zooming.
                posX = np.subtract(px_mass, center_loc[0])*xf
                posY = np.subtract(py_mass, center_loc[1])*yf
                
                if (sink_ind is not None):
                    sinkX = np.subtract(px_sink, center_loc[0])*xf
                    sinkY = np.subtract(py_sink, center_loc[1])*yf

                # get particle velocities in case we want to plot that also.
                velX = dd['particle_velocity_x'][mass_ind].in_units('km/s').v
                velY = dd['particle_velocity_y'][mass_ind].in_units('km/s').v


            elif (proj_dir == 'x'):
                
                xf = (xLim[1] - xLim[0]) / (slc.ds.domain_right_edge[1].v - slc.ds.domain_left_edge[1].v) * zoom_factor
                yf = (yLim[1] - yLim[0]) / (slc.ds.domain_right_edge[2].v - slc.ds.domain_left_edge[2].v) * zoom_factor
                
                posX = np.subtract(py_mass, center_loc[1])*xf
                posY = np.subtract(pz_mass, center_loc[2])*yf
                
                if (sink_ind is not None):
                    sinkX = np.subtract(py_sink, center_loc[1])*xf
                    sinkY = np.subtract(pz_sink, center_loc[2])*yf
                
                velX = dd['particle_velocity_y'][mass_ind].in_units('km/s').v
                velY = dd['particle_velocity_z'][mass_ind].in_units('km/s').v


            elif (proj_dir == 'y'):
                
                xf = (xLim[1] - xLim[0]) / (slc.ds.domain_right_edge[2].v - slc.ds.domain_left_edge[2].v) * zoom_factor
                yf = (yLim[1] - yLim[0]) / (slc.ds.domain_right_edge[0].v - slc.ds.domain_left_edge[0].v) * zoom_factor

                posX = np.subtract(pz_mass, center_loc[2])*xf
                posY = np.subtract(px_mass, center_loc[0])*yf

                if (sink_ind is not None):
                    sinkX = np.subtract(pz_sink, center_loc[2])*xf
                    sinkY = np.subtract(px_sink, center_loc[0])*yf
                
                velX = dd['particle_velocity_z'][mass_ind].in_units('km/s').v
                velY = dd['particle_velocity_x'][mass_ind].in_units('km/s').v

            # Now scatter plot the particles.
            if (debug):
                print "scattering particles."
            slc_ax.scatter(posX, posY, s=p_size, c=mass_color, marker='o', edgecolors='w',
                            alpha=0.6, linewidth=1.0) #np.multiply(20.0,np.log10(mass.v))
            
            if (sink_ind is not None):
                slc_ax.scatter(sinkX, sinkY, s=20, c='k', marker=(5,2), alpha=0.6)
                if (debug):
                    print "scattered sinks."

            #print "Scatter plot done."

            # Plotting messes with the axes, so reset them to be what they were before.
            if (debug):
                print "resetting limits."
            slc_ax.set_xlim(xLim)
            slc_ax.set_ylim(yLim)
            
            # Now add annotations using matplotlib.
            if (debug):
                print "annotating stuff."
            #slc_ax.text(0.5, 0.95, ds.current_time.in_units('Myr'),
            #     horizontalalignment='center', fontsize=20, color='w',
            #     transform=slc_ax.transAxes)

            slc_ax.text(0.5, 0.05, "# particles = " + `len(mass.v)`,
                 horizontalalignment='center', fontsize=20, color='w',
                 transform=slc_ax.transAxes)

            slc_ax.text(0.5,0.01,"total mass = " + `int(round(sum(mass.v)))`,
                 horizontalalignment='center', fontsize=20, color='w',
                 transform=slc_ax.transAxes)
            
            # If requested, add velocity vectors for particles above some threshold mass.
            if (debug):
                print "Checking to add velocity."
            
            #if (plot_massive_vel):
                
                #massiveInd = np.where(dd['particle_mass'][:].in_units('Msun').v > threshold_mass)

                #massX = posX[massiveInd]
                #massY = posY[massiveInd]

                #massVelX = velX[massiveInd]
                #massVelY = velY[massiveInd]

                #q = proj_ax.quiver(massX, massY, massVelX, massVelY)

                #proj_ax.quiverkey(q,0.90,0.90,1.0, label="1 km/s")
            
            if (debug):
                print "Saving plots."
            # Now save or show the plot.

            if (save):
                if (plot_name == None):
                    slc.save(plt_var+plt_file[file_num][-4:]+'.png')
                else:
                    slc.save(plot_name+plt_file[file_num][-4:]+'.png')
            else:
                slc.show()
            slc.plots.clear()
    
        # No particles, just plot the gas.
        else:
            if (first_except): 
                print "Plotting with particles failed, falling back to plotting just the gas."
                first_except = False
            
            if (center_on_com and np.all(center_loc == 0.0) and not center_on_most_massive):    
                if (debug): print "Using the density center locations from density_center.pickle."

                with open('./dens_center.pickle', 'rb') as df:
                    center_loc = pickle.load(df)[file_num,:]

            if (plt_type=="slice"):
                slc = yt.SlicePlot(ds, proj_dir, plt_var,
                                   center=center_loc)
            elif (plt_type=="projection"):
                slc = yt.ProjectionPlot(ds, proj_dir ,plt_var,
                                   center=center_loc, weight_field=weight_variable)
            elif (plt_type=="max_proj"):
                slc = yt.ProjectionPlot(ds, proj_dir, plt_var,
                                        center=center_loc, method="mip")
            elif (plt_type=="thin_proj"):
                # Make a thin projection, averaging the density weighted data over +- 3 cells.
                
                # Boundary should be periodic for this.
                ds.periodicity=[True, True, True]
                # First make the left and right corner of the region based
                # on the full domain.
                left_corner = ds.domain_left_edge
                right_corner = ds.domain_right_edge
                # Now adjust the size of the region along the line of sight`.
                depth = 6*dd['dx'].min().in_units('cm')
                if (proj_dir=='x'):
                    left_corner[0] = ds.quan(center_loc[0],'cm') - 0.5 * depth
                    right_corner[0] = ds.quan(center_loc[0],'cm') + 0.5 * depth
                if (proj_dir=='y'):
                    left_corner[1] = ds.quan(center_loc[1],'cm') - 0.5 * depth
                    right_corner[1] = ds.quan(center_loc[1],'cm') + 0.5 * depth
                if (proj_dir=='z'):
                    left_corner[2] = ds.quan(center_loc[2],'cm') - 0.5 * depth
                    right_corner[2] = ds.quan(center_loc[2],'cm') + 0.5 * depth
                weight_variable = 'density'
                # Create the region
                region = ds.box(left_corner, right_corner) 
                slc = yt.ProjectionPlot(ds, proj_dir, plt_var,
                                        center=center_loc, weight_field=weight_variable)
            else: print "Error, improper plt_type given!"

            slc.annotate_timestamp(0.5,0.95, time_unit='yr')
            
            slc.zoom(zoom_factor)

            if (plt_cmap!=None):
                slc.set_cmap(plt_var, plt_cmap)

            if (plt_log==False):
                slc.set_log(plt_var, plt_log)
            if (zlim!=None):
                slc.set_zlim(plt_var, zlim[0], zlim[1])
            if (ann_velocity):
                slc.annotate_velocity()
                plt_var=plt_var+"_vel"
            if (ann_mag):
    
                maglimL = 0.5
                maglimH = 0.7
                if (proj_dir == 'x'):
                    slc.annotate_line_integral_convolution('magy', 'magz', lim=(maglimL,maglimH), alpha=1.0)
    
                if (proj_dir == 'y'):
                    slc.annotate_line_integral_convolution('magx', 'magz', lim=(maglimL,maglimH), alpha=1.0)
    
                if (proj_dir == 'z'):
                    slc.annotate_line_integral_convolution('magx', 'magy', lim=(maglimL,maglimH), alpha=1.0)

            if (ann_magSL and not ann_mag):
                
                if (proj_dir == 'x'):
                    slc.annotate_streamlines('magy', 'magz')
                
                if (proj_dir == 'y'):
                    slc.annotate_streamlines('magx', 'magz')
        
                if (proj_dir == 'z'):
                    slc.annotate_streamlines('magx', 'magy')
            
            if (save):
                if (plot_name == None):
                    slc.save(plt_var+plt_file[file_num][-4:]+'.png')
                else:
                    slc.save(plot_name+plt_file[file_num][-4:]+'.png')
            else:
                slc.show()
            slc.plots.clear()

    # No particles, just plot the gas.
    else:
        
        if (plt_type=="slice"):
            slc = yt.SlicePlot(ds, proj_dir, plt_var,
                               center=center_loc)
        elif (plt_type=="projection"):
            slc = yt.ProjectionPlot(ds, proj_dir ,plt_var,
                               center=center_loc, weight_field=weight_variable)
        elif (plt_type=="max_proj"):
                slc = yt.ProjectionPlot(ds, proj_dir, plt_var,
                                        center=center_loc, method="mip")
        elif (plt_type=="thin_proj"):
            # Make a thin projection, averaging the density weighted data over +- 3 cells.
            
            # Boundary should be periodic for this.
            ds.periodicity=[True, True, True]
            # First make the left and right corner of the region based
            # on the full domain.
            left_corner = ds.domain_left_edge
            right_corner = ds.domain_right_edge
            # Now adjust the size of the region along the line of sight`.
            depth = 6*dd['dx'].min().in_units('cm')
            if (proj_dir=='x'):
                left_corner[0] = ds.quan(center_loc[0],'cm') - 0.5 * depth
                right_corner[0] = ds.quan(center_loc[0],'cm') + 0.5 * depth
            if (proj_dir=='y'):
                left_corner[1] = ds.quan(center_loc[1],'cm') - 0.5 * depth
                right_corner[1] = ds.quan(center_loc[1],'cm') + 0.5 * depth
            if (proj_dir=='z'):
                left_corner[2] = ds.quan(center_loc[2],'cm') - 0.5 * depth
                right_corner[2] = ds.quan(center_loc[2],'cm') + 0.5 * depth
            weight_variable = 'density'
            # Create the region
            region = ds.box(left_corner, right_corner) 
            slc = yt.ProjectionPlot(ds, proj_dir, plt_var, data_source=region,
                                    center=center_loc, weight_field=weight_variable)
        else: print "Error, improper plt_type given!"
        if (ann_mag):

            maglimL = 0.5
            maglimH = 0.7
            if (proj_dir == 'x'):
                slc.annotate_line_integral_convolution('magy', 'magz', lim=(maglimL,maglimH), alpha=1.0)

            if (proj_dir == 'y'):
                slc.annotate_line_integral_convolution('magx', 'magz', lim=(maglimL,maglimH), alpha=1.0)

            if (proj_dir == 'z'):
                slc.annotate_line_integral_convolution('magx', 'magy', lim=(maglimL,maglimH), alpha=1.0)

        if (ann_magSL and not ann_mag):
            
            if (proj_dir == 'x'):
                slc.annotate_streamlines('magy', 'magz')
            
            if (proj_dir == 'y'):
                slc.annotate_streamlines('magx', 'magz')

            if (proj_dir == 'z'):
                slc.annotate_streamlines('magx', 'magy')

        slc.annotate_timestamp(0.5,0.95, time_unit='yr')
        
        if (plt_cmap!=None):
            slc.set_cmap(plt_var, plt_cmap)
        
        slc.zoom(zoom_factor)
        
        if (plt_log==False):
            slc.set_log(plt_var, plt_log)
        if (zlim!=None):
            slc.set_zlim(plt_var, zlim[0], zlim[1])
        if (ann_velocity):
            slc.annotate_velocity()
            plt_var=plt_var+"_vel"
        if (save):
            if (plot_name == None):
                slc.save(plt_var+plt_file[file_num][-4:]+'.png')
            else:
                slc.save(plot_name+plt_file[file_num][-4:]+'.png')
        else:
            slc.show()
        slc.plots.clear()

    return
    
if (__name__=="__main__"):

    args = parser.parse_args()

    # Options for the plotting routine.
    zlim              = args.set_zlim 
    plt_var           = args.variable 
    plt_type          = args.plot_type 
    axis_dir          = args.plot_axis
    plt_log           = args.no_log
    zoom_factor       = args.zoom 
    ann_vel           = False 
    cen_loc           = np.array(args.center)
    center_on_com     = args.center_on_com
    center_on_most_massive = args.center_on_most_massive
    ann_sphere        = args.annotate_sphere
    #w_field           = None
    p_size            = args.p_size
    include_particles = args.include_particles
    plot_name         = args.plot_name
    weight_variable   = args.weight_variable
    ann_mag           = args.annotate_B_field
    ann_magSL         = args.annotate_B_lines
    track_ID          = args.track_ID
    debug             = args.debug
    file_dir          = args.directory
    start             = args.start_file_number
    end               = args.end_file_number

    # Make this the current working directory.
    os.chdir(file_dir)

    # Number of processors to use.
    nproc = args.np
    
    # Use checkpoint files?
    use_chk = args.use_chk
    if (use_chk):
        file_name = 'chk'
    else:
        file_name = 'plt_cnt'
    
    plt, start, end = gather_files(file_name, file_dir,
                 start=start, end=end, debug=debug)
    
    prt = None # We don't need to load part files, that is auto done now.

    if (weight_variable != None and plt_type != 'projection'):
        print "Error: can't weight a slice plot, only projections!"
        sys.exit()

    if (debug):
        print "start   =", start
        print "end     =", end
        print "# files =", len(plt)
        print "plt     =", plt

    num_files = len(plt)

    # Initialize MPI and setup for the run.
    rank, size, comm, local_files, \
           num_local_files, work_start, work_end \
            = setup_for_mpi_run(plt, num_files, debug=debug)

    if (rank == 0): 
        # print "Plotting", plt_type, "of", plt_var, "on", size, "processors starting at", plt[start], "and ending at", plt[end], \
        #    "at location", cen_loc
        # sys.stdout.flush()
        t0 = time.time()

    if (num_local_files > 0):
        print "Processor", rank, "plotting file", plt[work_start], "to", plt[work_end-1], "for total of", num_local_files, "plots."
        
        sys.stdout.flush()

        for i in range(num_local_files):

            local_file_index = i + work_start
            plot_func(plt, local_file_index, prt, proj_dir=axis_dir, plt_var=plt_var, 
                       plt_type=plt_type, save=True, zoom_factor=zoom_factor, zlim=zlim,
                       center_loc=cen_loc, plt_log=plt_log, ann_velocity=ann_vel, 
                       p_size=p_size, include_particles=include_particles, plot_name=plot_name,
                       weight_variable=weight_variable, ann_mag=ann_mag, 
                       ann_magSL=ann_magSL, center_on_com=center_on_com, 
                       center_on_most_massive=center_on_most_massive, 
                       track_ID=track_ID, debug=debug)
    
    comm.Barrier()
    
    if (rank == 0):
        t1 = time.time()
        print "Code took", t1-t0, "seconds to run with", size, "processors used." 

    sys.exit()
    comm.Abort()
