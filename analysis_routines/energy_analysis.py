# Imports

import numpy as np
import yt
yt.mylog.level=50
import pickle
from binary_analysis import setup_for_mpi_run, get_files
from load_flash_particles import *
from amuse.lab import units as u

from amuse.community.flash.binary_analysis.energy_routines import *

import argparse

parser = argparse.ArgumentParser(description="Run an analysis of energy \
                                 in the stars and gas in FLASH plot files. \
                                 This anaylsis script will identify clusters \
                                 in the data, then calculate the properties \
                                 in the region surrounding the clusters. \
                                 Written by: Joshua Wall, Drexel University")

parser.add_argument("-d", "--directory", default="./",
                   help="Input directory where the particle files are located. \
                         Default is './'")
parser.add_argument("-w", "--write_directory", default="./",
                   help="Output directory where the files are written. \
                         Default is './'")
parser.add_argument("-f", "--filename", default="*plt_cnt*",
                   help="Filename prefix (before the numbers). \
                         Default is *plt_cnt*.")
parser.add_argument("-o", "--out_filename", default="clusters",
                   help="Base filename for output files. \
                         Default is clusters.")
parser.add_argument("-db", "--debug", action="store_true",
                    help="Debug this script.")
parser.add_argument("-nh", "--num_hop", default=16, type=int,
                    help="Number of neighbors for Hop when actually hopping.")
parser.add_argument("-nd", "--num_density", default=64, type=int,
                    help="Number of neighbors for Hop during density calculation.")
parser.add_argument("-od", "--outer_density_limit", default=1.0, type=float,
                    help="Outer density limit in solar masses per parsec.")
parser.add_argument("-rf", "--refresh_hop", action="store_true",
                    help="Start and stop Hop on each loop. Not recommended.")


args        = parser.parse_args()
file_dir    = args.directory
file_name   = args.filename
out_dir     = args.write_directory
out_file    = args.out_filename
debug       = args.debug
refresh_hop = args.refresh_hop
num_hop     = args.num_hop
num_density = args.num_density
outer_density_limit = args.outer_density_limit | u.MSun*u.parsec**-3


if (debug): print "Starting up in debug mode.", debug

# Load files.
files = get_files(FLASH_file_dir=file_dir, FLASH_file_name=file_name)
num_files = len(files)

if (debug):
      print files
      print num_files

# Setup for MPI run.
rank, size, comm, local_files, \
    num_local_files, work_start, work_end = \
        setup_for_mpi_run(files, num_files, debug=debug)

if (debug): print local_files

if (not refresh_hop):
    # Setup a single hop worker for each processor.
    hop = setup_hop(outer_dens_limit=outer_density_limit,
                num_hop=num_hop,num_density=num_density)

if (debug): print hop.parameters

frac_done = 0.

for i,f in enumerate(local_files):

    # Progress tracker (at least on the root processor).
    if ( int((float(i) / float(num_local_files))*100.) >= frac_done and rank == size-1 ):
        print "Progress at {:.0f} %".format(frac_done)
        frac_done += 10. 

    # Load the gas data using yt.
    ds = yt.load(f)
    dd = ds.all_data()
    # Load the AMUSE star particles.
    stars = get_ps_from_yt(f)
    if (stars is None or len(stars) < num_density or len(stars) < num_hop): continue

    # Define the domain for the analysis
    # here if you want to restrict it.
    if (refresh_hop):
        groups, n_groups = find_clusters_with_hop(stars, num_hop=num_hop,
                                                num_density=num_density,
                                                outer_dens_limit=outer_density_limit,
                                                debug=debug)
    else:
        groups, n_groups = find_clusters_with_hop(stars, passed_hop=hop,
                                              debug=debug)

    # Calculate the total energy of the stars 
    # (KE + gas pot'l on stars + stars pot'l on stars).
    grp_com, grp_comv, grp_TM, \
    grp_radius, grp_KE, grp_gPE, \
    grp_sPE, grp_TE \
               = calculate_stellar_group_properties(ds, groups, debug=debug)

    # Calculate the total energy of the gas 
    # (KE + ME + TE + gas pot'l on the gas + stars pot'l on the gas).
    grp_gas_TM, grp_gas_KE, \
    grp_gas_EI, grp_gas_ME, \
    grp_gas_gPE, grp_gas_sPE, \
    grp_gas_TE \
               = calculate_gas_properties(ds, groups, grp_com,
                                          grp_radius, debug=debug)

    # Make plots.
    plot_clumps(groups, stars.mass.sum(),
                save_plts=True, plt_dir=out_dir,
                plt_name='clumpy'+f[-4:])

    # Store the data
    clusters = cluster_storage(groups, n_groups, 
                       grp_com, grp_comv, grp_TM,
                       grp_radius, grp_KE, grp_gPE,
                       grp_sPE, grp_TE,
                       grp_gas_TM, grp_gas_KE,
                       grp_gas_EI, grp_gas_ME,
                       grp_gas_gPE, grp_gas_sPE,
                       grp_gas_TE)

    # Now save all the results to a pickle file.
    with open(out_dir+'clusters'+f[-4:]+'.pickle', 'wb') as wf:
            pickle.dump(clusters, wf)
            wf.close()
# Stop the hop worker.
if (not refresh_hop): hop.stop()
print "Done!"