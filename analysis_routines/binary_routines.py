
from scipy.spatial.distance import cdist, pdist
from amuse.lab import units as u
import numpy as np

# To retrieve binary elements info
from amuse.community.kepler.interface import Kepler
from amuse.units.nbody_system import nbody_to_si

def get_energies_from_particles(particles, pm=None, pp=None, pv=None, debug=False):

    """Get the total energy, relative kinetic energy, and potential energy
    between every particle with respect to all other particles in the set.
    
    Generally takes an AMUSE particle set, but you can also pass directly
    the positions, masses and velocities of the particles if you pass
    particles == None.
    
    Returns matricies of the energies of each particle (on the 0 axis)
    with every other particle (on the 1 axis). The diagonial is zeroed.
    To get the total energy of the set, simply sum along one axis.
    
    Keywords:
    particles -- AMUSE particle set (set to None if using pm, pp, and pv)
    pm -- particle masses     (default = None)
    pp -- particle positions  (default = None)
    pv -- particle velocities (default = None)
    
    Returns:
    TE     -- total relative energies
    rel_ke -- relative kinectic energies
    pu     -- potential energies
    """
    pre = "get_energies_from_particles"
    if (debug): print pre, "entered."

    gc  = u.constants.G.value_in(u.cm**3 * u.g**-1 * u.s**-2)
    
    if (particles is None and (pm is None 
                               or pp is None 
                               or pv is None)):
        print "Error: particles and pm/pp/pv cannot be None."
    elif (particles is not None
          and (pm is not None
               or pp is not None 
               or pv is not None)):
        print "Error: received both an AMUSE \
               particle set and values for pm/pp/pv. I'm not \
               sure who to believe here!"
    elif (particles is not None):
        pm = particles.mass.value_in(u.g)
        pp = particles.position.value_in(u.cm)
        pv = particles.velocity.value_in(u.cm/u.s)
        
    lp = len(pm)

    print "Number of stars = {}".format(lp)

    # Relative particle KE
    # center of mass velocities

    rel_pke = np.zeros((lp,lp))
    pu      = np.zeros((lp,lp))

    for i in range(lp):
        
        for j in range(i+1,lp):
            
            mu = pm[i]*pm[j]/(pm[i]+pm[j])
            rel_vel = pv[i,:]-pv[j,:]
            rel_pke[i,j] = rel_pke[j,i] = 0.5*mu*np.sum(rel_vel**2.0)
            
            rel_dis = np.sqrt(np.sum((pp[i,:] - pp[j,:])**2.0))
            pu[i,j] = pu[j,i] = gc*pm[i]*pm[j]/rel_dis
            
            if (rel_dis < 1.):
                print "pu[i,j]=", pu[i,j]
                print "i,j=", i,j
                print "mass[i], mass[j]=", pm[i], pm[j]
                print "r=", rel_dis
                print "mu=", mu


    # Particle-Particle PE
    """    r = cdist(pp,pp)
    np.fill_diagonal(r,1e99)
    masses = np.outer(pm,pm)
    pu = yt.units.G.v*masses/r
    """
    np.fill_diagonal(pu,0)
    #ppe = 0.5*yt.units.G.v*(pu).sum(axis=0)
    
    TE = rel_pke - pu

    print "Max total energy = {}".format(TE.max())
    print "Min total energy = {}".format(TE.min())
    
    return TE, rel_pke, pu


def check_perturbed_binary(star1, star2, pert, debug=False):
    

    """Given a binary of star1 and star2 and a third perturber pert,
    determine the perturbation on the binary by comparing the
    difference in the accelerations of star1 by pert and star2
    by pert to the accelerations of star1 and star2 on each other.
    
    Keywords:
    star1 -- first member of binary
    star2 -- second member of binary
    pert  -- perturbing star
    
    Returns:
    gamma -- the ratio of perturb acceleration to member acceleration
    """
    pre = "check_perturbed_binary"
    if (debug): print pre, "entered."
    
    gc  = u.constants.G.value_in(u.cm**3 * u.g**-1 * u.s**-2)
    
    s1m = star1.mass.value_in(u.g)
    s1p = star1.position.value_in(u.cm)
    
    s2m = star2.mass.value_in(u.g)
    s2p = star2.position.value_in(u.cm)
    
    pm = pert.mass.value_in(u.g)
    pp = pert.position.value_in(u.cm)
    # Individual accelerations of the binary members on
    # each other.
    r12vec = s1p-s2p
    r12mag = np.sqrt(np.sum((s1p-s2p)**2.0))
    r21vec = s2p-s1p
    r21mag = np.sqrt(np.sum((s2p-s1p)**2.0))
    
    r32vec = pp-s2p
    r32mag = np.sqrt(np.sum((pp-s2p)**2.0))
    r31vec = pp-s1p
    r31mag = np.sqrt(np.sum((pp-s1p)**2.0))
    
    accel_1on2 = gc * s1m * r12vec / r12mag**3.
    accel_2on1 = gc * s2m * r21vec / r21mag**3.
    # Average binary mutual acceleration.
    mutual_accel = gc * 0.5*(s1m+s2m) / np.sum((s2p-s1p)**2.0)
    
    # Direct acceleration caluclations. This makes no assumptions about
    # the orbits, it just calculates the tides at the current binary
    # positions.
    # Pertuber acceleration on star1
    ps1a = gc *  pm * r31vec / r31mag**3.
    # Perturber acceleration on star2
    ps2a = gc *  pm * r32vec / r32mag**3.
        
    # Gamma based on averaging the internal binary acceleration
    gamma = (  np.sqrt(np.sum((ps2a - ps1a)**2.0)) 
            /  np.sqrt(np.sum((mutual_accel)**2.0))  )
    
    if (debug): print pre, "exit."
    return gamma
    
def find_all_binaries(stars, limiting_gamma=0.01, debug=False):
    
    """
    Given a particle set, find all mutally bound binaries
    that are not perturbed by a gamma larger than limiting_gamma.
    Return the particle set and a dictionary mutually_bnd, whose
    key / value pairs are the two indicies in the stars particle
    set of each mutually bound binary that was found.
    
    Keyword arguments:
    stars          -- AMUSE particle set
    limiting gamma -- the limiting ratio of perturber to binary
                      accelerations.
                      
    Returns:
    stars          -- AMUSE particle set.
    mutually_bnd   -- dictionary whose key / value pairs are the two 
                      indicies in the stars particle set of each 
                      mutually bound binary that was found.
    """
    pre = "find_all_binaries"
    if (debug): print pre, "entered."
    
    # First calculate all the relative energies of all stars
    # to each other.
    TE, rel_pke, pu = get_energies_from_particles(stars, debug=debug)
    
    # Now find all first pass mutually bound pairs.
    mul_im_bnd_to, mul_bnd_to_me, mutually_bnd = \
                            find_bound_relations(TE, stars, debug=debug)
    
    # Now test each pair for perturbers.
    
    if (debug):
        print "# of binaries before perturbation check =", \
            len(mutually_bnd.keys())

    check_binaries_for_perturbers(stars,
                                  mutually_bnd,
                                  limiting_gamma=limiting_gamma, debug=debug)
                
    if (debug): 
        print "# of binaries after perturbation check =", \
            len(mutually_bnd.keys())
        print pre, "exit."
    return stars, mutually_bnd

def find_bound_relations(TE, stars, debug=False):
    
    """Given a total relative energy matrix that has total energy
    TE[i,j] for the ith star's total energy relative to the jth
    star, find any jth star the ith star might be bound to and
    store in mul_im_bnd_to and any jth star bound to the ith
    star and store in mul_bnd_to_me.
    
    Keywords:
    
    TE  -- total relative energy matrix
    
    Returns:
    
    mul_im_bnd_to -- dictionary whose key is the index of the ith
                     star and whose values are the indices of all
                     stars the ith star is bound to.
    mul_bnd_to_me -- dictionary whose key is the index of the ith
                     star and whose values are the indices of all
                     stars that are bound to the ith star.
    mutally_bnd   -- dictionary whose key is the ith star and
                     whose (single) value is one star to which
                     it is both most bound and that is most bound
                     to it.
    """
    pre = "find_bound_relations"
    if (debug): print pre, "entered."
    
    # Note TE[:,i] in this loop returns the stars this star index/key
    # is bound to while TE[i,:] in this loop returns all stars bound
    # this index/key.
    mul_im_bnd_to = {}
    mul_bnd_to_me = {}

    pm = stars.mass.value_in(u.MSun)

    for i in range(len(TE[:,0])):

        bnd_ind = np.where(TE[:,i] < 0.0)[0]
        if len(bnd_ind > 0):
            mul_im_bnd_to[i] = bnd_ind[np.argsort(TE[bnd_ind,i])]

        bnd_ind = np.where(TE[i,:] < 0.0)[0]
        if len(bnd_ind > 0):
            mul_bnd_to_me[i] = bnd_ind[np.argsort(TE[i,bnd_ind])]
            
    # Now lets find any pairs that are mutually most bound.
    mutally_bnd = {}

    for key, val in mul_bnd_to_me.iteritems():
        # Do we love each other?
        other_key = val[0]
        #print other_key
        if (other_key in mul_bnd_to_me.keys()
            and other_key not in mutally_bnd.keys()):
            
            if (mul_bnd_to_me[other_key][0] == key):
                # Most massive star is always the key index,
                # the secondary (less massive) is always the value.
                if (pm[key] > pm[val[0]]):
                    mutally_bnd[key]=val[0]
                else:
                    mutally_bnd[val[0]]=key
    if (debug): print pre, "exit."
    return mul_im_bnd_to, mul_bnd_to_me, mutally_bnd



def check_binaries_for_perturbers(stars,
                                  mutually_bnd,
                                  limiting_gamma=0.01,
                                  debug=False):
    
    pre = "check_binaries_for_perturbers"
    if (debug): print pre, "entered."
    
    #stars.bp = None
    rm_keys = []
    for key, val in mutually_bnd.iteritems():
        
        star1 = stars[key]
        star2 = stars[val]
        
        other_stars = stars - star1 - star2
        
        perturbed_too_much = False

        for pert in other_stars:
            
            gamma = check_perturbed_binary(star1, star2, pert, debug=debug)
            if (gamma > limiting_gamma):
                perturbed_too_much = True
                #print "gamma = {}".format(gamma)
                break

        if (perturbed_too_much):
            # Add this key to a list to be removed
            # from the mutually_bnd dictionary.
            rm_keys.append(key)

        #else:
            ## Add this pair to the stars list.
            #stars[key].bp = val
            #stars[val].bp = key
        
    # Remove from mututally_bnd dict
    for k in rm_keys:
        mutually_bnd.pop(k, None)

    if (debug): print pre, "exit."
    return
    
def get_binary_number_fractions(stars, mutually_bnd, mass_bin_edges, debug=False):
    
    pre = "get_binary_number_fractions"
    if (debug): print pre, "entered."

    nbins           = len(mass_bin_edges)-1
    pm              = stars.mass.value_in(units.MSun)
    frac_in_bin     = np.zeros(nbins)
    mass_in_bin     = np.zeros(nbins) # total mass in bin
    num_in_mass_bin = np.zeros(nbins) # total # in bin
    bin_mass_in_bin = np.zeros(nbins) # binary mass in bin
    bin_num_in_bin  = np.zeros(nbins) # binary number in bin
    #mass_bin_edges  = np.zeros(nbins+1)

    mass_sorted_ind    = np.argsort(pm) #[::-1]
    sorted_m           = pm[mass_sorted_ind]
    tot_m              = sorted_m.sum()
    frac_m             = sorted_m/tot_m
    cul_m              = np.cumsum(frac_m)
    last_upper_bin_ind = 0

    #print cul_m

    #mass_bin_edges[:] = bin_edges[:]*tot_m/ms

    for i in range(nbins):

        #upper_bin_ind = np.where((bin_edges[i] < cul_m) &
        #                         (cul_m <= bin_edges[i+1]))[0]

        upper_bin_ind = np.where((mass_bin_edges[i] < sorted_m) &
                         (sorted_m <= mass_bin_edges[i+1]))[0]

        #print pm[mass_sorted_ind[upper_bin_ind]]
        
        # total number of all stars in this mass bin
        num_in_mass_bin[i] = float(len(upper_bin_ind))
        # the total mass in this bin
        mass_in_bin[i]     = sorted_m[upper_bin_ind].sum()
        #if (np.size(upper_bin_ind) > 0):
        #    mass_bin_edges[i+1] = sorted_m[upper_bin_ind[-1]]
        #else:
        #    mass_bin_edges[i+1] = mass_bin_edges[i]
        #last_upper_bin_ind = upper_bin_ind
        #print mass_sorted_ind[upper_bin_ind]

        cnt_m = 0.

        for key, val in mutually_bnd.iteritems():
            
            if (key in mass_sorted_ind[upper_bin_ind]):
                cnt_m += 1.
                bin_mass_in_bin[i] += pm[key]
            #if (val in mass_sorted_ind[upper_bin_ind]):
            #    cnt_m += 1.
            #    bin_mass_in_bin[i] += pm[val]

        if (cnt_m > 0):
            frac_in_bin[i] = cnt_m/num_in_mass_bin[i]
            bin_num_in_bin[i] = cnt_m
            
    if (debug): print pre, "exit."
    return (frac_in_bin, bin_num_in_bin, num_in_mass_bin,
            bin_mass_in_bin, mass_in_bin)

def get_binary_mass_ratios(stars, mutually_bnd, debug=False):
    
    pre = "get_binary_mass_ratios"
    if (debug): print pre, "entered."
    
    pm  = stars.mass.value_in(u.MSun)
    age = stars.age.value_in(u.yr) 
    nb  = len(mutually_bnd)
    tags_bnd = []
    q  = np.zeros(nb)
    s1 = np.zeros(nb)
    s2 = np.zeros(nb)
    pa = np.zeros(nb)
    sa = np.zeros(nb)
    i  = 0

    for key, val in mutually_bnd.iteritems():

        m1 = pm[key]
        m2 = pm[val]

        # s1 is the primary, s2 is the companion.
        if (m1 > m2):
            s1[i] = m1
            s2[i] = m2
            pa[i]  = age[key]
            sa[i]  = age[val]
            tags_bnd.append(`stars.tag[key]`+'_'+`stars.tag[val]`)

        else:         
            s2[i] = m1
            s1[i] = m2
            pa[i]  = age[val]
            sa[i]  = age[key]
            tags_bnd.append(`stars.tag[val]`+'_'+`stars.tag[key]`)
        i = i+1    
    
    q = s2/s1
    if (debug): print pre, "exit."
    return q, s1, s2, pa, sa, tags_bnd


# My wrapper for the function get_component_binary_elements
def get_com_bin_elements(stars, mutually_bnd, pass_kep=None, debug=False):
    
    pre = "get_com_bin_elements"
    if (debug): print pre, "entered."
    
    n_of_b   = len(mutually_bnd.keys()) 
    bin_mass = np.zeros(n_of_b)
    a        = np.zeros(n_of_b)
    e        = np.zeros(n_of_b)
    r        = np.zeros(n_of_b)
    E        = np.zeros(n_of_b)
    t        = np.zeros(n_of_b)
    
    if (pass_kep is None):
        if (debug): print pre, "is making its own Kepler."
        converter = nbody_to_si(1.0| u.cm, 1.0 | u.g)
        kep = Kepler(unit_converter=converter)
    else:
        if (debug): print pre, "is using a passed Kepler."
        kep = pass_kep

    if (debug): print pre, "Number of binaries here is", len(mutually_bnd.keys())
    
    i=0
    for key, val in mutually_bnd.iteritems():
        
        star1 = stars[key]
        star2 = stars[val]
        # t for temporary
        tm,ta,te,tr,tempE,tt = \
            get_component_binary_elements(star1, star2, kep, debug=debug)
        # strip units so we can use numpy arrays
        bin_mass[i]=tm.value_in(u.g)
        a[i]=ta.value_in(u.cm)
        e[i]=te
        r[i]=tr.value_in(u.cm)
        E[i]=tempE.value_in(u.erg/u.g)*bin_mass[i]
        t[i]=tt.value_in(u.s)
        i += 1
    
    if (pass_kep is None):
        if (debug): print pre, "is stopping Kepler."
        kep.stop()
    
    if (debug): print pre, "exit."
    return bin_mass,a,e,r,E,t

### Straight out of the multiples module.
### Requires you to pass a Kepler worker.

def get_component_binary_elements(comp1, comp2, kep, peri = 0, debug=False):

    pre = "get_component_binary_elements"
    if (debug): print pre, "entered."
    
    mass = comp1.mass + comp2.mass
    pos = comp2.position - comp1.position
    vel = comp2.velocity - comp1.velocity
    kep.initialize_from_dyn(mass, pos[0], pos[1], pos[2],
                            vel[0], vel[1], vel[2])
    a,e = kep.get_elements()
    r = kep.get_separation()
    E,J = kep.get_integrals()		# per unit reduced mass, note
    if peri:
        M,th = kep.get_angles()
        if M < 0:
            kep.advance_to_periastron()
        else:
            kep.return_to_periastron()
    # Except here, cause I don't want an "evolution time"
    #t = kep.get_time()
    t = kep.get_period() # instead I'd like the period, thanks!

    if (debug): print pre, "exit."
    return mass,a,e,r,E,t

def m_from_at(a,t):
    
    """
    Get the total binary mass from Kepler's 3rd law.
    Assumes cgs units.
    
    Keyword arguments:
    a -- semi-major axis
    t -- period of the orbit
    
    Returns:
    m -- sum of the binary masses
    """
    
    gc  = u.constants.G.value_in(u.cm**3 * u.g**-1 * u.s**-2)
    
    print a/au
    print t/dy
    
    m   = 4.0*np.pi**2.0*a**3. / (gc*t**2.0)
    return m

def a_from_mt(m,t):
    gc  = u.constants.G.value_in(u.cm**3 * u.g**-1 * u.s**-2)

    return (t**2.0*gc*m / (4.0*np.pi**2.))**(1./3.)

def t_from_ma(m,a):
    gc  = u.constants.G.value_in(u.cm**3 * u.g**-1 * u.s**-2)
    
    return np.sqrt(4.0*np.pi**2.0*a**3.0/(gc*m))
    
def get_all_binary_properties(stars, mutually_bnd,
                              test_binary_ecc=True,
                              pass_kep=None, debug=False):
    
    """ Gets all of the binary properties for a given
        particle set and its accompaning dictionary
        of mutually bound binaries. 
        
        *** Note this routine by default will modify 
        in place the mutually_bnd dictionary to remove 
        any binaries that have and eccentricity greater than 1. ***
         
        Keyword arguments
        stars        -- AMUSE particle set (all of the stars)
        mutually_bnd -- A dictionary with key / value pairs that are
                         the indicies of the stars which are mutually
                         bound to one another (binaries).
        test_binary_ecc -- Whether to modify the mutually_bnd dictionary
                            to remove any binary with an eccentricity
                            greater than 1. Default is true.
        
        Returns -- 
        q    -- binary mass ratio s2/s1, where s1 is the primary and s2 is
                the secondary masses.
        s1   -- primary star mass (the more massive binary).
        s2   -- secondary star mass
        page -- primary star age
        sage -- secondary star age
        bm   -- binary total mass
        a    -- semi-major axis
        e    -- eccentricity
        r    -- current binary separation
        En   -- binary total energy
        t    -- binary period
        nb   -- total number of binaries
    """
    
    pre = "get_all_binary_properties"
    if (debug): print pre, "entered."

    q, s1, s2, page, sage, tags_bnd = \
           get_binary_mass_ratios(stars, mutually_bnd, debug=debug)
    
    bm,a,e,r,En,t = \
           get_com_bin_elements(stars, mutually_bnd, pass_kep=pass_kep,debug=debug)
    
    if (test_binary_ecc): # Correct for any e > 1?
        # If your e > 1, you're not a bound binary.
        e_lt_1  = np.where(e<=1.0)[0]
        e_gtr_1 = np.where(e>1.0)[0].tolist()
        
        q    = q[e_lt_1]
        s1   = s1[e_lt_1]
        s2   = s2[e_lt_1]
        page = page[e_lt_1]
        sage = sage[e_lt_1]
        bm   = bm[e_lt_1]
        a    = a[e_lt_1]
        e    = e[e_lt_1]
        r    = r[e_lt_1]
        En   = En[e_lt_1]
        t    = t[e_lt_1]
        
        # Note keys in mutually_bnd are indicies to the stars array.
        if (len(e_gtr_1) > 0): print "[get_all_binary_properties]: Removing", \
                              len(e_gtr_1), "binaries from mutually_bnd", \
                              "for having e > 1."      
        my_keys = mutually_bnd.keys()
        for k in e_gtr_1:
            mutually_bnd.pop(my_keys[k])
    
    nb = len(mutually_bnd.keys())
    
    if (debug): print pre, "exit."
    return q, s1, s2, page, sage, bm, a, e, r, En, t, nb, tags_bnd

