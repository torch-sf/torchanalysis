"""
energy_analysis2.py
Joshua Wall
Drexel University
January 2019

In this version of energy analysis
we use my new task based parallelization
routines.
Note that this is only really useful for running
on a large number of processors, where having
a manager process that does nothing but manage
work outweighs the cost of losing that work.
This is ESPECIALLY useful when the chunks of
work are of uneven sizes.
"""

# Imports

import numpy as np
import yt
yt.mylog.level=50
import pickle

from load_flash_particles import get_ps_from_yt
from amuse.lab import units as u

from analysis_routines.energy_routines import *
from analysis_routines.analysis_utilities import *
from analysis_routines.task_based_processing import *

import argparse

parser = argparse.ArgumentParser(description="Run an analysis of energy \
                                 in the stars and gas in FLASH plot files. \
                                 This anaylsis script will identify clusters \
                                 in the data, then calculate the properties \
                                 in the region surrounding the clusters. \
                                 Written by: Joshua Wall, Drexel University")

parser.add_argument("-d", "--directory", default="./",
                   help="Input directory where the particle files are located. \
                         Default is './'")

parser.add_argument("-w", "--write_directory", default="./",
                   help="Output directory where the files are written. \
                         Default is './'")

parser.add_argument("-f", "--filename", default="*plt_cnt*",
                   help="Filename prefix (before the numbers). \
                         Default is *plt_cnt*.")

parser.add_argument("-o", "--out_filename", default="clusters",
                   help="Base filename for output files. \
                         Default is clusters.")

parser.add_argument("-cf", "--cluster_finder", default="hop",
                   choices=["hop", "dbscan", "none"],
                   help="Algorithm used to locate clusters. \
                         'none' means just treat all the stars \
                         as a single group. \
                         Default is dbscan.")

parser.add_argument("-db", "--debug", action="store_true",
                    help="Debug this script.")

parser.add_argument("-nh", "--num_hop", default=16, type=int,
                    help="Number of neighbors for Hop when actually hopping.")

parser.add_argument("-nd", "--num_density", default=64, type=int,
                    help="Number of neighbors for Hop during density calculation.")

parser.add_argument("-rsd", "--relative_saddle_density", action="store_true",
                    help="Use Hop's relative saddle density.")

parser.add_argument("-rsdt", "--relative_saddle_density_thres", default=0.5, type=float,
                    help="The ratio of the saddle density to the min peak density \
                          for determining when to merge to clusters into one \
                          using the relative saddle density threshold.")

parser.add_argument("-od", "--outer_density_limit", default=1.0, type=float,
                    help="Outer density limit in solar masses per parsec.")


parser.add_argument("-c", "--chunk_size", default=1, type=int,
                    help="Number of chunks to send to each worker process.")

parser.add_argument("-rf", "--refresh_hop", action="store_true",
                    help="Start and stop Hop on each loop. Not recommended.")

parser.add_argument("-s", "--start_file_number", default=None, type=int,
                    help="The starting number of the files you want to \
                          plot. By default its None, which picks the first file \
                          in the folder.")

parser.add_argument("-e", "--end_file_number", default=None, type=int,
                    help="The ending number of the files you want to \
                          plot. By default its None, which picks the \
                          last file in the folder.")

parser.add_argument("-p", "--save_plts", action="store_true",
                    help="Store plot files of the clusters.")

parser.add_argument("-bbn", "--bin_by_number", action="store_true",
                    help="For calculating the half mass \
                        radii of stars binned by mass, \
                        should we bin by equal mass or \
                        instead bin by equal number of stars?.")
parser.add_argument("-fb", "--frac_bin", default=-1.0, type=float,
                    help="Value of lagrangian bin mass fraction to be used \
                        for a cluster's radius. Values must be float between \
                        0.0 and 1.0 not including. Default is -1.0 which uses \
                        the .total_radius() amuse data property for each cluster.")


args        = parser.parse_args()
file_dir    = args.directory
file_name   = args.filename
start       = args.start_file_number
end         = args.end_file_number
out_dir     = args.write_directory
out_file    = args.out_filename
save_plts   = args.save_plts
debug       = args.debug

# Clustering args.
cluster_finder                = args.cluster_finder
refresh_hop                   = args.refresh_hop
num_hop                       = args.num_hop
num_density                   = args.num_density
outer_density_limit           = args.outer_density_limit | u.MSun*u.parsec**-3
relative_saddle_density       = args.relative_saddle_density
relative_saddle_density_thres = args.relative_saddle_density_thres
# For calculating the half mass radii of stars binned by mass,
# should we bin by equal mass or instead bin by equal number
# of stars?
bin_equal_numbers_of_stars = args.bin_by_number
chunk_size  = args.chunk_size
frac_bin = args.frac_bin


if (debug): print "Starting up in debug mode.", debug

# Setup for MPI run.
rank, size, comm = initialize_mpi()

pre = "Proc", rank

# Load files.

if (rank==0):
    if (debug):
        print "Calling gather files"
        print "file_name = ", file_name
        print "file_dir  = ", file_dir
        print "start     = ", start
        print "end       = ", end
    files, start, end = gather_files(file_name, file_dir,
                        start=start, end=end, debug=debug)
    num_files = len(files)

    if (debug):
        print files
        print num_files
else:
    files=None

hop=None
if (cluster_finder == "hop"):
    # No Hop needed on the root as he's
    # not doing any analysis here.
    if (not refresh_hop and (rank != 0)):
        # Setup a single hop worker for each processor.
        hop = setup_hop(
            num_hop=num_hop,num_density=num_density,
            outer_density_limit=outer_density_limit,
            relative_saddle_density=relative_saddle_density,
            relative_saddle_density_thres=relative_saddle_density_thres,
            debug=debug
            )
        if (debug): print hop.parameters



fargs = []
fkwargs = {
    'cluster_finder':cluster_finder,
    'num_hop':num_hop,
    'num_density':num_density,
    'outer_density_limit':outer_density_limit,
    'relative_saddle_density':relative_saddle_density,
    'relative_saddle_density_thres':relative_saddle_density_thres,
    'passed_hop':hop,
    'out_dir':out_dir,
    'out_file':out_file,
    'debug':debug,
    'save_plts':save_plts,
    'bin_equal_numbers_of_stars':bin_equal_numbers_of_stars,
    'frac_bin':frac_bin
    }

if (rank == 0):
    print "Options selected are:"
    print fkwargs

perform_task_in_parallel(
    run_energy_analysis_on_batch_of_files,
    fargs, fkwargs, files, chunk_size,
    rank, size, comm, root=0, debug=debug)

# Stop the hop worker.
if (cluster_finder == "hop" and not refresh_hop and (rank !=0)): hop.stop()
print pre, "Done!"
