import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import scipy.interpolate as spi
from amuse.lab import *
import glob
import h5py
# Note MPI_Init is called when mpi4py is imported.
# MPI_Finalize is called when the script ends.
from mpi4py import MPI
import pickle # Save the results.
import time as pt
import argparse


parser = argparse.ArgumentParser(description=" Calculate the location of the \
                                 density center of stars in Flash particle files. \
                                 Written by: Joshua Wall, Drexel University")
                                 
parser.add_argument("-d", "--directory", default=".",
                   help="Input directory where the particle files are located. \
                         Default is '.' Note we assume the particle files look like *part*.")

parser.add_argument("-w", "--write_directory", default=".",
                   help="Output directory where the files are written. \
                         Default is '.'")

parser.add_argument("-f", "--filename", default="*part*",
                   help="Filename prefix (before the numbers). \
                         Default is *part*.")

parser.add_argument("-s", "--stars_only", action="store_true",
                    help="Calculate only on star particles, excluding sink particles.")

parser.add_argument("-db", "--debug", action="store_true",
                    help="Debug this script.")

# Initialize MPI
comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()

if (rank == 0):
    t1 = pt.time()

args       = parser.parse_args()
file_dir   = args.directory
file_name  = args.filename
out_dir    = args.write_directory
stars_only = args.stars_only
debug      = args.debug

pre = "Proc", rank

if (debug): print "Size =", size
if (debug): print "Rank =", rank

files = glob.glob(file_dir+'/'+file_name)
files.sort()

num_files = len(files)

if (debug): print pre, "num_files=", num_files

# Calculate beginning and ending index for part
# of the files array that each processor will
# compute on.

# Find the number of files each processor will work on.
base_work_size = int(np.ceil(num_files/size))

if (debug):
    print pre, "Base unit of work =", base_work_size
else:
    if (rank == 0): print "Base unit of work =", base_work_size

# Make an array of starting and ending indices for
# each processor.
work_size_array = np.arange(size+1)*base_work_size

if (debug):
    print pre, "Work array =", work_size_array
else:
    if (rank == 0): print "Work array =", work_size_array

# Push any left overs onto the last processor.
work_size_array[-1] = num_files-1

if (debug):
    print pre, "Work array =", work_size_array
else:
    if (rank == 0): print "Work array =", work_size_array

# Check that all of these files are accessable.
#for w in work_size_array:
#    print files[w]

# Set this local processors start and end points.
work_start = work_size_array[rank]
work_end   = work_size_array[rank+1]

local_files     = files[work_start:work_end]
num_local_files = len(local_files)

if (debug):
    print pre, "work_start=", work_start
    print pre, "work_end  =", work_end
    print pre, "num_local_files=", num_local_files

# Initialize the array to hold the density center locations.
dens_center = np.zeros( (num_files, 3) )
# Array for the accretion rate of sink particles.
mdot = np.zeros(num_files)
# Array for total stellar mass.
msum = np.zeros(num_files)
# Array for current simulation time.
time = np.zeros(num_files)
# Array for the total sink mass.
sink_mass = np.zeros(num_files)

# Dictionaries to record all of the stars
# made during the run and their creation times.
# Just going to move this to a different code.
#mass_all_stars = {}
#creation_time_all_stars = {}

# Process the local parts of the files.

for i, f in enumerate(local_files):
    
    try:
        del(ds)
    except NameError:
        pass
    
    if (debug): print pre, "I'm on step", i+work_start
    
    ds = h5py.File(f, 'r')
    
    part_data  = ds.get('tracer particles')
    part_names = ds.get('particle names')
    real_scalars = ds.get('real scalars')
    time[i+work_start] = real_scalars[0][1]
    part_data = np.array(part_data)
    part_names = np.array(part_names)
    
    if (part_data.size > 1):
        
        mass_ind = np.where(np.char.strip(part_names)=='mass')[0]
        mdot_ind = np.where(np.char.strip(part_names)=='accr_rate')[0]
        tag_ind  = np.where(np.char.strip(part_names)=='tag')[0]
        type_ind = np.where(np.char.strip(part_names)=='type')[0]
        ct_ind   = np.where(np.char.strip(part_names)=='creation_time')[0]
        x_ind = np.where(np.char.strip(part_names)=='posx')[0]
        y_ind = np.where(np.char.strip(part_names)=='posy')[0]
        z_ind = np.where(np.char.strip(part_names)=='posz')[0]
        vx_ind = np.where(np.char.strip(part_names)=='velx')[0]
        vy_ind = np.where(np.char.strip(part_names)=='vely')[0]
        vz_ind = np.where(np.char.strip(part_names)=='velz')[0]

        # Get the particle types
        type_part = part_data[:,type_ind]
        if (stars_only):
            # Find only star particles (particle_type=1)
            star_ind = np.where(type_part==1)[0]
        else:
            # Use stars and sinks ("prestellar objects").
            star_ind = np.array(range(len(type_part))).flatten()
        # Find only sink particles (particle_type=2)
        sink_ind = np.where(type_part==2)[0]

        # Masses of stars 
        mass_part  = part_data[star_ind,mass_ind].ravel()
        # Number of stars
        num_stars  = len(mass_part) 
        
        # Make an AMUSE particle set to compute on.
        stars      = Particles(num_stars)
        stars.x    = part_data[star_ind,x_ind].ravel()    | units.cm
        stars.y    = part_data[star_ind,y_ind].ravel()    | units.cm
        stars.z    = part_data[star_ind,z_ind].ravel()    | units.cm
        stars.vx   = part_data[star_ind,vx_ind].ravel()   | units.cm / units.s
        stars.vy   = part_data[star_ind,vy_ind].ravel()   | units.cm / units.s
        stars.vz   = part_data[star_ind,vz_ind].ravel()   | units.cm / units.s
        stars.mass = part_data[star_ind,mass_ind].ravel() | units.g

        # Compute the kinetic and potential energies of the stars.
        KE = stars.kinetic_energy().in_(units.g*units.cm**2/units.s**2)
        PE = stars.potential_energy().in_(units.g*units.cm**2/units.s**2)
        TE = KE+PE  # total energy
        MT = stars.mass.sum() # total mass

        # Record the total mass of all stars.
        msum[i+work_start] = MT.value_in(units.g)
        # Record the accretion rates of the sinks.
        mdot[i+work_start] = part_data[sink_ind,mdot_ind].sum() # units are g/s 
        # Record the total mass of all sinks.
        sink_mass[i+work_start] = part_data[sink_ind,mass_ind].sum() # units are g
        
        # Do we have enough stars to calculate nearest neighbors?
        if (num_stars > 20):
            
            # AMUSE unit converter
            cv = nbody_system.nbody_to_si(MT, TE)
            # Note: Uses HOP, which is going to spawn a worker process.
            # This means for doing this analysis on Cartesius, call mpiexec with half the processors
            # you have available for this script (or the job will fail).
            dc, cr, cd = stars.densitycentre_coreradius_coredens(unit_converter=cv, reuse_hop=True)
            #dc, cr, cd = stars.densitycentre_coreradius_coredens(unit_converter=cv, reuse_hop=False)
            #print "dc=",dc
            dens_center[i+work_start,:] = dc.value_in(units.cm)
            #p = stars.cluster_core(unit_converter=cv, reuse_hop=True)
            #print p.position
            #dens_center[i,:] = p.position.value_in(units.cm)
            
            del(dc, cr, cd)
            #del(p)
            del(stars)
            del(cv)

        # Moved to a separate code.
        # Tags of stars
        #tags_part  = part_data[star_ind,tag_ind]
        # Creation times of stars
        #ctime_part = part_data[star_ind,ct_ind]
        #
        # Now add any particle who we haven't added previously to the dictionaries.
        #for i, tag in enumerate(tags_part):
        #    
        #    if tag not in mass_all_stars:
        #        
        #        mass_all_stars[tag]          = mass_part[i]
        #        creation_time_all_stars[tag] = ctime_part[i]

# Reduce the finished data on the root process. Note sum is the default operation.
# Note that a capital letter on the method means this is for numpy arrays!
recv_dens_array = np.zeros_like(dens_center)
comm.Reduce(dens_center, recv_dens_array, root=0)
dens_center = recv_dens_array
del(recv_dens_array)

recv_msum_array = np.zeros_like(msum)
comm.Reduce(msum, recv_msum_array, root=0)
msum = recv_msum_array
del(recv_msum_array)

recv_mdot_array = np.zeros_like(mdot)
comm.Reduce(mdot, recv_mdot_array, root=0)
mdot = recv_mdot_array
del(recv_mdot_array)

recv_sink_mass_array = np.zeros_like(sink_mass)
comm.Reduce(sink_mass, recv_sink_mass_array, root=0)
sink_mass = recv_sink_mass_array
del(recv_sink_mass_array)

recv_time_array = np.zeros_like(time)
comm.Reduce(time, recv_time_array, root=0)
time = recv_time_array
del(recv_time_array)

if (debug):
    print pre, "done with analysis. Now writing data."
    print pre, "dens_center[0] =", dens_center[0]
else:
    if (rank == 0):
        print "Done with analysis. Now writing data."

if (rank == 0):

    # Fill the zero locations that exist before there where particles with the first particle location.
    dens_center_first_non_zero = dens_center[np.where(dens_center[:,0] != 0.0)[0][0],:]
    dens_center[np.where(dens_center[:,0] == 0.0),:] = dens_center_first_non_zero

    # Store this numpy array as a pickled object.
    with open(out_dir+'/dens_center.pickle', 'wb') as wf:
        pickle.dump(dens_center, wf)

    with open(out_dir+'/msum.pickle', 'wb') as wf:
        pickle.dump(msum, wf)

    with open(out_dir+'/mdot.pickle', 'wb') as wf:
        pickle.dump(mdot, wf)

    with open(out_dir+'/sink_mass.pickle', 'wb') as wf:
        pickle.dump(sink_mass, wf)

    with open(out_dir+'/time.pickle', 'wb') as wf:
        pickle.dump(time, wf)

comm.Barrier()

if (rank == 0):

    t2 = pt.time()
    # Done!
    print "Finished writing data."
    print "Time to run was", t2 - t1

