##############################
##
##  Helper routines for taking
##  particle sets from FLASH
##  data files and loading it
##  into AMUSE particle sets.
##
##############################


import yt
import numpy as np
import h5py
from amuse.lab import Particles
from amuse.lab import units as u
from amuse.lab import write_set_to_file
import glob


# Routines for processing a set of files from an entire FLASH run
# and storing the particle sets.

def convert_all_particle_files_to_AMUSE_particle_sets(particle_type='all',
                                                      FLASH_file_dir='.',
                                                      FLASH_file_name='*_part_*',
                                                      AMUSE_file_dir='.',
                                                      AMUSE_file_name='stars'):
                                                          
    """ Takes as input the location of a set of FLASH particle files and
        then converts these files to AMUSE particle sets, writing the sets
        to the AMUSE particle hdf5 format for later processing.
        
        Keyword arguments:
        particle_type -- the type of particle that you
                         want from FLASH. Can be:
                         star -- active FLASH particle type
                         sink -- sink FLASH particle type
                         any other string or none returns all particle types.
                         The default is all.
        FLASH_file_dir  -- location of the FLASH particle files
        FLASH_file_name -- name of the FLASH particle (or checkpoint) files
        AMUSE_file_dir  -- location to store the final data
        AMUSE_file_name -- base name for the AMUSE particle sets
        
        Returns:
        Nothing.
    """
    
    files = glob.glob(FLASH_file_dir+FLASH_file_name)
    files.sort() # Ensure they are in numerical order.
    
    for f in files:
        temp_stars = get_ps_from_hdf5(f, particle_type=particle_type)
        if (temp_stars is None): continue
        # Here we use the FLASH file number (f[-4:]) to also number the particle sets.
        temp_stars_filename = AMUSE_file_dir+AMUSE_file_name+f[-4:]+'.hdf5'
        write_set_to_file(temp_stars, temp_stars_filename, format='hdf5')
    
    print "Done writing FLASH particles in", FLASH_file_dir, \
          "to AMUSE particle sets in", AMUSE_file_dir
    return

# Routines for loading a single particle set from a single file.

def get_ps_from_hdf5(partfile, particle_type='all', no_star_warning=False):
    
    """ Returns an AMUSE particle set from a FLASH
        hdf5 particle file (NOT a plot file) or checkpoint file.
        
        Keyword arguments:
        partfile      -- the FLASH particle file
        particle_type -- the type of particle that you
                         want from FLASH. Can be:
                         star -- active FLASH particle type
                         sink -- sink FLASH particle type
                         any other string or none returns all particle types.
                         The default is all.
                         
        Returns:
        stars -- An AMUSE particle set.
    """
    
    stars = None

    ds = h5py.File(partfile, 'r')
    
    part_data    = ds.get('tracer particles')
    part_names   = ds.get('particle names')
    part_data    = np.array(part_data)
    part_names   = np.array(part_names)
    real_scalars = ds.get('real scalars')
    part_time    = real_scalars[0][1]
    
    if (part_data.size > 1):
        
        mass_ind  = np.where(np.char.strip(part_names)=='mass')[0]
        tag_ind   = np.where(np.char.strip(part_names)=='tag')[0]
        type_ind  = np.where(np.char.strip(part_names)=='type')[0]
        ct_ind    = np.where(np.char.strip(part_names)=='creation_time')[0]
        posx_ind  = np.where(np.char.strip(part_names)=='posx')[0]
        posy_ind  = np.where(np.char.strip(part_names)=='posy')[0]
        posz_ind  = np.where(np.char.strip(part_names)=='posz')[0]
        velx_ind  = np.where(np.char.strip(part_names)=='velx')[0]
        vely_ind  = np.where(np.char.strip(part_names)=='vely')[0]
        velz_ind  = np.where(np.char.strip(part_names)=='velz')[0]
        
        # Get the particle types
        type_part = part_data[:,type_ind].flatten()
        
        if (particle_type == 'star' or particle_type == 'stars'):
            # Find only star particles (particle_type=1)
            star_ind = np.where(type_part==1)[0]
        elif (particle_type == 'sink' or particle_type == 'sinks'):
            # Find only sink particles (particle_type=2)
            star_ind = np.where(type_part==2)[0]
        elif (particle_type == 'all'):
            star_ind = np.arange(len(type_part))
        else:
            print "WARNING: Requested particle type not recognized." \
                  " Defaulting to returning all particle types in file!"
            star_ind = np.arange(len(type_part))
        
        num_parts = len(star_ind)
        if (num_parts == 0): return stars
        
        # allocate the array
        stars = Particles(num_parts)
        
        # Masses of stars
        stars.mass = part_data[star_ind,mass_ind] | u.g
        # Tags of stars
        stars.tag  = part_data[star_ind,tag_ind]
        # Creation times of stars
        stars.ct   = part_data[star_ind,ct_ind]   | u.s
        # Current hydro time in FLASH.
        stars.ht   = part_time | u.s
        # Actual age of the stars.
        stars.age  = stars.ht - stars.ct
        # Positions
        stars.x = part_data[star_ind,posx_ind]    | u.cm
        stars.y = part_data[star_ind,posy_ind]    | u.cm
        stars.z = part_data[star_ind,posz_ind]    | u.cm
        # Velocities
        stars.vx = part_data[star_ind,velx_ind]   | u.cm/u.s
        stars.vy = part_data[star_ind,vely_ind]   | u.cm/u.s
        stars.vz = part_data[star_ind,velz_ind]   | u.cm/u.s
        # Set an index attribute. Useful for some functions/codes in AMUSE.
        stars.index_of_the_particle = np.arange(num_parts)

    
    else:
        if (no_star_warning): print "Error: No particles found in this file!"
    
    return stars


def get_ps_from_yt(pltfile, particle_type="all"):
    
    """Take a FLASH plotfile and return an AMUSE
       particle set.
       
       Keyword arguments:
       pltfile      -- The FLASH hdf5 plot or checkpoint file.
                       Note a plotfile means there must also be
                       a particle file with the same ID number.
       particle_type -- the type of particle that you
                         want from FLASH. Can be:
                         star -- active FLASH particle type
                         sink -- sink FLASH particle type
                         any other string or none returns 
                         all particle types.
                         The default is all
       
       Returns:
       stars -- An AMUSE particle set.
    """
    
    stars = None

    ds = yt.load(pltfile)
    dd = ds.all_data()

    # Test for the presence of particles. If none, jump to exception.
    if (getattr(ds.fields, 'io', None) != None):
    
        all_parts_mass = dd['particle_mass'].in_units('g')
        all_parts_pos  = dd['particle_position'].in_units('cm')
        all_parts_vel  = dd['particle_velocity'].in_units('cm/s')
        
        if (particle_type == 'star' or particle_type == 'stars'):
            # Index for only the star particles, and not including sink particles.
            mass_ind = np.where(dd['particle_type'].v == 1)
        elif (particle_type == 'sink' or particle_type == 'sinks'):
            mass_ind = np.where(dd['particle_type'].v == 2)
        elif (particle_type == 'all'):
            mass_ind = np.arange(len(all_parts_mass))
        else:
            print "WARNING: Requested particle type not recognized." \
                    " Defaulting to returning all particle types in file!"
            mass_ind = np.arange(len(all_parts_mass))
            
        all_parts_mass = all_parts_mass[mass_ind]
        all_parts_pos  = all_parts_pos[mass_ind]
        all_parts_vel  = all_parts_vel[mass_ind]
            
        num_parts = len(all_parts_mass.v)
        
        stars = Particles(num_parts)
        stars.mass     = all_parts_mass.v | u.g
        # Creation times of stars
        stars.ct   = dd['particle_creation_time'][mass_ind].v   | u.s
        # Current hydro time in FLASH.
        stars.ht   = ds.current_time.v | u.s
        # Actual age of the stars.
        stars.age  = stars.ht - stars.ct
        # Positions
        stars.x = dd['particle_position_x'][mass_ind].v    | u.cm
        stars.y = dd['particle_position_y'][mass_ind].v    | u.cm
        stars.z = dd['particle_position_z'][mass_ind].v    | u.cm
        # Velocities
        stars.vx = dd['particle_velocity_x'][mass_ind].v   | u.cm/u.s
        stars.vy = dd['particle_velocity_y'][mass_ind].v   | u.cm/u.s
        stars.vz = dd['particle_velocity_z'][mass_ind].v   | u.cm/u.s
        #stars.position = all_parts_pos.v  | u.cm
        #stars.velocity = all_parts_vel.v  | u.cm/u.s
        stars.index_of_the_particle = range(num_parts)
        stars.tag      = dd['particle_tag'][mass_ind]
        
    return stars


def get_nparrays_from_hdf5(partfile, particle_type="all"):
    
    """ Returns an set of numpy arrays from a FLASH
        hdf5 particle file (NOT a plot file) or checkpoint file.
        
        Keyword arguments:
        partfile      -- the FLASH particle file
        particle_type -- the type of particle that you
                         want from FLASH. Can be:
                         star -- active FLASH particle type
                         sink -- sink FLASH particle type
                         any other string or none returns 
                         all particle types.
                         The default is all.
                         
       Returns:
       mass             -- particle mass
       tags             -- particle ID tags
       ct               -- simulation time when particle formed
       pp               -- x,y,z positions 
       pv               -- x,y,z velocities
    """
    mass = tags = ct = posx = posy = posz = velx = vely = velz = None
    
    ds = h5py.File(partfile, 'r')
    
    part_data    = ds.get('tracer particles')
    part_names   = ds.get('particle names')
    part_data    = np.array(part_data)
    part_names   = np.array(part_names)
    real_scalars = ds.get('real scalars')
    part_time    = real_scalars[0][1]
    
    if (part_data.size > 1):
        
        mass_ind  = np.where(np.char.strip(part_names)=='mass')[0]
        tag_ind   = np.where(np.char.strip(part_names)=='tag')[0]
        type_ind  = np.where(np.char.strip(part_names)=='type')[0]
        ct_ind    = np.where(np.char.strip(part_names)=='creation_time')[0]
        posx_ind  = np.where(np.char.strip(part_names)=='posx')[0]
        posy_ind  = np.where(np.char.strip(part_names)=='posy')[0]
        posz_ind  = np.where(np.char.strip(part_names)=='posz')[0]
        velx_ind  = np.where(np.char.strip(part_names)=='velx')[0]
        vely_ind  = np.where(np.char.strip(part_names)=='vely')[0]
        velz_ind  = np.where(np.char.strip(part_names)=='velz')[0]
        
        # Get the particle types
        type_part = part_data[:,type_ind].flatten()
        
        if (particle_type == 'star' or particle_type == 'stars'):
            # Find only star particles (particle_type=1)
            star_ind = np.where(type_part==1)[0]
        elif (particle_type == 'sink' or particle_type == 'sinks'):
            # Find only sink particles (particle_type=2)
            star_ind = np.where(type_part==2)[0]
        elif (particle_type == 'all'):
            star_ind = np.arange(len(type_part))
        else:
            print "WARNING: Requested particle type not recognized." \
                  " Defaulting to returning all particle types in file!"
            star_ind = np.arange(len(type_part))
        
        num_stars = len(star_ind)
        if (num_stars == 0): return mass, tags, ct, pp, pv

        # Masses of stars
        mass = part_data[star_ind,mass_ind]
        # Tags of stars
        tags = part_data[star_ind,tag_ind]
        # Creation times of stars
        ct   = part_data[star_ind,ct_ind]
        # Positions
        posx = part_data[star_ind,posx_ind]
        posy = part_data[star_ind,posy_ind]
        posz = part_data[star_ind,posz_ind]
        # Velocities
        velx = part_data[star_ind,velx_ind]
        vely = part_data[star_ind,vely_ind]
        velz = part_data[star_ind,velz_ind]
        pp = np.array([posx, posy, posz]).T
        pv = np.array([velx, vely, velz]).T
    else:
        print "Error: No particles found in this file!"
        
    return mass, tags, ct, pp, pv
