import matplotlib

font = {'family' : 'sans',
        'weight' : 'normal',
        'size'   : 24}

matplotlib.rc('font', **font)

import matplotlib.pyplot as plt
import numpy as np

def plot_my_hist(x, xlabel, ylabel, hist_bins="auto", logdata=False,
                 show_plt=True, save_plt_name=None, 
                 norm_factor=1.0, figsize=(8,8), tw=1, tmajl=10, tminl=5,
                 plt_title=None, plt_xlog=False, cum_hist=False,
                 den=False):
    

    f = plt.figure(figsize=figsize)
    ax = f.add_subplot(111)
    ax.tick_params(axis="both", which="both", direction="in")
    ax.tick_params(axis="both", which="both", width=tw)
    ax.tick_params(axis="both", which="major", length=tmajl)
    ax.tick_params(axis="both", which="minor", length=tminl)

    if (logdata):
        n, bins, patch = ax.hist(np.log10(x*norm_factor),
                                 bins=hist_bins,
                                 density=den, cumulative=cum_hist,
                                 edgecolor='k')
    else:
        n, bins, patch = ax.hist((x*norm_factor), bins=hist_bins,
                                 density=den, cumulative=cum_hist,
                                 edgecolor='k')
    if (plt_title is not None): plt.title(plt_title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    if (plt_xlog): plt.xscale('log')
    
    if (save_plt_name is not None):
        f.savefig(save_plt_name+'.png', bbox_inches='tight')
        f.savefig(save_plt_name+'.pdf', bbox_inches='tight')
    if (show_plt):
        plt.show()
    return n, bins, patch
    
def plot_my_scatter(x, y, xlabel, ylabel, logx=False, logy=False,
                   show_plt=True, save_plt_name=None, 
                   x_norm=1.0, y_norm=1.0, multi_set=None,
                   x_lim=None, y_lim=None, 
                   stretch_x=False, stretch_y=False, lim_factor=1.25,
                   figsize=(8,8), tw=1, tmajl=10, tminl=5,
                   plt_title=None, plt_grid=True, plt_legend=False):
    

    f = plt.figure(figsize=figsize)
    ax = f.add_subplot(111)
    ax.tick_params(axis="both", which="both", direction="in")
    ax.tick_params(axis="both", which="both", width=tw)
    ax.tick_params(axis="both", which="major", length=tmajl)
    ax.tick_params(axis="both", which="minor", length=tminl)

    if (multi_set is None):
        ax.scatter(x/x_norm, y/y_norm)
        
    else:
        cmap = plt.get_cmap('Set1')
        lind = 0 # old number in set
        for i,(num_in_set,name) in enumerate(multi_set):
            
            uind = num_in_set + lind
            c1 = cmap.colors[i]
            ax.scatter(x[lind:uind]/x_norm, y[lind:uind]/y_norm, c=c1, label=name)
            lind=uind

    if (plt_title is not None): plt.title(plt_title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    
    if (x_lim is not None):
        ax.set_xlim(x_lim[0],x_lim[1])
    else:
        lll = np.floor(np.log10(np.min(x/x_norm)))
        lul = np.ceil(np.log10(np.max(x*x_norm)))
        lower_lim = 10**lll
        upper_lim = 10**lul
        plt.xlim(lower_lim, upper_lim)
        #if (stretch_x): plt.xlim(np.min(x/x_norm)/lim_factor,
        #                np.max(x*x_norm)*lim_factor)
        if (logx):
            plt.xticks(np.logspace(lll, lul,lul-lll))
            print lul-lll
            ax.xaxis.set_major_locator(plt.MaxNLocator(lul-lll))
    if (y_lim is not None):
        ax.set_ylim(y_lim[0],y_lim[1])
    else:
        lll = np.floor(np.log10(np.min(y/y_norm)))
        lul = np.ceil(np.log10(np.max(y*y_norm)))
        lower_lim = 10**lll
        upper_lim = 10**lul
        plt.ylim(lower_lim, upper_lim)
        #if (stretch_y): plt.ylim(np.min(y/y_norm)/lim_factor,
        #                np.max(y*y_norm)*lim_factor)
        if (logy): plt.yticks(np.logspace(lll, lul,lul-lll))
                       
    if (logx): plt.xscale('log')
    if (logy): plt.yscale('log')
    if (plt_grid): plt.grid(which='major')
    if (plt_legend or multi_set is not None): plt.legend(fontsize=14)
    
    if (save_plt_name is not None):
        f.savefig(save_plt_name+'.png', bbox_inches='tight')
        f.savefig(save_plt_name+'.pdf', bbox_inches='tight')
    if (show_plt):
        plt.show()
    return
    
from sklearn.grid_search import GridSearchCV
from sklearn.cross_validation import LeaveOneOut
from sklearn.neighbors import KernelDensity

def make_kde_plot(input_var, input_var_name, input_var_x_label,
                  lbw=-2, ubw=1, nbw=1000, save_plts=False,
                  save_dir='./'):
    
    # lower and upper bounds of the data
    lb = np.min(input_var)
    ub = np.max(input_var)

    # Now normalize the data. Note that most ML algorthims struggle
    # on data unless you norm it from 0 to 1.
    var_norm = (input_var-lb)/(ub-lb)

    # a sample space to test our outcomes across that looks more smooth than our data
    x_d = np.linspace(0.0,1.0,1000)[:,np.newaxis]

    # run a cross-validation to find the proper bandwidth
    # for our KDE fit. Since we are mostly concerned about
    # our small sample size, we use the jack-knife method

    bandwidths = 10 ** np.linspace(lbw, ubw, nbw)
    grid = GridSearchCV(KernelDensity(kernel='gaussian'),
                        {'bandwidth': bandwidths},
                        cv=LeaveOneOut(len(var_norm)))

    grid.fit(var_norm[:, None]);

    bw =  grid.best_params_['bandwidth']
    print bw, 10**lbw, 10**ubw
    
    if (bw == lbw or bw == ubw): print "WARNING: You hit the limit on your cross validation." \
                                       "Bandwidth limits need to be extended."

    # instantiate and fit the KDE model
    kde = KernelDensity(bandwidth=bw, kernel='gaussian')
    kde.fit(var_norm[:, None])

    # score_samples returns the log of the probability density
    logprob = kde.score_samples(x_d)

    f = plt.figure(figsize=(10,8))

    plt.fill_between((x_d[:,0]*(ub-lb)+lb), np.exp(logprob), alpha=0.5, label=r'KDE $\sigma=${:.2F}'.format(bw))
    plt.plot(input_var, np.full_like(input_var, -0.01), '|k', markeredgewidth=1, alpha=0.5)
    plt.ylabel(r"$\rho_{\rm KDE}$")
    plt.xlabel(input_var_x_label)
    #plt.xscale('log')
    #plt.yscale('log')
    plt.legend(loc='upper left')

    if (save_plts): f.savefig(save_dir+input_var_name+'_pdf.png', bbox_inches='tight')
    if (save_plts): f.savefig(save_dir+input_var_name+'_pdf.pdf', bbox_inches='tight')

    return
