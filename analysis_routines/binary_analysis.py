
from load_flash_particles import *
from binary_routines import *

from amuse.lab import units as u
from amuse.lab import write_set_to_file
import numpy as np
import pickle

# Note MPI_Init is called when mpi4py is imported.
# MPI_Finalize is called when the script ends.
from mpi4py import MPI


class binary_storage(object):
    
    """A class for storing all this binary data."""
    
    def __init__(self, stars=None, mutually_bnd=None,
                 tags_bnd=None, q=0.0, s1=0.0, s2=0.0,
                 page=0.0, sage=0.0, bm=0.0, 
                 a=0.0, e=0.0, r=0.0,
                 En=0.0, t=0.0, nb=0):
        self.stars = stars
        self.mutually_bnd = mutually_bnd
        self.tags_bnd = tags_bnd
        self.q=q; self.s1=s1; self.s2=s2
        self.page=page; self.sage=sage; self.bm=bm
        self.a=a; self.e=e; self.r=r
        self.En=En; self.t=t; self.nb=nb
        return


def mpi_gather_dict_to_root(mydict, comm, root=0):
    
    """ For a given dictionary that was created on
        each processor separately during an MPI
        task, this function gathers all the
        dictionaries on the root process
        under the same name.
        
        Keyword arguments
        mydict -- the dictionary you want to gather
        comm   -- the communicator from MPI
        root   -- location to gather mydict to. Default is 0.
        
        Returns
        mydict -- the concatenated dictionary from
                  all processes.
    """
    
    rank = comm.Get_rank()
    all_dicts = comm.gather(mydict, root=root)
    
    if (rank == root):
        for item in all_dicts[0:root]: # all but the root
            mydict.update(item)
        for item in all_dicts[root+1:]: # all but the root
            mydict.update(item)
    
    return mydict

def setup_for_mpi_run(files, num_files, debug=False):
    
    # Initialize MPI
    comm = MPI.COMM_WORLD
    size = comm.Get_size()
    rank = comm.Get_rank()
    
    pre = "Proc", rank

    if (debug): print "Size =", size
    if (debug): print "Rank =", rank
    if (debug): print pre, "num_files=", num_files

    # Calculate beginning and ending index for part
    # of the files array that each processor will
    # compute on.

    # Find the number of files each processor will work on.
    base_work_size = int(np.ceil(num_files/size))

    if (debug):
        print pre, "Base unit of work =", base_work_size
    else:
        if (rank == 0): print "Base unit of work =", base_work_size

    # Make an array of starting and ending indices for
    # each processor.
    work_size_array = np.arange(size+1)*base_work_size

    if (debug):
        print pre, "Work array =", work_size_array
    else:
        if (rank == 0): print "Work array =", work_size_array

    # Push any left overs onto the last processor.
    work_size_array[-1] = num_files-1

    if (debug):
        print pre, "Work array =", work_size_array
    else:
        if (rank == 0): print "Work array =", work_size_array

    # Check that all of these files are accessable.
    #for w in work_size_array:
    #    print files[w]

    # Set this local processors start and end points.
    work_start = work_size_array[rank]
    work_end   = work_size_array[rank+1]

    local_files     = files[work_start:work_end]
    num_local_files = len(local_files)

    if (debug):
        print pre, "work_start=", work_start
        print pre, "work_end  =", work_end
        print pre, "num_local_files=", num_local_files
        
    return rank, size, comm, local_files, \
           num_local_files, work_start, work_end

def get_files(FLASH_file_dir='./', FLASH_file_name='*_part_*'):
    
    """ Given a directory and file name for a FLASH
        data set, get the files for that set so
        we can do some analysis.
        
        Keyword arguments
        FLASH_file_dir  -- location of the FLASH particle files
        FLASH_file_name -- name of the FLASH particle (or checkpoint) files
        
        Returns
        files -- The files for analysis, sorted by file number.
    """

    files = glob.glob(FLASH_file_dir+FLASH_file_name)
    files.sort() # Ensure they are in numerical order.
    
    return files

def do_ejection_analysis(files, AMUSE_file_dir='./', debug=False):
    
    pre = "do_ejection_analysis"
    if (debug): pre, "entered."
                             
    # Some arrays and dictionaries for the analysis of the data.
    tot_part_mass = np.zeros(len(files)) # Total mass of all particles at this time.
    
    # Stuff about stars when they were created.
    creation_mass_all_stars = {}
    creation_time_all_stars = {}
    creation_pos_all_stars  = {}
    creation_vel_all_stars  = {}

    # Stuff about stars that are ejected from the simulation.
    vel_mag_all_stars       = {}
    ejected_birth_mass      = {}
    ejected_birth_position  = {}
    ejected_birth_time      = {}
    ejected_vel             = {}
    
    for i,f in enumerate(files):
        
        ### Load the FLASH data into a particle set. Direct from hdf5 is 
        # the fastest. ###
        
        stars = get_ps_from_hdf5(f, particle_type='stars')
        if (stars is None): continue
        
        # Look to see if we have any new tags that we haven't seen before.
        # If yes, record the mass, pos, vel and creation time in a global
        # dictionary with the tag as the key so it can be unquiely identified.
        
        for j, tag in enumerate(stars.tag):
            # Is this a new star that we've never seen before?
            if tag not in creation_mass_all_stars:
                creation_mass_all_stars[tag] = stars.mass[j].value_in(u.g)
                creation_time_all_stars[tag] = stars.ct[j].value_in(u.s)
                creation_pos_all_stars[tag]  = stars.position[j].value_in(u.cm)
                creation_vel_all_stars[tag]  = stars.velocity[j].value_in(u.cm/u.s)
            else:
                # Update the vel mag for this star.
                vel  = np.sqrt(stars[j].vx**2.0 + stars[j].vy**2.0 + stars[j].vz**2.0)
                vel_mag_all_stars[tag] = vel.value_in(u.cm/u.s)
            
        # Are there any stars that have left the simulation? If so, record
        # the last known good information about them before they left.
        for tag, val in vel_mag_all_stars.iteritems():
            if tag not in stars.tag:
                # Birth information
                ejected_birth_mass[tag]     = creation_mass_all_stars[tag]
                ejected_birth_position[tag] = creation_pos_all_stars[tag]
                ejected_birth_time[tag]     = creation_time_all_stars[tag]
                # Last known velocity before it left the simulation.
                ejected_vel[tag]            = vel_mag_all_stars[tag] 
        
        # Total mass in particles in this file.
        tot_part_mass[i] = np.array(creation_mass_all_stars.values()).sum()    
        
    creation_list = [creation_mass_all_stars,
                     creation_time_all_stars,
                     creation_pos_all_stars,
                     creation_vel_all_stars]
    
    ejection_list = [ejected_birth_mass,
                     ejected_birth_position,
                     ejected_birth_time,
                     ejected_vel]
    
    all_data = [creation_list, ejection_list, tot_part_mass]
    with open(AMUSE_file_dir+'creation_ejection_mass_data'+'.pickle', 'wb') as wf:
        pickle.dump(all_data, wf)
    
    if (debug): print pre, "exit."
    return creation_list, ejection_list, tot_part_mass
    
def do_binary_analysis(gamma, files,
                            AMUSE_file_dir='./',
                            AMUSE_file_name='stars',
                            write_stars_to_separate_AMUSE_file=False,
                            start_one_kep=True, debug=False):
                                                          
    """ Takes as input the location of a set of FLASH particle files and
        then converts these files to AMUSE particle sets, followed by
        locating and analyzing the binary populations in the sets. Stores
        the results to either AMUSE particle sets or pickles in 
        AMUSE_file_dir. All quantities are stored in cgs without attached
        AMUSE units for ease of processing with numpy or matplotlib plotting.
        
        Keyword arguments:
        gamma           -- the limiting ratio of perturber to binary
                           accelerations. This allows for the rejection
                           of a binary if a nearby perturbing star is
                           tidally disrupting the binary at a ratio of
                           greater than gamma =  
                           tidal acceleration / binary mutual acceleration.
                           Note for all three stars being equal mass and
                           d = distance of perturber to com of binary
                           a = binary semi-major axis
                           you find:
                           gamma = 0.75    for d/a = 2
                           gamma = 0.03456 for d/a = 5
                           gamma = 0.00408 for d/a = 10
        
        AMUSE_file_dir  -- location to store the final data
        AMUSE_file_name -- base name for the AMUSE particle sets
        write_stars_to_separate_AMUSE_file -- should I write a separate
                                              AMUSE particle file?
        
        Returns:
        all_mut_bnd         -- A list of all the mutually_bnd dictionaries.
        set_num_of_binaries -- A list of how many binaries are in each set.
    """
    pre = "do_binary_analysis"
    if (debug): pre, "entered."
    
    #files = glob.glob(FLASH_file_dir+FLASH_file_name)
    #files.sort() # Ensure they are in numerical order.
    
    # Ensure that Kepler doesn't keep starting
    # and stopping, which might lead to hangs.
    if (start_one_kep):
        if (debug): print pre, "starting Kepler."
        converter = nbody_to_si(1.0| u.cm, 1.0 | u.g)
        kep = Kepler(unit_converter=converter)
    else:
        kep=None

    # Make a list of mutually bound binaries from all of the stars.
    all_mut_bnd         = []
    # Number of binaries in each set.
    set_num_of_binaries = []
    i = 0
    for f in files:
        
        ### Load the FLASH data into a particle set. Direct from hdf5 is 
        #   the fastest. ###
        
        if (debug): print pre, "on file #", f[-4:] 

        stars = get_ps_from_hdf5(f, particle_type='stars')
        if (stars is None or len(stars) < 2): continue
        
        if (debug): print pre, "entering find_all_binaries."
        ### Now we start the actual binary analysis. ###
        stars, mutually_bnd = find_all_binaries(stars,
                                    limiting_gamma=gamma, debug=debug)

        if (debug): print pre, "entering get_all_binary_properties."
        # Note this routine will modify mutually_bnd in place
        # to remove any binary with e > 1.
        q, s1, s2, page, sage, bm, a, e, r, En, t, nb, tags_bnd = \
                           get_all_binary_properties(stars, mutually_bnd,
                                                     pass_kep=kep,
                                                     debug=debug)

        # Store what binaries we found to a global list.
        all_mut_bnd.append(mutually_bnd)
        # Store how many binaries we found.
        set_num_of_binaries.append(len(mutually_bnd.keys()))
        print "Number of binaries in this set = {}".format(set_num_of_binaries[i])
        
        ### Write data to file for analysis. ###
        
        if (debug): print pre, "writing data."
        # Here we use the FLASH file number (f[-4:]) to also number the particle sets.
        if (write_stars_to_separate_AMUSE_file):
            stars_filename = AMUSE_file_dir+AMUSE_file_name+f[-4:]+'.hdf5'
            write_set_to_file(stars, stars_filename, format='hdf5')
        
        # Pickle the binary data.
        binary_data = binary_storage(stars, mutually_bnd, tags_bnd,
                         q, s1, s2, page, sage, bm, a, e, r, En, t, nb)
        
        with open(AMUSE_file_dir+'binaries'+f[-4:]+'.pickle', 'wb') as wf:
            pickle.dump(binary_data, wf)
            wf.close()
        i+=1

    # Clean up Kepler worker.
    if (start_one_kep):
        if (debug): print pre, "stopping Kepler."
        kep.stop()

    print "Done writing to AMUSE particle sets in", AMUSE_file_dir
    
    if (debug): print pre, "exit."
    return all_mut_bnd, set_num_of_binaries

