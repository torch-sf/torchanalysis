
from glob import glob
import pickle
import numpy as np
import sys # In case we need to abort

# Note MPI_Init is called when mpi4py is imported.
# MPI_Finalize is called when the script ends.
from mpi4py import MPI

def gather_files(generic_file_name, file_dir, suffix='',
                 start=None, end=None, debug=False):

    """
    This function globs up all files that
    match the glob pattern of generic_file_name*
    that are in file_dir

    Keyword arguments:
    generic_file_name -- File name to search for,
                         like flash_hdf5_plt_cnt_
                         would search *flash_hdf5_plt_cnt*
                         or plt would search *plt*.
    file_dir  -- Directory to glob inside.
    suffix    -- Any string that comes after the numbering of the file.
    start     -- String number that matches the starting file number,
                 i.e. flash_hdf5_plt_cnt_0104 would mean start='104'.
    end       -- String number that matches the ending file number.
    debug     -- Switch on debugging print statements.

    Returns:
    files     -- Sorted globbed list of file names
                 truncated from start to end.
    """

    joined_file_name = file_dir+'*'+generic_file_name+'*'
    files = glob(joined_file_name) # Get the files.
    files.sort() # Sort them!
    if (debug):
        print generic_file_name
        print file_dir
        print joined_file_name
        print 'start=', start
        print 'end=', end
        print files

    # FIRST: Remove forced plot files from list if it is there.
    if ('forced' in files[0]): files = files[1:]
    
    if (start==None):
        start = 0
    else: # Find the starting file index.
        start = str(start).zfill(4)
        start = glob(joined_file_name+start+suffix)
        start = np.where(np.array(files)==start[0])[0]
        start = start[0].astype(int)

    if (end==None): 
        end = len(files)
    else: # Find the ending file index.
        end = str(end).zfill(4)
        end = glob(joined_file_name+end+suffix)
        end = np.where(np.array(files)==end[0])[0]
        end = end[0].astype(int)+1
        
    files = files[start:end]
    
    if (debug):
        print 'start=', start
        print 'end=', end
        print files

    return files, start, end

def initialize_mpi(debug=False):
    """
    Only initialize MPI and do
    nothing else.

    Keyword arguments:
    debug -- Switch on some print statements.

    Returns:
    rank -- Processor rank
    size -- Number of total processors
    comm -- The communicator for MPI.
    """
    
    # Initialize MPI
    comm = MPI.COMM_WORLD
    size = comm.Get_size()
    rank = comm.Get_rank()

    if (debug): print "Size =", size
    if (debug): print "Rank =", rank

    return rank, size, comm

def setup_for_mpi_run(files, num_files, debug=False):
    
    """
    Setup MPI (including initialization)
    and divide up a list of files
    for each processor to work
    on in a data parallel way.
    Generally useful for analyzing
    a large set of plot or checkpoint files.

    Keyword arguments:
    files     -- The list of files to divide
                 among processors.
    num_files -- The number of files in files.
    debug     -- Switch on some print statements.

    Returns:

    rank            -- Processor rank.
    size            -- Number of all processors.
    comm            -- MPI communicator.
    local_files     -- List of local files for this proc.
    num_local_files -- Number of local files for this proc.
    work_start      -- Starting index of files for this proc.
    work_end        -- Ending index of files for this proc.
    """

    # Initialize MPI
    rank, size, comm = initialize_mpi(debug=debug)
    pre = "Proc", rank
    # Calculate beginning and ending index for part
    # of the files array that each processor will
    # compute on.

    # Find the number of files each processor will work on.
    base_work_size = int(np.ceil(num_files/size))

    if (debug):
        print pre, "Base unit of work =", base_work_size
    else:
        if (rank == 0): print "Base unit of work =", base_work_size

    # Make an array of starting and ending indices for
    # each processor.
    work_size_array = np.arange(size+1)*base_work_size

    if (debug):
        print pre, "Work array =", work_size_array
    else:
        if (rank == 0): print "Work array =", work_size_array

    # Push any left overs onto the last processor.
    remaining_files = num_files % size
    for k in range(remaining_files):
        work_size_array[k+1:] += 1

    if (debug):
        print pre, "Work array =", work_size_array
    else:
        if (rank == 0): print "Work array =", work_size_array

    # Check that all of these files are accessable.
    #for w in work_size_array:
    #    print files[w]

    # Set this local processors start and end points.
    work_start = work_size_array[rank]
    work_end   = work_size_array[rank+1]

    local_files     = files[work_start:work_end]
    num_local_files = len(local_files)

    if (debug):
        print pre, "work_start=", work_start
        print pre, "work_end  =", work_end
        print pre, "num_local_files=", num_local_files
        
    return rank, size, comm, local_files, \
           num_local_files, work_start, work_end

def mpi_reduce_np_array_in_place(array, comm,
                                 root=0, oper=MPI.SUM,
                                 debug=False, pre=None):

    """
    Here I mimic the MPI_reduce operation
    in place with an array.

    Keyword arguments:
    array -- The data which should be reduced in place.
    comm  -- MPI communicator.
    root  -- Where to reduce to. Default is 0
    oper  -- The reduction operation. Default is MPI.SUM
    Returns:
    array -- The now reduced array.
    """

    recv_array = np.zeros_like(array)
    if (debug): print pre, 'before reduce, array=',array
    comm.Reduce(array, recv_array, root=root, op=oper)
    if (debug): print pre, 'after reduce, array=', array
    if (debug): print pre, 'after reduce, recv_array=', recv_array
    array = recv_array.copy()
    del(recv_array)
    return array

def mpi_gather_dict_to_root(mydict, comm, root=0):
    
    """ For a given dictionary that was created on
        each processor separately during an MPI
        task, this function gathers all the
        dictionaries on the root process
        under the same name.
        
        Keyword arguments
        mydict -- the dictionary you want to gather
        comm   -- the communicator from MPI
        root   -- location to gather mydict to. Default is 0.
        
        Returns
        mydict -- the concatenated dictionary from
                  all processes.
    """
    
    rank = comm.Get_rank()
    all_dicts = comm.gather(mydict, root=root)
    
    if (rank == root):
        for item in all_dicts[0:root]: # all but the root
            mydict.update(item)
        for item in all_dicts[root+1:]: # all but the root
            mydict.update(item)
    
    return mydict

def load_pickles_as_dictionary(base_file_name, file_dir='./',
                               numbered_files=False,
                               glob_files=False, 
                               whole_filename_passed=False,
                               debug=False):

    """
    This function returns a list of pickled
    files as a dictionary with the key as
    the file number, or in the case you passed a
    single base_file_name and glob_files=False, it
    returns a single unpickled object.
    Note it assumes the following format if numbered_files=True:
    file_dir/base_file_name[0-9][0-9][0-9][0-9].pickle
    when loading the files and obtaining the keys.
    It assumes if you passed multiple base names that
    your only loading one file for each name,
    (which could be a globbed set of numbered files)
    otherwise we exit with an error.

    Keyword arguments:
    base_file_name -- The base name of the file in a list
                      (before the number and .pickle).
    file_dir       -- Directory where the files are.
                      Default is './'.
    numbered_files -- If true we will assume that
                      the base name of all files
                      is the same and will set
                      the key to be the four numbers
                      before '.pickle'.
    glob_files     -- If true the function will 
                      attempt to glob all the files
                      and sort them for you.
    whole_filename_passed -- Used when you want to
                             pass a previously globbed file or
                             set of filenames, where the
                             entire filename is passed as
                             base_file_name.

    Returns
    unpickled_files -- All of the objects unpickled and packed in a dictionary.
    """
    multiple_base_names = len(base_file_name) > 1

    if (debug):
        print "multiple_base_names =", multiple_base_names
        print "base_file_name =", base_file_name

    if (multiple_base_names and glob_files):
        print "Cannot both have multiple base file names \
        and glob files. "
        sys.exit("multiple_base_names and glob_files \
        cannot both be true!")

    if (multiple_base_names): # Gather up all the files using name.
        pickled_files = []
        for name in base_file_name:
            pickled_files.append(name)
    elif (glob_files): # Assume you need me to glob up everything.
        joined_file_name = (file_dir+base_file_name[0]+
                         '*'+'.pickle')
        if (debug): print "joined_file_name =", joined_file_name
        pickled_files = glob(joined_file_name)
    else: # Assume that this one file is the file to unpickle
        if (debug): print "Looks like a single file to me."
        
        if (whole_filename_passed):
            joined_file_name = base_file_name
        else:
            joined_file_name = (file_dir+base_file_name[0]+
                         '*'+'.pickle')
        if (debug): print "joined_file_name =", joined_file_name
        # Now load this one file, then return immediately.
        with open(joined_file_name[0], 'r') as f:
            unpickled_files = pickle.load(f)

        return unpickled_files

    if (debug): print "pickled_files =", pickled_files

    unpickled_files = {}

    for i,file in enumerate(pickled_files):
        if (not numbered_files):
            key = base_file_name[i]
            key = key.replace(file_dir, '')
            key = key.replace('.pickle', '')
            if (debug): print key
        else:
            # The key should be the four numbers
            # that come before 7 characters '.pickle'.
            key = int(file[-11:-7])
            if (debug): print key
        with open(file) as f:
            unpickled_files[key] = pickle.load(f)

    if (debug): print "unpickled_files =", unpickled_files

    return unpickled_files
