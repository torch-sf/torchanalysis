import numpy as np
from amuse.lab import generic_unit_converter, nbody_system
from amuse.community.hop.interface import Hop
from amuse.lab import units as u
from amuse.lab import Particles
import yt
yt.mylog.level=50
import matplotlib
matplotlib.use('Agg')
import matplotlib
font = {'family' : 'sans',
        'weight' : 'normal',
        'size'   : 24}

matplotlib.rc('font', **font)
matplotlib.rc({'savefig.dpi':300})
import matplotlib.pyplot as plt

from functools import partial

from load_flash_particles import get_ps_from_yt
import pickle

from sklearn.preprocessing import StandardScaler
from sklearn.cluster import DBSCAN

import sys # For debugging and exit.

# Note MPI_Init is called when mpi4py is imported.
# MPI_Finalize is called when the script ends.
from mpi4py import MPI


# For plotting stuff
lw  = 2
my_color    = 'grey'
their_color = 'darkblue'
fs  = 20 
dpi = 300

# Make some derived data fields
# for yt.
from yt import derived_field
without_mag_fields = False

@derived_field(name = "jeans", units = "cm")
def _jeans(field, data):
    return np.sqrt((np.pi*data["gas","sound_speed"]**2.0)/(yt.units.physical_constants.G*data["dens"]))
@derived_field(name = "jeans_min")
def _jeans_min(field, data):
    return data["jeans"]/(6.0*data["dx"])

@derived_field(name = "gpotEdens", units = "g*cm**-1*s**-2")
def _gpotEdens(field, data):
    
    return (0.5*data['dens']*data['gpot']).in_base()


@derived_field(name='magedens', units="erg/cm**3")
def _magedens(field, data):
    
    return data["magp"]/(4.0*np.pi)

@derived_field(name="etotal", units="erg/cm**3")
def _etotal(field, data):
    global without_mag_fields
    try:
        return  (data["dens"].in_base()*(
            0.5*(data["velocity_magnitude"].in_base())**2.0 
            + data["gpot"].in_base()  
            + data["eint"].in_base()) 
            + data["magedens"].in_base())
    except:
        if (not without_mag_fields):
            print "No magnetic field found, so not using them."
            without_mag_fields=True
        return  (data["dens"].in_base()*(
            0.5*(data["velocity_magnitude"].in_base())**2.0 
            + data["gpot"].in_base()  
            + data["eint"].in_base()))
@derived_field(name="etotal_all_cells", units="erg")
def _etotal_all_cells(field, data):
    
    return  np.sum(data['cell_volume']*(data["density"].in_base()*(
            0.5*(data["velocity_magnitude"].in_base())**2.0 
            + 0.5*data["gpot"].in_base() # *0.5 Because g_ij + g_ji is double counting. 
            + data["eint"].in_base()) 
            + data["magedens"].in_base()))

# A class for storing cluster data with pickle
class cluster_storage(object):

    def __init__(self, groups, n_groups,
                 grp_com=None, grp_comv=None, grp_TM=None,
                 grp_radius=None, grp_KE=None, grp_gPE=None,
                 grp_sPE=None, grp_TE=None,
                 grp_gas_TM=None, grp_gas_comv=None, grp_gas_KE=None,
                 grp_gas_EI=None, grp_gas_ME=None, grp_gas_gPE=None,
                 grp_gas_sPE=None, grp_gas_TE=None,
                 grp_frac_dense_gas=None, grp_frac_jeans_gas=None,
                 total_frac_dense_gas=None, total_frac_jeans_gas=None,
                 half_mass_radii=None, lagrangian_radii=None,
                 cluster_boundedness=None):

        # The actual stars in the clusters.
        # Note the ones with index=-1 are not in a cluster.
        self.groups      = groups
        # Number of clusters.
        self.n_groups    = n_groups
        # These are all stellar cluster parameters.
        self.grp_com     = grp_com
        self.grp_comv    = grp_comv
        self.grp_TM      = grp_TM
        self.grp_radius  = grp_radius
        self.grp_KE      = grp_KE
        self.grp_gPE     = grp_gPE
        self.grp_sPE     = grp_sPE
        self.grp_TE      = grp_TE
        # And now the gas parameters associated with this cluster.
        self.grp_gas_TM  = grp_gas_TM
        self.grp_gas_comv= grp_gas_comv
        self.grp_gas_KE  = grp_gas_KE
        self.grp_gas_EI  = grp_gas_EI
        self.grp_gas_ME  = grp_gas_ME
        self.grp_gas_gPE = grp_gas_gPE
        self.grp_gas_sPE = grp_gas_sPE
        self.grp_gas_TE  = grp_gas_TE
        # Lets also keep up with the amount of dense
        # and Jeans unstable gas in the runs.
        self.grp_frac_dense_gas = grp_frac_dense_gas
        self.grp_frac_jeans_gas = grp_frac_jeans_gas
        self.total_frac_dense_gas = total_frac_dense_gas
        self.total_frac_jeans_gas = total_frac_jeans_gas
        # Cluster structure information.
        self.half_mass_radii = half_mass_radii
        self.lagrangian_radii = lagrangian_radii
        self.cluster_boundedness = cluster_boundedness

        return
        


# Functions
from scipy.interpolate import RegularGridInterpolator as RGI
def trilinear_interpolation(ds, interp_point, interp_var):

    """
    Interpolate the interp_var using trilinear interpolation
    at the interp_point from the dataset.
    This will utilize scipy.interpolate.RegularGridInterpolator
    """

    # Obtain the list of points near interp_point

    # Get the cell size and cell center position
    # nearest interp_point
    # point = ds.point(interp_point)

    return

def mpi_reduce_np_array_in_place(array, comm, root=0, oper=MPI.SUM):

    """
    Here I mimic the MPI_reduce operation
    in place with an array.

    Keyword arguments:
    array -- The data which should be reduced in place.
    comm  -- MPI communicator.
    root  -- Where to reduce to. Default is 0
    oper  -- The reduction operation. Default is MPI.SUM
    Returns:
    array -- The now reduced array.
    """

    recv_array = np.zeros_like(array)
    comm.Reduce(array, recv_array, op=oper, root=root)
    array = recv_array
    del(recv_array)
    return array

def get_dense_gas_and_jeans_fractions(dd, dens_gas_limit=1e4):

    """
    Returns the fraction of gas that is denser than
    dens_gas_limit in the yt data object dd,
    as well as the fraction of gas that is currently
    Jeans unstable in dd.

    Keyword arguments:
    dd  -- yt data object
    dens_gas_limit -- limit over which gas is considered dense

    Returns:
    dense_frac -- Fraction of gas mass over dens_gas_limit.
    jeans_frac -- Fraction of gas mass that is Jeans unstable.
    """
    total_gas_mass = np.sum(dd['cell_mass'].v)

    dense_frac = (
        np.sum(dd['cell_mass'][np.where(dd['H_nuclei_density'].v > dens_gas_limit)])
         / total_gas_mass
    )

    jeans_frac = (
        np.sum(dd['cell_mass'][np.where(dd['jeans_min'].v < 1.0)])
         / total_gas_mass
    )

    return dense_frac, jeans_frac

def find_clusters_with_dbscan(stars, outer_density_limit=1.0 | u.MSun*u.parsec**-3,
                              avg_stellar_mass=0.586,
                              eps=0.4, min_samples=12, leaf_size=30,
                              return_labels=False, debug=False):

    """
    Find all the stars in clusters using
    the DBSCAN implementation from scikit-learn.

    Keyword Arguments:
    stars               -- AMUSE particle set
    outer_density_limit -- If set, use this density_limit
                           in solar masses per parsec^-3 to
                           compute eps to figure
                           out where the cluster edges are
                           instead of the input eps. 
                           A good default choice
                           is 1.0 MSun * pc^-3.
                           Note this setting overrides eps.
    avg_stellar_mass    -- Average stellar mass of the IMF
                           used to make the stars your
                           clustering. Default is 
                           from a Kroupa IMF that goes 
                           from 0.08 to 150 Msun.
    eps                 -- Minimum  neighbor distance to be
                           considered in the cluster in pc.
                           This value is calculated for you
                           if you use outer_density_limit.
    min_samples         -- Minimum number of neighbors to
                           be considered a core particle.
                           Default is 12, the default for
                           DBSCAN.
    leaf_size           -- Number of particles in a leaf
                           on the KD tree the code uses
                           to find neighbors. Default is
                           30, the default for DBSCAN.
    return_labels       -- Also return the raw DBSCAN label output?
    debug               -- Turn on debugging output

    Returns:
    groups        -- A list of particle sets for the particles in each cluster.
    n_groups      -- The number of clusters found.
    labels        -- The actual label indicies returned by DBSCAN,
                     only returned if return_labels=True.
    unique_labels -- The unique labels returned by DBSCAN,
                     only returned if return_labels=True.
    """

    pre = "[find_clusters_with_dbscan]:"

    if (outer_density_limit is not None):

        # The number of samples should
        # be greater than that of
        # the average density of the
        # SN in a pc^3 (~ 0.01, BT), but not as high
        # as that of an open cluster 
        # (~10 Msun / pc^3, Binney and Tremaine)

        # Note the mean number density of the solar
        # neighborhood is 0.17 stars per parsec^3,
        # while the mean in an open cluster is 17 stars
        # per parsec^3, so a good choice is the mean
        # of these in log space, or about 1 star per parsec^-3.

        # So here we are saying they should be at least closer
        # that the average distance between stars in the SN.
        number_density_limit = (outer_density_limit.value_in(u.MSun*u.parsec**-3) 
                             / avg_stellar_mass)

        if (number_density_limit < 0.17):
            print pre, "WARNING: Your number density limit \
                        at", number_density_limit, "pc^-3 \
                        is smaller than that of the solar \
                        neighborhood, which is ~ 0.17 pc^-3!"

        eps = number_density_limit**(-1./3.)

    if (debug):
        print pre, "outer_density_limit  =", outer_density_limit
        print pre, "avg_stellar_mass     =", avg_stellar_mass
        print pre, "number_density_limit =", number_density_limit
        print pre, "eps =", eps

    particle_positions = stars.position.value_in(u.parsec)

    # Note: I don't think its necessary to scale
    #       the inputs for DBSCAN when you are
    #       using a simple Eulerian metric on 3-d
    #       position space, as its just rescaling eps.

    # Get a DBSCAN instance running.
    db = DBSCAN(eps=eps, min_samples=min_samples, leaf_size=leaf_size)
    # Do the clustering.
    clstrs = db.fit_predict(particle_positions)
    # Get the unique cluster lables (i.e. the number of clusters).
    labels = db.labels_
    unique_labels = set(labels) # This returns only the unique ones.
    # Anything with an index of -1 is noise.
    n_groups = len(filter((lambda x: x>=0),unique_labels))

    groups = []

    for label in unique_labels:
        if (label >= 0): # Don't include noise particles here.
            groups.append(stars[np.where(labels == label)[0]])

    if (debug):
        print pre, "groups=", groups
        print pre, "n_groups=", n_groups
        print pre, "labels=", labels
        print pre, "unique_labels=", unique_labels

    if (return_labels):
        return groups, n_groups, labels, unique_labels
    else:
        return groups, n_groups

def setup_hop(
    num_hop=16, num_density=64,
    outer_density_limit=1.0 | u.MSun*u.parsec**-3,
    peak_dens_factor=3.0,
    saddle_dens_factor=2.5,
    relative_saddle_density = False,
    relative_saddle_density_thres = 0.5,
    debug=False):
    
    """
    Setup a HOP worker (Eisenstien & Hut 1998).

    Arguments:
    num_hop               -- Number of neighbors to check for hopping to
                             the most dense neighbor.
    num_density           -- Number of neighbors to use to calculate the local
                             mass density at thie particle's location.
    outer_density_limit   -- The outer cutoff density. All stars at densities
                             below this number are removed from all clusters.
    peak_dens_factor      -- peak_density is given by peak_dens_factor*outer_density_limit
                             The default is 3.0 (as recommended in the paper).
    saddle_dens_factor    -- saddle_density is given by saddle_dens_factor*outer_density_limit
                             The default is 2.5 (as recommended in the paper).
    relative_saddle_density -- Instead of using an absolute saddle density,
                               use a relative calculation of the ratio of
                               the saddle density / min(peak density).
                               If this value is less than relative_saddle
                               _density_thres merge the two groups.
    relative_saddle_density_thres -- Threshold factor below which two
                                     groups sharing a boundary are merged.

    Returns:
    hop                   -- A HOP worker that can be used to do clustering.
    """

    conv = nbody_system.nbody_to_si(100.0 | u.MSun, 1.0 | u.parsec)
    #conv = generic_unit_converter.ConvertBetweenGenericAndSiUnits(1. |u.g,
    #                                                              1. |u.cm,
    #                                                              1. |u.s)
    
    hop = Hop(unit_converter=conv) #, redirection="none") #, debugger='gdb')

    # From Eistenstein and Hut, peak density
    peak_dens_limit = (peak_dens_factor*outer_density_limit).as_quantity_in(u.g*u.cm**-3)

    # From Eistenstein and Hut, saddle density
    saddle_dens_limit = (saddle_dens_factor*outer_density_limit).as_quantity_in(u.g*u.cm**-3)

    if (debug):
        print "On HOP setup entry:"
        print "HOP setup outer_dens_limt   =", outer_density_limit.as_quantity_in(u.MSun*u.parsec**-3)
        print "HOP setup peak_dens_limit   =", peak_dens_limit.as_quantity_in(u.MSun*u.parsec**-3)
        print "HOP setup saddle_dens_limit =", saddle_dens_limit.as_quantity_in(u.MSun*u.parsec**-3)
        print hop.parameters

    hop.parameters.number_of_neighbors_for_hop = num_hop # default: 64
    hop.parameters.number_of_neighbors_for_local_density = num_density # default: 64
    hop.parameters.outer_density_threshold  = outer_density_limit.as_quantity_in(u.g*u.cm**-3)  # default: -1000.0 m**-3 * kg
    hop.parameters.peak_density_threshold   = peak_dens_limit   # default: -1000.0 m**-3 * kg
    hop.parameters.saddle_density_threshold = saddle_dens_limit # default: -1000.0 m**-3 * kg
    hop.parameters.relative_saddle_density_threshold = relative_saddle_density
    hop.parameters.saddle_density_threshold_factor = relative_saddle_density_thres

    if (debug):
        print "On HOP setup just before exit:"
        print "HOP setup outer_dens_limt   =", outer_density_limit.as_quantity_in(u.MSun*u.parsec**-3)
        print "HOP setup peak_dens_limit   =", peak_dens_limit.as_quantity_in(u.MSun*u.parsec**-3)
        print "HOP setup saddle_dens_limit =", saddle_dens_limit.as_quantity_in(u.MSun*u.parsec**-3)
        print hop.parameters

    return hop


def find_clusters_with_hop(
    stars, num_hop=16, num_density=64,
    outer_density_limit=1.0 | u.MSun*u.parsec**-3,
    peak_dens_factor=3.0,
    saddle_dens_factor=2.5,
    relative_saddle_density = False,
    relative_saddle_density_thres = 0.5,
    passed_hop=None, debug=False):

    """
    Find all the stars in clusters from
    a particle set using HOP (Eisenstien & Hut 1998).
    
    Keyword arguments
    stars                 -- AMUSE particle set
    num_hop               -- Number of neighbors to check for hopping to
                             the most dense neighbor.
    num_density           -- Number of neighbors to use to calculate the local
                             mass density at thie particle's location.
    outer_density_limit   -- The outer cutoff density. All stars at densities
                             below this number are removed from all clusters.
    peak_dens_factor      -- peak_density is given by peak_dens_factor*outer_density_limit
                             The default is 3.0 (as recommended in the paper).
    saddle_dens_factor    -- saddle_density is given by saddle_dens_factor*outer_density_limit
                             The default is 2.5 (as recommended in the paper).
    relative_saddle_density -- Instead of using an absolute saddle density,
                               use a relative calculation of the ratio of
                               the saddle density / min(peak density).
                               If this value is less than relative_saddle
                               _density_thres merge the two groups.
    relative_saddle_density_thres -- Threshold factor below which two
                                     groups sharing a boundary are merged.
    passed_hop            -- If you are calling this in a loop its reccomended to
                             pass a HOP worker here, since starting and stopping
                             HOP repeatedly generally hangs the HOP worker.

    Returns
    
    groups  -- A list of particle sets for the particles in each cluster.
    ngroups -- The number of clusters found.
    
    """

    if (passed_hop is None):
        stars_total_E = stars.kinetic_energy() + stars.potential_energy()
        stars_total_M = stars.mass.sum()
        conv = nbody_system.nbody_to_si(stars_total_M, stars_total_E)
        #conv = generic_unit_converter.ConvertBetweenGenericAndSiUnits(
        #           stars_total_M, stars_total_E, u.constants.G)
        #conv = generic_unit_converter.ConvertBetweenGenericAndSiUnits(
        #           1.0 | u.cm, 1.0 | u.g, 1.0 | u.s)
        hop = Hop(unit_converter=conv) #, redirection="none") #, debugger='gdb')

        # From Eistenstein and Hut, peak density
        peak_dens_limit = (peak_dens_factor*outer_density_limit).as_quantity_in(u.g*u.cm**-3)

        # From Eistenstein and Hut, saddle density
        saddle_dens_limit = (saddle_dens_factor*outer_density_limit).as_quantity_in(u.g*u.cm**-3)

        if (debug):
            print "On HOP entry:"
            print "HOP outer_dens_limt   =", outer_density_limit.as_quantity_in(u.MSun*u.parsec**-3)
            print "HOP peak_dens_limit   =", peak_dens_limit.as_quantity_in(u.MSun*u.parsec**-3)
            print "HOP saddle_dens_limit =", saddle_dens_limit.as_quantity_in(u.MSun*u.parsec**-3)
            print hop.parameters

        hop.parameters.number_of_neighbors_for_hop = num_hop # default: 64
        hop.parameters.number_of_neighbors_for_local_density = num_density # default: 64
        hop.parameters.outer_density_threshold  = outer_density_limit.as_quantity_in(u.g*u.cm**-3)  # default: -1000.0 m**-3 * kg
        hop.parameters.peak_density_threshold   = peak_dens_limit   # default: -1000.0 m**-3 * kg
        hop.parameters.saddle_density_threshold = saddle_dens_limit # default: -1000.0 m**-3 * kg
        hop.parameters.relative_saddle_density_threshold = relative_saddle_density # Use a relative instead of absolute saddle density?
        hop.parameters.saddle_density_threshold_factor = relative_saddle_density_thres

        if (debug):
            print hop.get_fDensThresh()
            print "After HOP setup:"
            print "HOP outer_dens_limt   =", outer_density_limit.as_quantity_in(u.MSun*u.parsec**-3)
            print "HOP peak_dens_limit   =", peak_dens_limit.as_quantity_in(u.MSun*u.parsec**-3)
            print "HOP saddle_dens_limit =", saddle_dens_limit.as_quantity_in(u.MSun*u.parsec**-3)
            print hop.parameters
        
        if (debug): print "HOP parameters set."

    else:
        hop = passed_hop
        if (debug): print "Got a hop in here!"

    hop.particles.add_particles(stars)
    if (debug): 
        print "HOP particles added."
        #print hop.get_fDensThresh()

    hop.calculate_densities()
    if (debug):
        print "HOP densities calculated."
        #print hop.get_fDensThresh()
    hop.do_hop()
    if (debug): print "HOP done."

    n_groups = hop.get_number_of_groups()
    if (debug): print "Number of groups =", n_groups
    groups = [x.get_intersecting_subset_in(stars) for x in hop.groups()]
    
    if (passed_hop is None):
        hop.stop()
        if (debug): print "HOP stopped. Exiting."
    else:
        hop.particles.remove_particles(stars)

    return groups, n_groups

def get_half_mass_radii_for_mass_bins(stars, com1=None, 
                                      bin_equal_numbers_of_stars=False,
                                      mass_frac_bins=[0.0, 0.25, 0.5, 0.75, 1.0],
                                      verbose=0, rank=''):
    """
    Half mass radii of all stars in
    a mass bin from mass_frac_bins[i]
    to mass_frac_bins[i+1] Similar
    to a Lagrangian radii, but not
    cumlative in radius, instead stars
    at all radii are sorted by mass in bin.

    NOTE: This routine will change the
    mass fraction for the last bin to
    guarantee that at least TWO stars
    are in the last bin, always.

    Keyword arguments:
    stars          -- AMUSE particle set.
    com1           -- Particle set center of mass
                      in cm. If None, will be 
                      calculated.
    bin_equal_numbers_of_stars -- If True, attempt to
                      place roughly equal numbers of stars
                      in each bin. Note this shifts more
                      mass into the massive bins, but keeps
                      from having a single massive star in
                      the last bin.
    mass_frac_bins -- The fractional mass bins
                      used to divide the stars.
    verbose        -- Debugging verbosity.

    Returns:
    lag_r          -- An array of half mass
                      radii for each mass_frac_bin.
    """
    
    if (rank != ""):
        pre = "[half_mass_radii]: Rank", rank
    else:
        pre = "[half_mass_radii]:"

    if (verbose==True): verbose = 3 # Just turn it all on if debug was passed.

    lag_r       = np.zeros(len(mass_frac_bins)-1)

    # Masses.
    lr_mass     = stars.mass.value_in(u.g)
    # Number of stars
    num_stars   = len(lr_mass)
    # Number of bins
    num_bins    = len(mass_frac_bins)-1
    # Mass indices sorted from least to greatest.
    sort_ind    = np.argsort(lr_mass)
    # Relative sorted cumulative masses.
    rel_lr_mass = lr_mass[sort_ind].cumsum()/lr_mass.sum()
    # Center of mass.
    if (com1 is None): com1 = stars.center_of_mass()

    if (verbose > 1): print pre, "rel_lr_mass=", rel_lr_mass
    if (verbose > 1): print pre, "len of rel_lr_mass", len(rel_lr_mass)

    # Ensure that at least TWO stars are always in the last mass bin.
    if (rel_lr_mass[-2] > mass_frac_bins[-2] 
        and not bin_equal_numbers_of_stars):
         mass_frac_bins[-2] = rel_lr_mass[-2] - 0.01

    for j in range(num_bins):
        # Lower and upper percentage bounds.
        lower       = mass_frac_bins[j]
        upper       = mass_frac_bins[j+1]
        if (verbose > 2): print pre, "lower=", lower
        if (verbose > 2): print pre, "upper=", upper
        
        if (bin_equal_numbers_of_stars):
            lower_ind = num_stars/num_bins*j
            upper_ind = num_stars/num_bins*(j+1)
        else:
            # Lower bound sorted index (exclusive).
            lower_ind   = np.where(rel_lr_mass > lower)[0][0]
            # Upper bound sorted index (inclusive).
            upper_ind   = np.where(rel_lr_mass <= upper)[0][-1]
        if (verbose > 2): print pre, "lower_ind=", lower_ind
        if (verbose > 2): print pre, "upper_ind=", upper_ind
        # Now select a subset of the stars in this bin
        if (lower_ind == upper_ind):
            bin_stars = stars[sort_ind[lower_ind]]
            if (verbose > 2): print pre, "Single star in bin of mass =", bin_stars.mass.value_in(u.MSun)
        else:
            bin_stars = stars[sort_ind[lower_ind:upper_ind]]
            if (verbose > 2):
                print pre, "bin_stars masses=", bin_stars.mass.value_in(u.MSun)
                print pre, "len(bin_stars)=", len(bin_stars)
        rel_pos   = (bin_stars.position - com1).value_in(u.cm)
        
        if (lower_ind > upper_ind):
            if (verbose > 2): print pre, "This bin must be empty, lower_ind > upper_ind!"
            lag_r[j] = 0.0
        elif (lower_ind == upper_ind):
            rel_r     = np.sqrt((rel_pos**2.).sum())
            lag_r[j] = rel_r
        else:
            rel_r     = np.sqrt((rel_pos**2.).sum(axis=1))
            sort_m    = bin_stars.mass[np.argsort(rel_r)].value_in(u.g)
            sort_r    = rel_r[np.argsort(rel_r)]
            rel_m     = sort_m.cumsum()/(bin_stars.mass.sum().value_in(u.g))
            if (verbose > 2): print pre, "rel_m=", rel_m
            hm_ind    = np.where(rel_m>= 0.5)[0][0]
            lag_r[j] = sort_r[hm_ind]
    if (verbose > 2): print pre, "lag_r =", lag_r
    if (verbose > 0): print pre, "stored particle half mass radii."

    return lag_r

def get_lagrangian_radii(stars, com=None, 
                         frac_bins=[0.0, 0.25, 0.5, 0.75, 1.0],
                         verbose=0, rank=''):
    """
    Lagrangian radii of all stars in
    a mass bin from frac_bins[i]
    to frac_bins[i+1], with the
    bins filled going from the center
    of mass outward in radius.


    NOTE: This routine will change the
    mass fraction for the last bin to
    guarantee that at least TWO stars
    are in the last bin, always.

    Keyword arguments:
    stars          -- AMUSE particle set.
    com            -- Particle set center of mass
                      in cm. If None, will be 
                      calculated.
    frac_bins      -- The fractional mass bins
                      used to divide the stars.
    verbose        -- Debugging verbosity.

    Returns:
    lag_r          -- Lagrangian radii.
    """
    if (rank != ""):
        pre = "[lagrangian_radii]: Rank", rank
    else:
        pre = "[lagrangian_radii]:"

    if (verbose==True): verbose = 3 # Just turn it all on if debug was passed.

    lag_r       = np.zeros(len(frac_bins)-1)
    # Center of mass.
    if (com is None): com = stars.center_of_mass()
    # Relative positions
    lr_rel_pos  = stars.position.value_in(u.cm) - com.value_in(u.cm)
    # Sorted relative position indicies.
    rel_radii   = np.sqrt(np.sum(lr_rel_pos**2.0, axis=1))
    sort_ind    = np.argsort(rel_radii)
    # Masses.
    lr_mass     = stars.mass.value_in(u.g)
    # Relative sorted cumulative masses.
    rel_lr_mass = lr_mass[sort_ind].cumsum()/lr_mass.sum()
    # Number of bins
    num_bins    = len(frac_bins)-1

    # Ensure that at least TWO stars are always in the first mass bin.
    #if (rel_lr_mass[1] > frac_bins[1]):
    #     frac_bins[1] = rel_lr_mass[1] + 0.01
    
    if (verbose > 1): print pre, "rel_lr_mass=", rel_lr_mass
    if (verbose > 1): print pre, "len of rel_lr_mass", len(rel_lr_mass)

    for j in range(num_bins):
        # Lower and upper percentage bounds.
        lower       = 0.0 # For Lagrangian radii, this is always cumulative and inclusive.
        upper       = frac_bins[j+1]
        if (verbose > 2): print pre, "lower=", lower
        if (verbose > 2): print pre, "upper=", upper
        
        # If the first star in radius encountered
        # is a higher fraction of the total stellar
        # mass than the current bin right edge,
        # skip this loop (i.e. leave this one zero).
        if (rel_lr_mass[0] > upper): continue
        
        # Lower bound sorted index (inclusive and cumulative, so always 0).
        lower_ind   = 0
        # Upper bound sorted index (inclusive).
        upper_ind   = np.where(rel_lr_mass <= upper)[0][-1]
        if (verbose > 2): print pre, "lower_ind=", lower_ind
        if (verbose > 2): print pre, "upper_ind=", upper_ind
        # Now select a subset of the stars in this bin
        if (lower_ind == upper_ind):
            bin_stars = stars[sort_ind[lower_ind]]
            if (verbose > 2): print pre, "Single star in bin of mass =", bin_stars.mass.value_in(u.MSun)
        else:
            bin_stars = stars[sort_ind[lower_ind:upper_ind]]
            if (verbose > 2):
                print pre, "bin_stars masses=", bin_stars.mass.value_in(u.MSun)
                print pre, "len(bin_stars)=", len(bin_stars)
        rel_pos   = (bin_stars.position - com).value_in(u.cm)
        
        if (lower_ind > upper_ind):
            if (verbose > 2): print pre, "This bin must be empty, lower_ind > upper_ind!"
            lag_r[j] = 0.0
        elif (lower_ind == upper_ind):
            rel_r     = np.sqrt((rel_pos**2.).sum())
            lag_r[j] = rel_r
        else:
            rel_r     = np.sqrt((rel_pos**2.).sum(axis=1))
            sort_m    = bin_stars.mass[np.argsort(rel_r)].value_in(u.g)
            sort_r    = rel_r[np.argsort(rel_r)]
            rel_m     = sort_m.cumsum()/(bin_stars.mass.sum().value_in(u.g))
            if (verbose > 2): print pre, "rel_m=", rel_m
            hm_ind    = np.where(rel_m>= 0.5)[0][0]
            lag_r[j] = sort_r[hm_ind]
    if (verbose > 2): print pre, "lag_r =", lag_r
    if (verbose > 0): print pre, "stored particle Lagrangian radii."

    return lag_r

def calculate_stellar_group_properties(ds, stellar_groups, frac_bin, debug=False):
    
    """
    Calculate the energies, mass, radius, COM and COM velocity
    for stellar groups.
    
    Keyword arguments:
    ds             -- yt dataset object for getting the gas potential
                      from the grid.
    stellar_groups -- a lise of AMUSE particle sets (presumably clusters)

    Returns:
    
    grp_com    -- Centers of mass
    grp_comv   -- Center of mass velocities
    grp_TM     -- Total masses
    grp_radius -- Maximum raduis of each cluster
    grp_KE     -- Total kinetic energies
    grp_gPE    -- Total gas potential energies
    grp_sPE    -- Total stellar potential energies
    grp_TE     -- Total energies

    """

    grp_com    = []
    grp_comv   = []
    grp_TM     = []
    grp_radius = []
    grp_KE     = []
    grp_gPE    = []
    grp_sPE    = []
    grp_TE     = []

    ad = ds.all_data()
    conv = generic_unit_converter.ConvertBetweenGenericAndSiUnits(
        1.0 | u.cm, 1.0 | u.g, 1.0 | u.s)
    for i,sg in enumerate(stellar_groups):

        if (debug):
            print type(sg)

        # Calculate the total energy of the stars (KE + gas pot'l on stars + stars pot'l on stars).

        # Get the center of mass and radius for use in looking at the gas.
        com    = sg.center_of_mass()
        
        if (frac_bin == -1.0):
            radius = sg.total_radius()
        elif (frac_bin > 0 and frac_bin < 1.0):
            print "com: ", com
            radius, mf = sg.LagrangianRadii(unit_converter=conv, mf=[frac_bin], cm=com)  
            radius = radius[0]
            print "Lagrangian radius = ", radius
        else:
            print "lagr frac bin not < 1 and > 0. Breaking."
            break
                        
        # First calculate the center of mass velocity for this group
        comvx  = (sg.mass*sg.vx).sum() / sg.mass.sum()
        comvy  = (sg.mass*sg.vy).sum() / sg.mass.sum()
        comvz  = (sg.mass*sg.vz).sum() / sg.mass.sum()
        # Subtract out to get the relative velocity of this group
        rel_vx = sg.vx - comvx
        rel_vy = sg.vy - comvy
        rel_vz = sg.vz - comvz
        # Now calculate the relative KE
        stars_KE = (0.5*(sg.mass*(rel_vx**2.0+rel_vy**2.0+rel_vz**2.0)).sum()).value_in(u.erg)
        #stars_KE  = sg.kinetic_energy().value_in(u.erg)

        s_on_s_PE = sg.potential_energy().value_in(u.erg)
        # Here we use the nearest grid point to get the gas gravity on the stars.
        g_on_s_PE = (sg.mass.value_in(u.g)
                     *ds.find_field_values_at_points(['gpot'],
                      sg.position.value_in(u.cm))).sum().v
        stars_TE = stars_KE + g_on_s_PE + s_on_s_PE

        if (debug):
            print "Group {} total mass       = {}".format(i,sg.mass.sum())
            print "Group {} com vel          = {} cm/s".format(i,grp_comv)
            print "Group {} total KE         = {} ergs".format(i,stars_KE)
            print "Group {} total stellar PE = {} ergs".format(i,s_on_s_PE)
            print "Group {} total gas PE     = {} ergs".format(i,g_on_s_PE)
            print "Group {} total energy     = {} ergs \n".format(i,stars_TE)

        grp_com.append(com.value_in(u.cm))
        grp_comv.append([comvx.value_in(u.cm/u.s), comvy.value_in(u.cm/u.s), comvz.value_in(u.cm/u.s)])
        grp_TM.append(sg.mass.sum().value_in(u.g))
        grp_radius.append(radius.value_in(u.cm))
        grp_KE.append(stars_KE)
        grp_gPE.append(g_on_s_PE)
        grp_sPE.append(s_on_s_PE)
        grp_TE.append(stars_TE)
                      
    return grp_com, grp_comv, grp_TM, grp_radius, grp_KE, grp_gPE, grp_sPE, grp_TE

use_stars_for_potential=False

def calculate_gas_properties(ds, groups, grp_com, grp_radius, debug=False):

    """
    Calculates the gas total mass
    and energy properties of a spherically
    defined region in a yt dataset for each
    com and radius in grp_com and grp_radius.

    Keyword arguments:
    ds         -- yt dataset
    stars      -- AMUSE particle set. Only in case
                  there is no 'bgpt' gas variable,
                  in which case we use this to get
                  the particle potential.
    grp_com    -- center of the spherical regions
    grp_raduis -- radius of the spherical regions

    Returns:
    grp_gas_TM  -- total mass of the gas in the region
    grp_gas_KE  -- total kinetic energy of the gas in the region
    grp_gas_EI  -- total internal energy of the gas in the region
    grp_gas_ME  -- total magnetic energy of the gas in the region
    grp_gas_gPE -- total potential energy from the gas on gas in the region
    grp_gas_sPE -- total potential energy from the particles on gas in the region
    grp_gas_TE  -- total energy from all the above of the gas in the region
    """
    global use_stars_for_potential

    if (getattr(ds.fields.flash, 'bgpt', None) is None 
        and not use_stars_for_potential):
        # Then there is no bridge potential on this run
        # so fallback to calculating the stellar potential
        # directly from the particles.
        print "Switching to using the AMUSE particle set to calculate stellar potential on the gas."
        use_stars_for_potential = True

    grp_gas_TM = []
    grp_gas_KE = []
    grp_gas_comv = []
    grp_gas_EI = []
    grp_gas_ME = []
    grp_gas_gPE = []
    grp_gas_sPE = []
    grp_gas_TE = []
    grp_gas_dense_frac = []
    grp_gas_jeans_frac = []

    # First calculate the global dense gas fraction
    # and Jeans unstable fraction.
    dd = ds.all_data()
    total_dense_frac, total_jeans_frac = \
        get_dense_gas_and_jeans_fractions(dd, dens_gas_limit=1e4)

    for i, (com,rad) in enumerate(zip(grp_com,grp_radius)):
        
        # define a sphere on the grid where
        # we will look at the gas.
        if (debug):
            print "grp_radius = ", grp_radius
            print "rad and type(rad): ", rad, type(rad)
        if rad < dd['dx'].min().v:
        # yt will throw error if we try to define data object
        # smaller than smallest grid element.
            rad = 1.0*dd['dx'].min().v
            if (debug):
                print "reset radius to dx min: ", 1.0*rad, type(1.0*rad)
        else:
            continue
        if (debug):
            print "about to make sphere: ", com, rad
        sph = ds.sphere(com, rad)
        gas_dense_frac, gas_jeans_frac = \
            get_dense_gas_and_jeans_fractions(sph, dens_gas_limit=1e4)
        # Calculate the total energy of the gas (KE + ME + TE + gas pot'l on the gas + stars pot'l on the gas).
        cell_mass       = sph['cell_mass'].v
        cell_total_mass = np.sum(cell_mass)
        cell_vx         = sph['velx'].v
        cell_vy         = sph['vely'].v
        cell_vz         = sph['velz'].v
        # Get the gas center of mass velocity.
        gas_comvx  = np.sum(cell_mass*cell_vx)/cell_total_mass
        gas_comvy  = np.sum(cell_mass*cell_vy)/cell_total_mass
        gas_comvz  = np.sum(cell_mass*cell_vz)/cell_total_mass

        gas_KE     = 0.5*np.sum(cell_mass*((cell_vx-gas_comvx)**2.0
                                          +(cell_vy-gas_comvy)**2.0
                                          +(cell_vz-gas_comvz)**2.0))
        gas_EI     = np.sum(cell_mass*sph['eint'].v)
        gas_ME     = np.sum(sph['magp'].v*sph['cell_volume'].v) / 4.0 / np.pi
        g_on_g_PE  = 0.5*np.sum(sph['gpot'].v*cell_mass) # *0.5 Because g_ij + g_ji is double counting. 
        
        if (use_stars_for_potential):
            # To keep from running out of memory when there are
            # a lot of cells, we'll have to use a slow loop here.
            s_on_g_PE = 0.0
            this_cell = Particles(1)
            # for j in range(len(sph['cell_mass'])):
            #     this_cell.mass = sph['cell_mass'][j].v | u.g
            #     this_cell.position = np.array([sph['x'][j].v.T,
            #                                    sph['y'][j].v.T,
            #                                    sph['z'][j].v.T]).T | u.cm
            #     s_on_g_PE += this_cell.potential_energy_in_field(groups[i]).value_in(u.erg)
            # Replaced the slow loop with a much faster map.
            potl_list = map(partial(map_stellar_potential_to_cells,
                            one_particle=this_cell,
                            stars=groups[i]),
                            zip(sph['cell_mass'].v,sph['x'].v,sph['y'].v,sph['z'].v))
            s_on_g_PE = np.sum(np.array(potl_list))
        else:
            s_on_g_PE  = np.sum(sph['bgpt'].v*cell_mass)

        gas_TE = gas_KE + gas_EI + gas_ME + g_on_g_PE + s_on_g_PE

        if (debug):
            print "Group gas {} total mass       = {}".format(i, cell_total_mass/1.989e33)
            print "Group gas {} com vel          = {} cm/s".format(i, grp_gas_comv)
            print "Group gas {} total KE         = {}".format(i, gas_KE)
            print "Group gas {} total EI         = {}".format(i, gas_EI)
            print "Group gas {} total ME         = {}".format(i, gas_ME)
            print "Group gas {} total gPE        = {}".format(i, g_on_g_PE)
            print "Group gas {} total sPE        = {}".format(i, s_on_g_PE)
            print "Group gas {} total TE         = {}".format(i, gas_TE)
            # Note this method doesn't double count the PE for gas on gas.
            #print "Group gas {} total TE         = {} \n".format(i, sph['etotal_all_cells'].v)

        grp_gas_TM.append(cell_total_mass)
        grp_gas_comv.append([gas_comvx , gas_comvy , gas_comvz])
        grp_gas_KE.append(gas_KE)
        grp_gas_EI.append(gas_EI)
        grp_gas_ME.append(gas_ME)
        grp_gas_gPE.append(g_on_g_PE)
        grp_gas_sPE.append(s_on_g_PE)
        grp_gas_TE.append(gas_TE)
        grp_gas_dense_frac.append(gas_dense_frac)
        grp_gas_jeans_frac.append(gas_jeans_frac)

    return grp_gas_TM, grp_gas_comv, grp_gas_KE, grp_gas_EI, grp_gas_ME, \
            grp_gas_gPE, grp_gas_sPE, grp_gas_TE, \
            total_dense_frac, total_jeans_frac, \
            grp_gas_dense_frac, grp_gas_jeans_frac

def get_boundedness_to_other_clusters(grp_com, grp_comv, grp_TM, grp_gas_comv, grp_gas_TM, debug=False):
    """
    Calculates the total energy (boundedness) of each cluster relative to all other clusters on the grid.
    this_cluster relative KE is compared to all other that_cluster's PE, where each cluster considers 
    their star and gas center of mass velocities and total masses.
    
    inputs:
    grp_com      - center of mass positions of all clusters (ordered from most to least massive.
    grp_comv     - 3D velocity vector of clusters center of mass
    grp_TM       - total mass of stars in clusters
    grp_gas_comv - center of mass velocity of gas within cluster radius
    grp_gas_TM   - total mass of gas in clusters
    
    outputs:
    cluster_boundedness - list of lists. Where each element is a cluster (ordered from most to least massive) 
                          and each sub-element is the cluster's total energy compared to another cluster 
                          on the grid (also ordered most to least massive). 
                          Both len(list) and len(sub-list) are the same == total number of clusters on grid.
                          As such, there is space to consider a cluster's bounded state to itself, which is left
                          to be 0.0 erg. 
    - Sean C Lewis 05/06/21
    """

    
    G = 6.67259e-8 # Gravitational constant cgs units
    cluster_boundedness = []
    for i,this_com in enumerate(grp_com):
        # cycle over each cluster present on grid
        # extract gas and stats com mass and velocity
        this_com    = this_com
        this_comv   = grp_comv[i]
        this_grp_TM = grp_TM[i]
        #this_gas_comv = grp_gas_comv[i] # Wont need to use this right now, just want to consider stars.
        #this_gas_TM = grp_gas_TM[i]     # But this info can be used for a more robust analysis later. - SCL 05/14/21

        # initialize list that will hold boundedness info for this single cluster.
        tmp_boundedness = []

        
        for j,that_com in enumerate(grp_com):
            # cycle over all other clusters on the grid
            # extract their gas and star info
            that_comv   = grp_comv[j]
            that_grp_TM = grp_TM[j]

            #that_gas_comv = grp_gas_comv[j] # Wont need to use this right now, just want to consider stars.
            #that_gas_TM = grp_gas_TM[j]     # But this info can be used for a more robust analysis later. - SCL 05/14/21
            
            if j==i:
                # This would be the same cluster that we are comparing to (from first for-loop; this_cluster)
                # so we dont need to consider this case, pass zeros and move on.
                # This also handles the case where there is only one cluster on the grid.
                KE = 0.
                PE = 0.
            else:
                # get relative position between the two clusters' centers of mass.
                # we will be assuming the gas and stars of each cluster have the same
                # center of mass position. This is not strictly true. Should be updated.
                r = [this_com[0]-that_com[0], this_com[1]-that_com[1], this_com[2]-that_com[2]]
                r_mag = np.sqrt(r[0]**2 + r[1]**2 + r[2]**2)

                # get relative velocity of stars center of mass
                stars_rel_v = [this_comv[0] - that_comv[0],
                               this_comv[1] - that_comv[1],
                               this_comv[2] - that_comv[2]]
                stars_rel_v_mag = np.sqrt(stars_rel_v[0]**2 + stars_rel_v[1]**2 + stars_rel_v[2]**2)

                # get relative velocity of gas center of mass
                # Dont need this now, but may be necessary for more robust analysis later. - SCL 05/14/21
                #gas_rel_v = [this_gas_comv[0] - that_gas_comv[0],
                #             this_gas_comv[1] - that_gas_comv[1],
                #             this_gas_comv[2] - that_gas_comv[2]]
                #gas_rel_v_mag = np.sqrt(gas_rel_v[0]**2 + gas_rel_v[1]**2 + gas_rel_v[2]**2)

                # Calculate reduced mass kinetic energy components of this_cluster 
                KE_this_stars = 0.5 * stars_rel_v_mag**2
                #KE_this_gas = 0.5 * this_gas_TM * gas_rel_v_mag**2 # Not using this right now, must be updated - SCL 05/14/21

                # KE of this_cluster
                KE = KE_this_stars #+ KE_this_gas
                # Reduced mass PE of this and that cluster orbital system
                PE = -G * (this_grp_TM + that_grp_TM) / r_mag

                if (debug):
                    print "[get_boundedness_from_other_clusters] this star mass = ", this_grp_TM / 1.98e33
                    print "[get_boundedness_from_other_clusters] star relative comv = ", stars_rel_v_mag
                    #print "[get_boundedness_from_other_clusters] this gas mass = ", this_gas_TM / 1.98e33
                    #print "[get_boundedness_from_other_clusters] gas relative comv = ", gas_rel_v_mag
                    print "[get_boundedness_from_other_clusters] KE star com = ", KE_this_stars
                    #print "[get_boundedness_from_other_clusters] KE gas com = ", KE_this_gas
                    print "[get_boundedness_from_other_clusters] distance between clusters = ", r_mag
                    print "[get_boundedness_from_other_clusters] that star mass = ", that_grp_TM / 1.98e33
                    #print "[get_boundedness_from_other_clusters] that gas mass = ", that_gas_TM / 1.98e33
                    print "[get_boundedness_from_other_clusters] PE of that cluster = ", PE

            # Note: we are calculating the specific (reduced mass) orbital energy of the two-cluster system here.
            energy = KE + PE
            # done with that_cluster
            # append energy result to the temporary boundedness list
            tmp_boundedness.append(energy)
            # loop again to next that_cluster
            
        # done with this_cluster.
        # append all its boundedness info to final list.
        cluster_boundedness.append(tmp_boundedness)

    return cluster_boundedness

def map_stellar_potential_to_cells(one_cell, one_particle, stars):

    """
    A simple worker function
    that takes the cell and
    a single particle, maps the
    cell mass and location to the
    particle and then calculates
    the stellar potential on
    the cell from all stars.
    Made this to simply be
    able to use the map
    function for speed over loops.
    """
    
    one_particle.mass     = one_cell[0] | u.g
    one_particle.position = np.array([one_cell[1],
                                      one_cell[2],
                                      one_cell[3]]) | u.cm
    
    potential = one_particle.potential_energy_in_field(stars).value_in(u.erg)

    return potential

def test_if_same_clusters(known_cluster_tags,
                          unknown_cluster_tags,
                          overlap_frac=0.5,
                          debug=False):
    
    """
    This function checks for the stellar
    tags in known_cluster_tags against
    the stellar tags in unknown_cluster_tags
    to determine if the two clusters contain
    the same stars up to the fraction 
    overlap_frac.
    
    Keyword arguments:
    known_cluster_tags   -- tags for known cluster
    unknown_cluster_tags -- tags to test against
    overlap_frac         -- tolerance fraction of
                            same tags to agree
                            this is the same cluster
    
    Returns:
    same_cluster -- Is this the same cluster,
                    given that the 
                    mutual_stellar_frac is
                    greater than overlap_frac.
    """
    
    number_of_test_tags = len(known_cluster_tags)
    same_cluster = False
    
    tags_in_both = np.isin(known_cluster_tags,
            unknown_cluster_tags,
            assume_unique=False)

    mutual_stellar_frac = float(np.sum(tags_in_both))/float(number_of_test_tags)

    if (debug):
        print "sum of tags_in_both =", np.sum(tags_in_both)
        print "overlap_frac =", overlap_frac
        print "mutual_frac  =", mutual_stellar_frac


    if (mutual_stellar_frac > overlap_frac):
        same_cluster=True
            
    return same_cluster, mutual_stellar_frac
    

# Function for plotting the clusters found by HOP.
def plot_clumps(groups, total_mass=None, time=None,
                save_plts=False, show_plts=False,
                plt_name="clumpy", plt_dir="./",
                lw=2, fs=20, dpi=300):

    """
    Plot clusters of particles found
    using the HOP algorithm in AMUSE.
    Plots are made in projection along
    each axis, as well as a scatter plot
    of the mass fraction vs. number of
    members.

    Keyword arguments:
    groups     -- list of particle sets representing
                  the clusters
    total_mass -- total mass of all the stars in all clusters
    save_plts  -- Should I write these plots to file (png and pdf)?
    plt_dir    -- Where to write the plots to the drive
    lw         -- Line width for line plots
    fs         -- Font size for plots
    dpi        -- Resolution for the plots

    Returns: Nothing
    """

    number_of_particles_in_group = []
    fraction_of_mass_in_group =  []

    if (total_mass is None):
        total_mass = sum([x.mass.sum().value_in(u.MSun) for x in groups]) | u.MSun

    for group in groups:
        number_of_particles_in_group.append(len(group))
        fraction = (group.mass.sum()/total_mass)
        fraction_of_mass_in_group.append(fraction)

    figure = plt.figure(figsize=(16,16)) #, dpi=dpi)


    subplot = figure.add_subplot(2, 2, 1)

    colormap = plt.get_cmap('Dark2') #pyplot.cm.Paired
    for index, group in enumerate(groups):
        index = index % 8 # There are only 8 color options in Dark2.
        # This reuses colors if we have lots of clusters. - SCL 05/14/21
        color = colormap.colors[index] #colormap(1.0 * index / len(groups))
        subplot.scatter(
            group.x.value_in(u.parsec),
            group.y.value_in(u.parsec),
            s = group.mass.value_in(u.MSun),
            edgecolors = color,
            facecolors = color,
            label = "{:.2F}".format(fraction_of_mass_in_group[index])
        )

    subplot.set_xlabel('x (pc)')
    subplot.set_ylabel('y (pc)')
    subplot.legend()

    subplot = figure.add_subplot(2, 2, 2)

    for index, group in enumerate(groups):
        index = index % 8 # There are only 8 color options in Dark2.
        # This reuses colors if we have lots of clusters. - SCL 05/14/21
        color = colormap(index) #colormap(1.0 * index / len(groups))
        subplot.scatter(
            group.x.value_in(u.parsec),
            group.z.value_in(u.parsec),
            s = group.mass.value_in(u.MSun),
            edgecolors = color,
            facecolors = color,
            label = "{:.2F}".format(fraction_of_mass_in_group[index])
        )

    subplot.set_xlabel('x (pc)')
    subplot.set_ylabel('z (pc)')
    subplot.legend()

    subplot = figure.add_subplot(2, 2, 3)

    for index, group in enumerate(groups):
        index = index % 8 # There are only 8 color options in Dark2.
        # This reuses colors if we have lots of clusters. - SCL 05/14/21
        color = colormap(index) #colormap(1.0 * index / len(groups))
        subplot.scatter(
            group.z.value_in(u.parsec),
            group.y.value_in(u.parsec),
            s = group.mass.value_in(u.MSun),
            edgecolors = color,
            facecolors = color,
            label = "{:.2F}".format(fraction_of_mass_in_group[index])
        )

    subplot.set_xlabel('z (pc)')
    subplot.set_ylabel('y (pc)')
    subplot.legend()

    if (time is not None): # Set the time on the plot using yt.
        #figure.suptitle(time)
        subplot.text(0.001, 0.001, time, horizontalalignment='left',
        verticalalignment='bottom', transform=subplot.transAxes)

    
    
    subplot = figure.add_subplot(2, 2, 4)

    for index in range(len(number_of_particles_in_group)):
        index = index % 8 # There are only 8 color options in Dark2.
        # This reuses colors if we have lots of clusters. - SCL 05/14/21
        color = colormap(index)
        subplot.scatter(
            number_of_particles_in_group[index],
            fraction_of_mass_in_group[index],
            edgecolors = color,
            facecolors = color
        )

    #subplot.set_xscale('log')
    #subplot.set_yscale('log')
    subplot.set_xlabel('N')
    subplot.set_ylabel(r'$f_M$')

    plt.tight_layout()
    
    #if (time is not None): # Set the time on the plot using yt.
        #figure.suptitle(time)

    if (save_plts):
        figure.savefig(plt_dir+plt_name+'.png', bbox_size='tight_layout')
        figure.savefig(plt_dir+plt_name+'.pdf', bbox_size='tight_layout')
    
    if (show_plts): plt.show()
    # Don't forget to close the figure
    # or you'll run out of RAM...
    plt.close(figure)

    return

def run_energy_analysis_on_batch_of_files(local_files,
                                          cluster_finder="none",
                                          num_hop=16,
                                          num_density=64,
                                          outer_density_limit=1. | u.MSun*u.parsec**-3,
                                          relative_saddle_density=False,
                                          relative_saddle_density_thres=0.8,
                                          passed_hop=None, out_dir='./', out_file='clusters',
                                          save_plts=False, debug=False,
                                          bin_equal_numbers_of_stars=False,
                                          current_index=None, frac_bin=-1.0):

    """
    This function is for use with the task
    based parallel routines. It performs
    the analysis on a batch of files passed
    as local_files.
    Note that in this version 
    everything is stored during 
    the analysis itself, so we
    don't need the current index arg.
    """
    
    for f in local_files:

        # Load the gas data using yt.
        ds = yt.load(f)
        # Load the AMUSE star particles.
        stars = get_ps_from_yt(f)
        
        if (cluster_finder == "hop"):
            
            if (stars is None 
                or len(stars) < num_density 
                or len(stars) < num_hop): continue
            
            # Define the domain for the analysis
            # here if you want to restrict it.
            if (passed_hop is None):
                groups, n_groups = find_clusters_with_hop(stars, num_hop=num_hop,
                                                        num_density=num_density,
                                                        outer_density_limit=outer_density_limit,
                                                        relative_saddle_density=relative_saddle_density,
                                                        relative_saddle_density_thres=relative_saddle_density_thres,
                                                        debug=debug)
            else:
                groups, n_groups = find_clusters_with_hop(stars, passed_hop=passed_hop,
                                                    debug=debug)

        elif (cluster_finder == "dbscan"):

            if (stars is None 
                or len(stars) < num_density
                or len(stars) < num_hop): continue

            groups, n_groups = \
            find_clusters_with_dbscan(stars,
                            outer_density_limit=outer_density_limit,
                            avg_stellar_mass=0.586,
                            min_samples=num_hop, leaf_size=num_density,
                            return_labels=False, debug=debug)

        elif (cluster_finder == "none"):

            if (stars is None 
                or len(stars) < num_hop): continue
            # Just make one "group" of stars to analyze.
            groups  = [stars]
            n_groups = 1

        else:
            print "[run_energy_analysis_on_batch_of_files]: cluster_finder \
            value not recongized. cluster_finder =", cluster_finder

        # Always sort the clusters from highest mass
        # to lowest mass to make plotting and analysis
        # easier.
        groups.sort(key=lambda x: x.mass.sum(), reverse=True)

        # Calculate the total energy of the stars 
        # (KE + gas pot'l on stars + stars pot'l on stars).
        grp_com, grp_comv, grp_TM, \
        grp_radius, grp_KE, grp_gPE, \
        grp_sPE, grp_TE \
                = calculate_stellar_group_properties(ds, groups, frac_bin, debug=debug)

        # Calculate the total energy of the gas 
        # (KE + ME + TE + gas pot'l on the gas + stars pot'l on the gas).
        grp_gas_TM, grp_gas_comv, grp_gas_KE, \
        grp_gas_EI, grp_gas_ME, \
        grp_gas_gPE, grp_gas_sPE, \
        grp_gas_TE, \
        total_dense_frac, total_jeans_frac, \
        grp_dense_frac, grp_jeans_frac \
                = calculate_gas_properties(
                    ds, groups, grp_com,
                    grp_radius, debug=debug
                    )

        if (debug):
            print "about to find boundedness of cluster relative to other clusters"
        # For each cluster, cycle through all clusters and calculate relative total energy to test for boundedness. 
        cluster_boundedness = get_boundedness_to_other_clusters(grp_com, grp_comv, grp_TM,
                                                                grp_gas_comv, grp_gas_TM, debug)
        if (debug):
            print "[cluster boundedness]: ", cluster_boundedness
        
        # Calculate half mass radii for each mass bin for all the clusters.
        half_mass_radii = []
        for j,grp in enumerate(groups):
            half_mass_radii.append(
                get_half_mass_radii_for_mass_bins(
                    grp,
                    com1=grp_com[j] | u.cm,
                    bin_equal_numbers_of_stars=bin_equal_numbers_of_stars,
                    verbose=debug
                )
            )
        
        if (debug):
            print "half_mass_radii =", half_mass_radii

        # Calculate the Lagrangian radii for all the clusters.
        lagrangian_radii = []
        for j,grp in enumerate(groups):
            lagrangian_radii.append(
                get_lagrangian_radii(
                    grp, com=grp_com[j] | u.cm,
                    verbose=debug
                )
            )


        if (save_plts):
            # Make plots.
            plot_clumps(groups, stars.mass.sum(),
                        time="{:.2F} Myr".format(ds.current_time.in_units('Myr').v),
                        save_plts=save_plts, plt_dir=out_dir,
                        plt_name='clumpy'+f[-4:])

        # Store the data
        clusters = cluster_storage(groups, n_groups, 
                        grp_com, grp_comv, grp_TM,
                        grp_radius, grp_KE, grp_gPE,
                        grp_sPE, grp_TE,
                        grp_gas_TM, grp_gas_comv,
                        grp_gas_KE, grp_gas_EI, grp_gas_ME,
                        grp_gas_gPE, grp_gas_sPE,
                        grp_gas_TE,
                        grp_dense_frac, grp_jeans_frac,
                        total_dense_frac, total_jeans_frac,
                        half_mass_radii, lagrangian_radii,
                        cluster_boundedness)

        # Now save all the results to a pickle file.
        with open(out_dir+out_file+f[-4:]+'.pickle', 'wb') as wf:
                pickle.dump(clusters, wf)
                wf.close()
    
    if (debug): print "Done!"

