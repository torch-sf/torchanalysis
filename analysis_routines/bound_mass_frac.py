import yt
yt.mylog.level=0
import numpy as np
from scipy.spatial.distance import cdist, pdist
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
#import scipy.interpolate as spi
from amuse.lab import *

import pickle # Save the results.
import time as pt
import argparse
import sys
# For a progress bar!
from tqdm import tqdm

from analysis_utilities import gather_files, setup_for_mpi_run, mpi_reduce_np_array_in_place
from energy_routines import get_dense_gas_and_jeans_fractions

parser = argparse.ArgumentParser(description=" Calculate the bound mass fraction \
                                of stars and gas in Flash checkpoint files. \
                                 Written by: Joshua Wall, Drexel University")
                                 
parser.add_argument("-d", "--directory", default="./",
                   help="Input directory where the particle files are located. \
                         Default is './' Note we assume the particle files look like *part*.")

parser.add_argument("-w", "--write_directory", default=".",
                   help="Output directory where the files are written. \
                         Default is '.'")

parser.add_argument("-f", "--filename", default="*chk*",
                   help="Filename prefix (before the numbers). \
                         Default is *chk*.")

parser.add_argument("-s", "--start_file_number", default=None, type=int,
                    help="The starting number of the files you want to \
                          plot. By default its None, which picks the first file \
                          in the folder.")

parser.add_argument("-e", "--end_file_number", default=None, type=int,
                    help="The ending number of the files you want to \
                          plot. By default its None, which picks the \
                          last file in the folder.")

parser.add_argument("-v", "--verbose", default=0, type=int,
                    help="Speak to me.")

parser.add_argument("-dr", "--dry_run", action="store_true",
                    help="Setup to run, but then exit before calculating anything.")

# Stuff used to calculate the gas energy density
from yt import derived_field
from yt.utilities.physical_constants import G, kboltz
import yt.units as u

@derived_field(name = "gpotEdens", units = "g*cm**-1*s**-2")
def _gpotEdens(field, data):
    return (0.5*data['dens']*data['gpot']).in_base()


@derived_field(name='magedens', units="erg/cm**3")
def _magedens(field, data):
    return data["magp"]/(4.0*np.pi)

@derived_field(name="etotal", units="erg/cm**3")
def _etotal(field, data):
    return (data["dens"].in_base()*(0.5*data["velocity_magnitude"].in_base()**2.0 
            + 0.5*data["gpot"].in_base() + data["eint"].in_base()) + data["magedens"].in_base())


def get_half_mass_radii_for_mass_bins(stars,
                                      mass_frac_bins=[0.0, 0.25, 0.5, 0.75, 1.0],
                                      verbose=0):

    # Lagrangian radii (half mass radii of stars CDF sorted by mass from
    # least massive to most massive).
    #conv = nbody_system.nbody_to_si(star_total_mass, star_total_energy)
    #lr, mf = stars.LagrangianRadii(unit_converter=conv, reuse_hop=True)
    #lag_r[lw_ind,:] = lr.value_in(units.cm)

    lag_r       = np.zeros((num_files, len(mass_frac_bins)-1))

    # Masses.
    lr_mass     = stars.mass.value_in(units.g)
    # Mass indices sorted from least to greatest.
    sort_ind    = np.argsort(lr_mass)
    # Relative sorted cumulative masses.
    rel_lr_mass = lr_mass[sort_ind].cumsum()/lr_mass.sum()
    if (verbose > 1): print "Rank", rank, "rel_lr_mass=", rel_lr_mass
    if (verbose > 1): print "len of rel_lr_mass", len(rel_lr_mass)

    for j in range(len(mass_frac_bins)-1):
        # Lower and upper percentage bounds.
        lower       = mass_frac_bins[j]
        upper       = mass_frac_bins[j+1]
        if (verbose > 2): print "Rank", rank, "lower=", lower
        if (verbose > 2): print "Rank", rank, "upper=", upper

        # Lower bound sorted index (exclusive).
        lower_ind   = np.where(rel_lr_mass > lower)[0][0]
        # Upper bound sorted index (inclusive).
        upper_ind   = np.where(rel_lr_mass <= upper)[0][-1]
        if (verbose > 2): print "Rank", rank, "lower_ind=", lower_ind
        if (verbose > 2): print "Rank", rank, "upper_ind=", upper_ind
        # Now select a subset of the stars in this bin
        bin_stars = stars[sort_ind[lower_ind:upper_ind]]
        if (verbose > 2): print "Rank", rank, "len(bin_stars)=", len(bin_stars)
        rel_pos   = (bin_stars.position - com1).value_in(units.cm)
        
        if (lower_ind > upper_ind):
            if (verbose > 2): print "This bin must be empty, lower_ind > upper_ind!"
            lag_r[lw_ind,j] = 0.0
        elif (lower_ind == upper_ind):
            rel_r     = np.sqrt((rel_pos**2.).sum())
            lag_r[lw_ind,j] = rel_r
        else:
            rel_r     = np.sqrt((rel_pos**2.).sum(axis=1))
            sort_m    = bin_stars.mass[np.argsort(rel_r)].value_in(units.g)
            sort_r    = rel_r[np.argsort(rel_r)]
            rel_m     = sort_m.cumsum()/(bin_stars.mass.sum().value_in(units.g))
            if (verbose > 2): print "Rank", rank, "rel_m=", rel_m
            hm_ind    = np.where(rel_m>= 0.5)[0][0]
            lag_r[lw_ind,j] = sort_r[hm_ind]

    if (verbose > 0): print "Rank", rank, "stored particle Lagrangian half mass radii."

    return lag_r

# Try to include magnetic energy for the gas.
try_with_mag  = True
# Try to include the particle potential for the gas.
try_with_bgpt = True

args = parser.parse_args()
file_dir  = args.directory
file_name = args.filename
file_path = file_dir+'/'+file_name
start     = args.start_file_number
end       = args.end_file_number
out_dir   = args.write_directory
verbose   = args.verbose
if (verbose > 0):
    debug = True
else:
    debug = False
dry_run   = args.dry_run

if (dry_run): verbose = 1

files, start, end = gather_files(file_name, file_dir,
                 start=start, end=end, debug=debug)

num_files       = len(files)

if (verbose > 0):
    print "start=", start
    print "end  =", end
    print "num_files=", num_files

rank, size, comm, local_files, \
    num_local_files, work_start, work_end \
    = setup_for_mpi_run(files, num_files, debug=debug)

if (rank == 0):
    t1 = pt.time()

comm.Barrier()

# Arrays for the bound mass fractions.
num_stars = np.zeros(num_files)
mass      = np.zeros(num_files)
star_mass = np.zeros(num_files)
# Dense and Jeans unstable fractions.
dense_frac = np.zeros(num_files)
jeans_frac = np.zeros(num_files)
# Virial / gravitational radius
vir_r     = np.zeros(num_files)
# Half mass radius
hm_r      = np.zeros(num_files)
# Mass in gas inside half mass radius
mass_in_hm_r   = np.zeros(num_files)
# Bound half mass radius
bhm_r     = np.zeros(num_files)
# Mass in gas inside bound half mass radius
mass_in_bhm_r  = np.zeros(num_files)
# Lagrangian radii
mass_frac_bins = [0.0, 0.25, 0.5, 0.75, 1.0]
lag_r          = np.zeros((num_files, len(mass_frac_bins)-1))
# Array for current simulation time.
time = np.zeros(num_files)

if (dry_run): comm.Abort()

if (rank ==0): pbar = tqdm(total=num_local_files)

# Process the local parts of the files.
for i in range(num_local_files):
    
    if (verbose > 0): print "Rank", rank, "starting loop", i
    
    try:
        del(ds)
    except NameError:
        pass
    
    has_particles = True
    
    #print "I'm on step", i+work_start
    # Local index of the file we need to load.
    local_file_index = i + work_start
    # Local working index that includes the offset from the first file
    # globbed above.
    lw_ind = i+work_start-start 

    ds = yt.load(files[local_file_index])
    dd = ds.all_data()
    if (verbose > 0): print "Rank", rank, "loaded file."
    
    # Test for the presence of particles. If none, jump to exception.
    if (getattr(ds.fields, 'io', None) == None):
        has_particles = False

    if (verbose > 0): print "Rank", rank, "tested for particles." 
    
    # Note the index is offset by the global start file.
    time[lw_ind] = ds.current_time.v
    if (verbose > 0): print "Rank", rank, "recorded time of", time[lw_ind]
    
    if (try_with_mag):
        try:
            M = dd["magedens"].v    # magnetic energy density
        except:
            M = 0.0
            try_with_mag = False
            print "Tried with magnetic energy, but it wasn't found."
    else:
        M = 0.0
    if (try_with_bgpt):
        try:
            W = (dd['dens'].v*(dd["gpot"].v+dd["bgpt"].v))  # potental energy density including particles potential
        except:
            W = (dd['dens']*dd["gpot"]).v  # potental energy density
            try_with_bgpt = False
            print "Tried with bridge potential, but it wasn't found."        
    else:
        W = (dd['dens']*dd["gpot"]).v  # potental energy density
    if (verbose > 0): print "Rank", rank, "tried to load mage and bgpt."

    U = 3.0/2.0*dd["pres"].v       # internal energy density
    T = (0.5*dd['dens']*           # kinetic energy density
     (dd['velx']**2.0+dd['vely']**2.0+dd['velz']**2.0)).v 
    
    mass0 = dd['cell_mass']
    #cm = dd['cell_mass']
    #cp = np.array([dd['x'].v, dd['y'].v, dd['z'].v]).T
    
    if (verbose > 0): print "Rank", rank, "Got gas energy and mass."

    if (has_particles):
        
        pm = dd['particle_mass'].v
        num_stars[lw_ind] = len(pm)
        # Skip if there are less than 20 particles.
        if (num_stars[lw_ind] < 20): continue
        
        pp = np.array([dd['particle_position_x'].v, 
                       dd['particle_position_y'].v, 
                       dd['particle_position_z'].v]).T
        pv = np.array([dd['particle_velocity_x'].v,
                       dd['particle_velocity_y'].v, 
                       dd['particle_velocity_z'].v]).T
        
        stars = Particles(len(pm))
        stars.mass     = pm | units.g
        stars.position = pp | units.cm
        stars.velocity = pv | units.cm/units.s
        
        # Particle KE
        pke = 0.5*pm*np.array([dd['particle_velocity_x'].v**2.0 +
                                 dd['particle_velocity_y'].v**2.0 + 
                                 dd['particle_velocity_z'].v**2.0]).flatten()

        # Particle-Particle PE
        ppe = (stars.mass*stars.potential()).value_in(units.erg)

        if (verbose > 0): print "Rank", rank, "got particle KE and PE."
        # Particle-Gas PE (on the particles)
        pge = np.zeros(len(pm))
        field_potl   = ds.find_field_values_at_points('gpot',pp*yt.units.cm)
        pge          = (pm*field_potl).v

        # Total Particle Energy.
        pet = (pke + pge + ppe).ravel()
        
        if (verbose > 0): print "Rank", rank, "got particle total energy."
        # Amount of particle mass that is bound.
        bound_ind = np.where(pet < 0.0)
        pmb = pm[bound_ind].sum()
        # Bound fraction.
        star_mass[lw_ind] = pmb / (pm.sum())

        if (verbose > 0): print "Rank", rank, "stored particle bound mass fraction."
        # Total mass and energy for converter and calculations below.
        star_total_mass   = pm.sum()  | units.g
        star_total_energy = pet.sum() | units.cm**2.0/units.s**2.0*units.g
        
        # Virial radius. Note this is exactly 1/2 the gravitational radius,
        # where r_g = G*M/|U|.
        vir_r[lw_ind] = stars.virial_radius().value_in(units.cm)
        if (verbose > 0): print "Rank", rank, "stored virial radius."
        
        # Half mass radius
        # Note here we are calculating the radius that contains half the
        # mass by cumlative mass distribution sorted BY RADIUS,
        # not sorted by MASS as in the Lagrangian radii of different
        # mass fractions.
        com1     = stars.center_of_mass()
        rel_pos = (stars.position - com1).value_in(units.cm)
        rel_r   = np.sqrt((rel_pos**2.).sum(axis=1))
        sort_m  = stars.mass[np.argsort(rel_r)]
        sort_r  = rel_r[np.argsort(rel_r)]
        rel_m   = sort_m.cumsum()/star_total_mass
        hm_ind  = np.where(rel_m>= 0.5)[0][0]
        hm_r[lw_ind] = sort_r[hm_ind]
        if (verbose > 0): print "Rank", rank, "stored particle half mass radius."
        
        min_dx = dd['dx'].min().v
        if (hm_r[lw_ind] > min_dx):
            sph = ds.sphere(com1.value_in(units.cm),(hm_r[lw_ind], 'cm'))
            mass_in_hm_r[lw_ind] = sph['cell_mass'].sum()

        # Bound half mass radius
        bound_stars = stars[bound_ind]
        #com2      = bound_stars.center_of_mass()
        rel_pos  = (bound_stars.position - com1).value_in(units.cm)
        rel_r    = np.sqrt((rel_pos**2.).sum(axis=1))
        sort_m   = bound_stars.mass[np.argsort(rel_r)].value_in(units.g)
        sort_r   = rel_r[np.argsort(rel_r)]
        rel_m    = sort_m.cumsum()/(bound_stars.mass.sum().value_in(units.g))
        hm_ind   = np.where(rel_m>= 0.5)[0][0]
        bhm_r[lw_ind] = sort_r[hm_ind]
        del (bound_stars)
        if (verbose > 0): print "Rank", rank, "stored particle bound half mass radius."
        
        if (bhm_r[lw_ind] > min_dx):
            sph = ds.sphere(com1.value_in(units.cm),(bhm_r[lw_ind], 'cm'))
            mass_in_bhm_r[lw_ind] = sph['cell_mass'].sum()

        lag_r = get_half_mass_radii_for_mass_bins(stars,
                                      mass_frac_bins=mass_frac_bins,
                                      verbose=verbose)

    #Total gas energy.
    try:
        get = M+W+T+U
    except:
        print "Summing gas total energy failed."
        print "M shape =", np.shape(M)
        print "T shape =", np.shape(T)
        print "W shape =", np.shape(W)
        print "U shape =", np.shape(U)
        comm.Abort()
    if (verbose > 0): print "Rank", rank, "got gas total energy."
    # Amount of gas mass that is bound.
    gmb = mass0[np.where(get < 0.0)].sum()
    # Bound fraction.
    mass[lw_ind]      = gmb / (mass0.sum())
    # Dense and Jeans unstable fractions.
    dense_frac[lw_ind], jeans_frac[lw_ind] \
        = get_dense_gas_and_jeans_fractions(dd, dens_gas_limit=1e4)

    if (verbose > 0): print "Rank", rank, "stored gas bound mass fraction."
    if (verbose > 0): print "Rank", rank, "finished loop", i
    
    if (rank == 0): pbar.update(i)

if (rank ==0): pbar.close()

comm.Barrier()

if (verbose > 0): print "Rank", rank, "starting data reduction." 
# Reduce the finished data on the root process. Note sum is the default operation.
# Note that a capital letter on the method means this is for numpy arrays!
num_stars = mpi_reduce_np_array_in_place(num_stars, comm)
if (verbose > 0): print "Rank", rank, "reduced num_stars."

mass = mpi_reduce_np_array_in_place(mass, comm)
if (verbose > 0): print "Rank", rank, "reduced mass." 

star_mass = mpi_reduce_np_array_in_place(star_mass, comm)
if (verbose > 0): print "Rank", rank, "reduced star mass."

dense_frac = mpi_reduce_np_array_in_place(dense_frac, comm)
if (verbose > 0): print "Rank", rank, "reduced dense frac."

jeans_frac = mpi_reduce_np_array_in_place(jeans_frac, comm)
if (verbose > 0): print "Rank", rank, "reduced jeans frac." 

vir_r = mpi_reduce_np_array_in_place(vir_r, comm)
if (verbose > 0): print "Rank", rank, "reduced virial radius." 

hm_r = mpi_reduce_np_array_in_place(hm_r, comm)
if (verbose > 0): print "Rank", rank, "reduced half mass radius."

bhm_r = mpi_reduce_np_array_in_place(bhm_r, comm)
if (verbose > 0): print "Rank", rank, "reduced bound half mass radius."

mass_in_hm_r = mpi_reduce_np_array_in_place(mass_in_hm_r, comm)
if (verbose > 0): print "Rank", rank, "reduced mass in half mass radius."

mass_in_bhm_r = mpi_reduce_np_array_in_place(mass_in_bhm_r, comm)
if (verbose > 0): print "Rank", rank, "reduced mass in bound half mass radius."

lag_r = mpi_reduce_np_array_in_place(lag_r, comm)
if (verbose > 0): print "Rank", rank, "reduced Lagrangian radii."

time = mpi_reduce_np_array_in_place(time, comm,
    debug=debug, pre='(Proc, {})'.format(rank))
if (verbose > 0): print "Rank", rank, "reduced time." 

if (rank == 0):
    print "Done with analysis. Now writing data."

if (rank == 0):

    # Store this numpy array as a pickled object.
    with open(out_dir+'/gas_bound_frac.pickle', 'wb') as wf:
        pickle.dump(mass, wf)

    with open(out_dir+'/star_bound_frac.pickle', 'wb') as wf:
        pickle.dump(star_mass, wf)

    with open(out_dir+'/dense_frac.pickle', 'wb') as wf:
        pickle.dump(dense_frac, wf)

    with open(out_dir+'/jeans_frac.pickle', 'wb') as wf:
        pickle.dump(jeans_frac, wf)
    
    with open(out_dir+'/vir_r.pickle', 'wb') as wf:
        pickle.dump(vir_r, wf)
    
    with open(out_dir+'/hm_r.pickle', 'wb') as wf:
        pickle.dump(hm_r, wf)
    
    with open(out_dir+'/bhm_r.pickle', 'wb') as wf:
        pickle.dump(bhm_r, wf)

    with open(out_dir+'/mass_in_hm_r.pickle', 'wb') as wf:
        pickle.dump(mass_in_hm_r, wf)
        
    with open(out_dir+'/mass_in_bhm_r.pickle', 'wb') as wf:
        pickle.dump(mass_in_bhm_r, wf)
    
    with open(out_dir+'/lag_r.pickle', 'wb') as wf:
        pickle.dump(lag_r, wf)
        
    with open(out_dir+'/time_bound_frac.pickle', 'wb') as wf:
        pickle.dump(time, wf)
        
if (verbose > 0): print "Rank", rank, "data writing complete." 

comm.Barrier()

if (rank == 0):

    t2 = pt.time()
    # Done!
    print "Finished writing data."
    print "Time to run was", t2 - t1

