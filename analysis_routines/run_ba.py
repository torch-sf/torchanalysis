### Run a binary analysis in parallel on a set of files.

from amuse.community.flash.binary_analysis.binary_analysis import *

import argparse


parser = argparse.ArgumentParser(description=" Run an analysis of all the \
                                 binaries of stars in Flash particle files. \
                                 Written by: Joshua Wall, Drexel University")
                                 
parser.add_argument("-d", "--directory", default="./",
                   help="Input directory where the particle files are located. \
                         Default is './' Note we assume the particle files look like *part*.")

parser.add_argument("-w", "--write_directory", default="./",
                   help="Output directory where the files are written. \
                         Default is './'")

parser.add_argument("-f", "--filename", default="*part*",
                   help="Filename prefix (before the numbers). \
                         Default is *part*.")

parser.add_argument("-o", "--out_filename", default="stars",
                   help="Base filename for output files. \
                         Default is stars.")

parser.add_argument("-g", "--gamma", default=0.01, type=float,
                    help="Set the ratio of perturber tidal acceleration \
                          to binary self acceleration. \
                          Default is 0.01.")

parser.add_argument("-db", "--debug", action="store_true",
                    help="Debug this script.")

parser.add_argument("-ej", "--do_ejection", action="store_true",
                    help="Also run an ejecton analysis? Note: This only works \
                          in serial, so any MPI stuff will run after this.")
                    

args       = parser.parse_args()
file_dir   = args.directory
file_name  = args.filename
out_dir    = args.write_directory
out_file   = args.out_filename
gamma      = args.gamma
debug      = args.debug
do_ej      = args.do_ejection

if (debug): print "Starting up in debug mode.", debug

files = get_files(FLASH_file_dir=file_dir, FLASH_file_name=file_name)
num_files = len(files)

if (debug):
      #print files
      print num_files

rank, size, comm, local_files, \
    num_local_files, work_start, work_end = \
        setup_for_mpi_run(files, num_files, debug=debug)

if (do_ej and rank == 0):
    cl, ejl, totmass = do_ejection_analysis(files, AMUSE_file_dir=out_dir, debug=debug)

all_mut_bnd, set_num_of_binaries = \
        do_binary_analysis(gamma, local_files,
                            AMUSE_file_dir=out_dir,
                            AMUSE_file_name=out_file,
                            start_one_kep = True,
                            debug=debug)

print "Done with analysis on rank ", rank
